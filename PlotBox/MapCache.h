//
//  MapCache.h
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MapCache : NSManagedObject

@property (nonatomic, retain) NSString * companyId;
@property (nonatomic, retain) NSString * facilityId;
@property (nonatomic, retain) NSString * mapFile;

@end
