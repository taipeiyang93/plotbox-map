//
//  SearchDeceasedRecordsViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/16/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import ArcGIS
import SwiftyJSON
import Alamofire
import CoreData

class SearchDeceasedRecordsViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, SHDropDownDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    /* UI Outlets */
    @IBOutlet weak var dropDownSections: SHDropdown!
    @IBOutlet weak var dropDownFacilities: SHDropdown!
    @IBOutlet weak var scrollViewSearchBar: UIScrollView!
    @IBOutlet weak var constraintSearchBarWidth: NSLayoutConstraint!
    @IBOutlet weak var mapAreaView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var tableSearchResults: UITableView!
    @IBOutlet weak var searchResultsView: UIView!
    
    @IBOutlet weak var constriantSearchResultsHeight: NSLayoutConstraint!
    @IBOutlet weak var constriantSearchResultsTop: NSLayoutConstraint!
    
    @IBOutlet weak var textFieldQuery: UITextField!
    
    @IBOutlet weak var pageNavigatorView: UIView!
    @IBOutlet weak var paginator: UICollectionView!
    /* Private members */
    private var mapManager: SearchMapManager?
    private var searchResults: NSArray = NSArray()
    private weak var facilities: NSArray?
    
    private var tableSearchResultsLaunched : Bool = false
    
    private var lastSearchText: String = ""
    private var lastFacilityId: String = ""
    private var lastSectionName: String = ""
    private var limit = 20
    private var pageCount = 0
    
    
    /* Overridden methods */
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.refreshSearchBarSizes() // Resize the searchbar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        println(NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        var error: NSError?
        println(NSBundle.mainBundle().pathForResource("965575804", ofType: "tpk"))
        
        NSFileManager.defaultManager().copyItemAtPath(NSBundle.mainBundle().pathForResource("965575804", ofType: "tpk")!, toPath: documentsPath.stringByAppendingPathComponent("965575804.tpk"), error: &error)
        
        MapCache.insertData("1", companyId: "ipad", mapFile: "965575804.tpk", context: CoreData.getManagedObjectContext()!)
        
        NSFileManager.defaultManager().copyItemAtPath(NSBundle.mainBundle().pathForResource("2126101103", ofType: "tpk")!, toPath: documentsPath.stringByAppendingPathComponent("2126101103.tpk"), error: &error)
        MapCache.insertData("2", companyId: "ipad", mapFile: "2126101103.tpk", context: CoreData.getManagedObjectContext()!)
*/
        
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading...")
        self.dropDownFacilities.dropDownDelegate = self
        
        // Get a list of facilities and their sections
        Facility.list({(results, error) in
            hud.hide(true)
            
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            if let results = results {
                self.facilities = results
                
                for var index = 0; index < results.count; ++index {
                    let facility: Facility = results[index] as! Facility
                    self.dropDownFacilities.addItem(SHDropdownItem(facility.name, tag: facility.facilityId))
                    if index == 0 {
                        
                        // Load the Map
                        self.mapManager = SearchMapManager(mapAreaView: self.mapAreaView, facility: facility)
                        self.dropDownSections.addItem(SHDropdownItem("All", tag: ""))
                        
                        for facilitySection in facility.sortedSections as! [FacilitySection] {
                            self.dropDownSections.addItem(SHDropdownItem(facilitySection.name, tag: facilitySection.facilitySectionId))
                        }
                    }
                }
                
            }
            
        })
        
        paginator.delegate = self
        paginator.dataSource = self
        pageNavigatorView.layer.borderWidth = 1
        pageNavigatorView.layer.cornerRadius = 2
        pageNavigatorView.clipsToBounds = true
        pageNavigatorView.layer.borderColor = UIColor(red: 221,green: 221,blue: 221).CGColor
        
        tableSearchResults.delegate = self
        tableSearchResults.dataSource = self
        textFieldQuery.delegate = self
        
        // Lose textfield focus if tapped anywhere else
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "handleSingleTap:")
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
        
        self.view.layoutIfNeeded()
        self.refreshSearchBarSizes() // Set searchbar sizes
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let mapManager = self.mapManager {
            mapManager.dismissCallout()
        }
        if segue.identifier == "segueViewDeceasedRecord" {
            if let deceasedRecord = sender as? DeceasedRecord {
                let recordDetailsViewController: RecordDetailsViewController = segue.destinationViewController as! RecordDetailsViewController

                recordDetailsViewController.deceasedRecord = deceasedRecord
                
                // Assign facility for viewing this deceased record
                if let facilities = self.facilities {
                    if dropDownFacilities.selectedIndex > -1 &&  dropDownFacilities.selectedIndex < facilities.count {
                        let facility: Facility = facilities[dropDownFacilities.selectedIndex] as! Facility
                        recordDetailsViewController.facility = facility
                    }
                }
            }
        }
    }
    
    
    /* Selectors */
    func actionViewDeceasedRecord(sender: UIButton!) {
        let buttonPosition: CGPoint  = (sender as UIButton).convertPoint(CGPointZero, toView: self.tableSearchResults)
        let indexPath: NSIndexPath? = self.tableSearchResults.indexPathForRowAtPoint(buttonPosition)
        if let indexPath = indexPath
        {
            let deceasedRecord: DeceasedRecord = self.searchResults[indexPath.row] as! DeceasedRecord
            if let mapManager = self.mapManager {
                mapManager.zoomToPlot(deceasedRecord.plot)
            }
            self.performSegueWithIdentifier("segueViewDeceasedRecord", sender: deceasedRecord)
        }
    }
    
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        // Close keyboard
        self.view.endEditing(true)
        // Close Sidebar
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            revealViewController.setFrontViewPosition(FrontViewPosition.Left, animated: true)
        }
        // Close Result list if required
        let location : CGPoint = recognizer.locationInView(self.searchResultsView)
        if self.searchResultsView.hidden || !self.searchResultsView.pointInside(location, withEvent: nil) {
            self.searchResultsView.hidden = true
        }
    }
    
    /* Private methods */
    private func refreshSearchBarSizes() {
        
        if self.view.frame.width < 768 {
            constraintSearchBarWidth.constant = 768 // Min width
        } else if self.view.frame.width > 900 {
            constraintSearchBarWidth.constant = 900 // Max width
        } else {
            constraintSearchBarWidth.constant = self.view.frame.width
        }
        
        UIView.animateWithDuration(0.2, animations: {
            self.view.layoutIfNeeded()
        })
        scrollViewSearchBar.contentSize = CGSize(width: constraintSearchBarWidth.constant, height: scrollViewSearchBar.contentSize.height)
    }
    
    private func search(text: String, facilityId: String, sectionName: String, offset: Int, limit: Int, newSearch: Bool) {
    
        if let mapManager = self.mapManager {
            mapManager.clearPins()
            mapManager.dismissCallout()
        }
        
        var hud: MBProgressHUD
        if newSearch {
            hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Searching...")
        } else {
            hud = MBProgressHUD.showHUDAddedTo(self.searchResultsView, animated: true, labelText: "Loading...")
        }
        
        DeceasedRecord.search(text, facilityId: facilityId, sectionName: sectionName, offset: offset, limit: self.limit, completionHandler: { (totalRecords, results, error) -> Void in
            hud.hide(true)
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            if let results = results {
                if results.count > 0 {
                    self.textFieldQuery.toggleHighlightShadow(true)
                    
                    self.searchResults = results
                    if let mapManager = self.mapManager {
                        mapManager.pinDeceasedRecords(results)
                    }
                    
                    self.tableSearchResults.reloadData()
                    if !self.tableSearchResultsLaunched {
                        self.searchResultsView.alpha = 0.0
                        self.searchResultsView.hidden = false
                        self.tableSearchResultsLaunched = true
                        
                        
                        
                        UIView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                            self.searchResultsView.alpha = 1
                            }, completion: { finished in
                                self.searchResultsView.alpha = 0.9
                        })
                        
                    }
                    
                    if newSearch {
                        self.pageCount = (totalRecords! + limit - 1) / limit
                        self.paginator.reloadData()
                        if totalRecords > 0 {
                            self.paginator.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                        }
                        self.constriantSearchResultsTop.constant = self.pageCount > 1 ? 50 : 0
                    }
                    
                    // Resize the search list's height based on the number of results
                    self.constriantSearchResultsHeight.constant = CGFloat(min(5, results.count)  * 60) +  self.constriantSearchResultsTop.constant
                    
                    
                    return
                }
            }
            
            Utils.showAlert("No Match found", message: "")
        })
    }
    
    /* Delegate messages */
    func textFieldDidBeginEditing(textField: UITextField) {
        if self.tableSearchResultsLaunched {
            self.searchResultsView.hidden = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.searchButtonClicked(textField)
        return false
    }
    
    func dropDown(dropDown: SHDropdown!, didChangeSelection newItem: SHDropdownItem!, newRow: Int, oldRow: Int) {
        if let facilities = self.facilities {
            if newRow < facilities.count {
                let facility: Facility = facilities[newRow] as! Facility
                
                // Change Map
                if let mapManager = self.mapManager {
                    mapManager.changeFacility(facility)
                    //mapManager.zoomToPoint(facility.latitude, longitude: facility.longitude)
                }
                // Change available sections
                self.dropDownSections.removeAllItems()
                self.dropDownSections.addItem(SHDropdownItem("All", tag: ""))
                
                for facilitySection in facility.sortedSections as! [FacilitySection] {
                    self.dropDownSections.addItem(SHDropdownItem(facilitySection.name, tag: facilitySection.facilitySectionId))
                }
                if !textFieldQuery.isBlank() {
                    self.search(self.lastSearchText, facilityId: self.lastFacilityId, sectionName: self.lastSectionName, offset: 0, limit: self.limit, newSearch: true)
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellWithDate", forIndexPath: indexPath) as! UITableViewCell
        
        var nameLabel : UILabel = cell.viewWithTag(1) as! UILabel
        var dateLabel : UILabel = cell.viewWithTag(2) as! UILabel
        var sectionLabel : UILabel = cell.viewWithTag(3) as! UILabel
        var viewButton : UIButton = cell.viewWithTag(4) as! UIButton
            
        let deceasedRecord: DeceasedRecord = self.searchResults[indexPath.row] as! DeceasedRecord
        
        nameLabel.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
        dateLabel.text = deceasedRecord.deceasedDate
        
        //nameLabel.textColor = UIColor.
        
        var sectionLabelText: String = deceasedRecord.plot.sectionName
        if !deceasedRecord.plot.row.isEmpty {
            sectionLabelText += "/" +  deceasedRecord.plot.row
        }
        
        sectionLabelText += "/" + deceasedRecord.plot.plotNumber
        sectionLabel.text = sectionLabelText
        
        viewButton.addTarget(self, action: "actionViewDeceasedRecord:", forControlEvents: UIControlEvents.TouchDown)
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(red: 240,green: 240,blue: 240)
        } else {
            cell.backgroundColor  = UIColor.whiteColor()
        }
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let plot : Plot = self.searchResults[indexPath.row].plot
        if let mapManager = self.mapManager {
            mapManager.zoomToPlot(plot)
        }
        
        // Colors
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            var viewButton : UIButton = cell.viewWithTag(4) as! UIButton
            viewButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            var viewButton : UIButton = cell.viewWithTag(4) as! UIButton
            viewButton.setTitleColor(UIColor(red: 48, green: 131, blue: 251), forState: UIControlState.Normal)
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 52, green: 163, blue: 215)
        bgColorView.layer.masksToBounds = true
        cell.selectedBackgroundView = bgColorView
    }
        
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pageCount
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
        
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        label.text = String(indexPath.item + 1)
        label.layer.borderWidth = 1
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        
        // Default selection
        if cell.selected {
            label.textColor = UIColor.whiteColor()
            label.backgroundColor =  UIColor(red: 52,green: 163,blue: 215)
            label.layer.borderColor = UIColor(red: 52,green: 163,blue: 215).CGColor
        } else {
            label.textColor = UIColor.blackColor()
            label.backgroundColor =  UIColor.whiteColor()
            label.layer.borderColor = UIColor(red: 240,green: 240,blue: 240).CGColor
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            let label : UILabel = cell.viewWithTag(1) as! UILabel
            label.textColor = UIColor.whiteColor()
            label.backgroundColor =  UIColor(red: 52,green: 163,blue: 215)
            label.layer.borderColor = UIColor(red: 52,green: 163,blue: 215).CGColor
            self.search(self.lastSearchText, facilityId: self.lastFacilityId, sectionName: self.lastSectionName, offset: indexPath.item * self.limit, limit: self.limit, newSearch: false)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            let label : UILabel = cell.viewWithTag(1) as! UILabel
            label.textColor = UIColor.blackColor()
            label.backgroundColor =  UIColor.whiteColor()
            label.layer.borderColor = UIColor(red: 240,green: 240,blue: 240).CGColor
        }
    }

        
    /* UI Actions */
    @IBAction func zoomInButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomIn(true)
        }
    }
    
    
    @IBAction func zoomOutButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomOut(true)
        }
    }
    
 
    @IBAction func prevButtonTapped(sender: AnyObject) {
        let selectedItems = paginator.indexPathsForSelectedItems()
        if selectedItems.count > 0 {
            let selectedIndexPath = (selectedItems[0] as! NSIndexPath)
            if selectedIndexPath.item > 0 {
                paginator.selectItemAtIndexPath(NSIndexPath(forItem: selectedIndexPath.item - 1, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                
                self.collectionView(paginator, didSelectItemAtIndexPath: NSIndexPath(forItem: selectedIndexPath.item - 1, inSection: 0))
                self.collectionView(paginator, didDeselectItemAtIndexPath: selectedIndexPath)
            }
        }
    }
    
    @IBAction func nextButtonTapped(sender: AnyObject) {
        let selectedItems = paginator.indexPathsForSelectedItems()
        if selectedItems.count > 0 {
            let selectedIndexPath = (selectedItems[0] as! NSIndexPath)
            if selectedIndexPath.item < self.pageCount - 1 {
                paginator.selectItemAtIndexPath(NSIndexPath(forItem: selectedIndexPath.item + 1, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                
                self.collectionView(paginator, didSelectItemAtIndexPath: NSIndexPath(forItem: selectedIndexPath.item + 1, inSection: 0))
                self.collectionView(paginator, didDeselectItemAtIndexPath: selectedIndexPath)
            }
        }
    }
    
    /*
    @IBAction func prevButtonDoubleTapped(sender: AnyObject) {
    }
    
    @IBAction func nextButtonDoubleTapped(sender: AnyObject) {
        let selectedItems = paginator.indexPathsForSelectedItems()
        if selectedItems.count > 0 {
            let selectedIndexPath = (selectedItems[0] as! NSIndexPath)
            if selectedIndexPath.item != self.pageCount - 1 {
                paginator.selectItemAtIndexPath(NSIndexPath(forItem: self.pageCount - 1, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                
                self.collectionView(paginator, didSelectItemAtIndexPath: NSIndexPath(forItem: self.pageCount - 1, inSection: 0))
                self.collectionView(paginator, didDeselectItemAtIndexPath: selectedIndexPath)
            }
        }
    }*/
    
    
    
    @IBAction func searchButtonClicked(sender: AnyObject) {
        if textFieldQuery.isBlank() {
            Utils.showAlert("Invalid Keyword", message: "Please enter a search keyword")
            return
        } else if count(textFieldQuery.trimmedText.utf16) < 3 {
            Utils.showAlert("Invalid Keyword", message: "Search keyword must be at least 3 characters long")
            return
        }
        
        self.lastSearchText = textFieldQuery.text
        
        // Get selected facility and section id
        self.lastFacilityId = ""
        if let selectedFacility = dropDownFacilities.getSelectedItem() {
            self.lastFacilityId = selectedFacility.tag as! String
        }
        
        self.lastSectionName = ""
        if let selectedSection = dropDownSections.getSelectedItem() {
            if dropDownSections.selectedIndex > 0 {
                self.lastSectionName = selectedSection.title as String
            }
        }
        
        self.tableSearchResultsLaunched = false
        self.searchResultsView.hidden = true
        self.textFieldQuery.toggleHighlightShadow(false)
        
        self.search(self.lastSearchText, facilityId: self.lastFacilityId, sectionName: self.lastSectionName, offset: 0, limit: self.limit, newSearch: true)
    }
}
