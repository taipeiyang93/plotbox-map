//
//  DeceasedRecord.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DeceasedRecordOnline {
    
    
    // GET
    class func search(session: LoginSession!, text: String, facilityId: String, sectionName: String, offset: Int, limit: Int, completionHandler: (Int?, NSArray?, NSError?) -> Void) {
        
        Alamofire.request(.GET, session.apiBaseUrl + searchDeceasedRecordsUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId, "search_text": text, "facility_id": facilityId, "section_name": sectionName, "offset": String(offset), "limit": String(limit)])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, nil, e)
                    return
                }
                
                var deceasedRecords: NSMutableArray = NSMutableArray()
                
                let totalRecords = json["total_deceased_records"].intValue
                if let jsonDeceasedRecords = json["deceased_records"].array {
                    for jsonDeceasedRecord in jsonDeceasedRecords
                    {
                        
                        if let deceasedRecordsData = jsonDeceasedRecord.dictionary {
                            let deceasedRecord = DeceasedRecord.managedInstance(CoreData.getTemporaryContext())
                            
                            deceasedRecord.deceasedRecordId = jsonDeceasedRecord["deceased_id"].stringValue
                            deceasedRecord.deceasedForename = jsonDeceasedRecord["deceased_forename"].stringValue
                            deceasedRecord.deceasedSurname = jsonDeceasedRecord["deceased_surname"].stringValue
                            deceasedRecord.deceasedDate = jsonDeceasedRecord["deceased_date"].stringValue.toPlotBoxDate
                            
                            // Get unmanaged Plot class
                            deceasedRecord.plot = Plot.managedInstance(CoreData.getTemporaryContext())
                            deceasedRecord.plot.latitude = jsonDeceasedRecord["latitude"].doubleValue
                            deceasedRecord.plot.longitude = jsonDeceasedRecord["longitude"].doubleValue
                            deceasedRecord.plot.plotId = jsonDeceasedRecord["plot_id"].stringValue
                            deceasedRecord.plot.plotNumber = jsonDeceasedRecord["plot_number"].stringValue
                            deceasedRecord.plot.sectionName = jsonDeceasedRecord["section_name"].stringValue
                            deceasedRecord.plot.row = jsonDeceasedRecord["row"].stringValue
                            deceasedRecord.plot.remainingCapacity = jsonDeceasedRecord["remaining_capacity"].intValue
                            deceasedRecord.plot.totalCapacity = jsonDeceasedRecord["total_capacity"].intValue
                            
                            deceasedRecords.addObject(deceasedRecord)
                        }
                    }
                }
                
                completionHandler(totalRecords, deceasedRecords, nil)
        })
        
    }
    
    // GET
    class func getById(session: LoginSession!, deceasedRecordId: String, completionHandler: (DeceasedRecord?, NSError?) -> Void) {
        
        Alamofire.request(.GET, session.apiBaseUrl + viewDeceasedRecordUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId, "deceased_id": deceasedRecordId])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
        
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
    
                var deceasedRecord: DeceasedRecord = DeceasedRecord.managedInstance(CoreData.getTemporaryContext())
                
                // Deceased record details
                deceasedRecord.deceasedRecordId = json["deceased_record"]["deceased_id"].stringValue
                deceasedRecord.deceasedTitle = json["deceased_record"]["deceased_title"].stringValue
                deceasedRecord.deceasedForename = json["deceased_record"]["deceased_forename"].stringValue
                deceasedRecord.deceasedSurname = json["deceased_record"]["deceased_surname"].stringValue
                deceasedRecord.deceasedDate = json["deceased_record"]["deceased_date"].stringValue
                deceasedRecord.religionDenomination = json["deceased_record"]["religion_denomination"].stringValue
                deceasedRecord.occupation = json["deceased_record"]["occupation"].stringValue
                deceasedRecord.gender = json["deceased_record"]["sex"].stringValue
                deceasedRecord.maritalStatus = json["deceased_record"]["marital_status"].stringValue
                deceasedRecord.ageYears = json["deceased_record"]["age_at_death_years"].stringValue
                deceasedRecord.ageDays = json["deceased_record"]["age_at_death_days"].stringValue
                deceasedRecord.ageWeeks = json["deceased_record"]["age_at_death_weeks"].stringValue
                deceasedRecord.ageMonths = json["deceased_record"]["age_at_death_months"].stringValue
                deceasedRecord.funeralType = json["deceased_record"]["funeral_type"].stringValue
                deceasedRecord.burialType = json["deceased_record"]["burial_type"].stringValue
                deceasedRecord.notes = json["deceased_record"]["notes"].stringValue
                deceasedRecord.isProblematic = json["deceased_record"]["is_problematic"].stringValue == "1"
                deceasedRecord.placeOfDeath = json["deceased_record"]["place_of_death"].stringValue
                deceasedRecord.lastResidence = json["deceased_record"]["last_residence"].stringValue
                deceasedRecord.lastResidenceAddress1 = json["deceased_record"]["last_residence_address_line_1"].stringValue
                deceasedRecord.lastResidenceAddress2 = json["deceased_record"]["last_residence_address_line_2"].stringValue
                deceasedRecord.lastResidenceTown = json["deceased_record"]["last_residence_town"].stringValue
                deceasedRecord.lastResidenceCountry = json["deceased_record"]["last_residence_county"].stringValue
                deceasedRecord.lastResidencePostcode = json["deceased_record"]["last_residence_postcode"].stringValue
                deceasedRecord.personManagingFuneral = json["deceased_record"]["person_managing_funeral"].stringValue
                deceasedRecord.registrarName = json["deceased_record"]["name_of_registrar"].stringValue
                deceasedRecord.burialDate = json["deceased_record"]["burial_date"].stringValue.toPlotBoxDate
                
                // Plot details
                deceasedRecord.plot = Plot.managedInstance(CoreData.getTemporaryContext())
                deceasedRecord.plot.depthRemainingFeet = json["deceased_record"]["depth_remaining_feet"].stringValue
                deceasedRecord.plot.depthRemainingInches = json["deceased_record"]["depth_remaining_inches"].stringValue
                deceasedRecord.plot.depthRemainingString = json["deceased_record"]["depth_remaining_string"].stringValue
                
                deceasedRecord.plot.sectionName = json["deceased_record"]["section_name"].stringValue
                deceasedRecord.plot.row = json["deceased_record"]["row"].stringValue
                deceasedRecord.plot.plotNumber = json["deceased_record"]["plot_number"].stringValue
                deceasedRecord.plot.plotId = json["deceased_record"]["plot_id"].stringValue
                deceasedRecord.plot.primaryImage = json["deceased_record"]["primary_image_id"].stringValue
                deceasedRecord.plot.facilityName = json["deceased_record"]["facility_name"].stringValue
                deceasedRecord.plot.deedOwnerForename = json["deceased_record"]["deed_owner_forename"].stringValue
                deceasedRecord.plot.deedOwnerSurname = json["deceased_record"]["deed_owner_surname"].stringValue
                deceasedRecord.plot.latitude = json["deceased_record"]["latitude"].doubleValue
                deceasedRecord.plot.longitude = json["deceased_record"]["longitude"].doubleValue
                
                deceasedRecord.plot.imageBaseUrl = json["image_base_path"].stringValue
                
                
                // Images of the Plot
                var images: NSMutableArray = NSMutableArray()
                if let plotImages = json["plot_images"].array {
                    for plotImage in plotImages
                    {
                        if let plotImageData = plotImage.dictionary {
                            if let imageId = plotImageData["image_id"] {
                                let image = PlotImage.managedInstance(CoreData.getTemporaryContext())
                                image.filename = imageId.stringValue
                                image.plot = deceasedRecord.plot

                                if imageId.stringValue == deceasedRecord.plot.primaryImage {
                                    images.insertObject(image, atIndex: 0) // Insert primary image at the top
                                } else {
                                    images.addObject(image)
                                }
                            }
                        }
                    }
                }
                
                deceasedRecord.plot.images = NSSet(array: images as [AnyObject])
                completionHandler(deceasedRecord, nil)
        })
        
    }
    
    // POST
    class func save(session: LoginSession!, deceasedRecord: DeceasedRecord, completionHandler: (Bool?, NSError?) -> Void) {
        
        Alamofire.request(.POST, session.apiBaseUrl + editDeceasedRecordUrl,
            parameters: ["access_token": session.token, "company_identifier": session.companyId,
                "deceased_id": deceasedRecord.deceasedRecordId,
                "age_at_death_years": deceasedRecord.ageYears,
                "age_at_death_days": deceasedRecord.ageDays,
                "age_at_death_weeks": deceasedRecord.ageWeeks,
                "age_at_death_months": deceasedRecord.ageMonths,
                "deceased_title": deceasedRecord.deceasedTitle,
                "deceased_surname": deceasedRecord.deceasedSurname,
                "deceased_forename": deceasedRecord.deceasedForename,
                "religion_denomination": deceasedRecord.religionDenomination,
                "occupation": deceasedRecord.occupation,
                "sex": deceasedRecord.gender,
                "marital_status": deceasedRecord.maritalStatus,
                "funeral_type": deceasedRecord.funeralType,
                "burial_type": deceasedRecord.burialType,
                "notes": deceasedRecord.notes,
                "is_problematic": deceasedRecord.isProblematic.boolValue ? "1" : "0",
                "place_of_death": deceasedRecord.placeOfDeath,
                "last_residence_address_line_1": deceasedRecord.lastResidenceAddress1,
                "last_residence_address_line_2": deceasedRecord.lastResidenceAddress2,
                "last_residence_town": deceasedRecord.lastResidenceTown,
                "last_residence_county": deceasedRecord.lastResidenceCountry,
                "last_residence_postcode": deceasedRecord.lastResidencePostcode,
                "deceased_date": deceasedRecord.deceasedDate,
                "burial_date": deceasedRecord.burialDate
            ]).responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                completionHandler(json["status"].stringValue == "success", nil)
        })
    }
    
}
