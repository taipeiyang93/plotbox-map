//
//  RecordDetailsViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/23/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import UIKit
import ArcGIS
import SwiftyJSON
import Alamofire
import WebImage

class RecordDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AGSMapViewLayerDelegate {
    
    /* IB Outlets */
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var tableDeceasedRecordDetails: UITableView!
    @IBOutlet weak var tablePlotDetails: UITableView!
    @IBOutlet weak var tablePlotImages: UITableView!
    @IBOutlet weak var deceasedFullName: UILabel!
    @IBOutlet weak var viewMapArea: UIView!
    
    /* Private members */
    private var mapManager: SearchMapManager?
    
    /* Internal Members */
    internal var deceasedRecord: DeceasedRecord?
    internal weak var facility: Facility?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
        
        
        self.viewMapArea.backgroundColor = UIColor(patternImage: UIImage(named: "view-deceased-pattern.jpg")!)
        
        tableDeceasedRecordDetails.delegate = self
        tablePlotDetails.delegate = self
        tablePlotImages.delegate = self
        
        tableDeceasedRecordDetails.dataSource = self
        tablePlotDetails.dataSource = self
        tablePlotImages.dataSource = self
        
        tableDeceasedRecordDetails.tableFooterView = UIView()
        tablePlotDetails.tableFooterView = UIView()
        
        if let deceasedRecord = self.deceasedRecord {
            deceasedFullName.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
            let deceasedRecordId: String = deceasedRecord.deceasedRecordId
            self.deceasedRecord = nil // #REF fix for iOS 8 bug: http://stackoverflow.com/questions/25793074/subtitles-of-uitableviewcell-wont-update
            
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading...")
            
            // Get more details
            DeceasedRecord.getById(deceasedRecordId, completionHandler: { (deceasedRecord, error) in
                hud.hide(true)
                if let error = error {
                    if error.domain == "unauthorized" {
                        if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                            sidebarVC.notifyTokenExpired()
                            return
                        }
                    }
                    Utils.showAlertWithCompletion(self, title: "Error", message: error.domain, okActionHandler: { (action: UIAlertAction!) -> Void in
                        if let navController = self.navigationController {
                            navController.popViewControllerAnimated(true)
                        }
                    })
                    return
                }
                
                if let deceasedRecord = deceasedRecord {
                    self.reloadData(deceasedRecord)
                }
                
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadData(deceasedRecord:DeceasedRecord) {
        self.deceasedFullName.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
        self.deceasedRecord = deceasedRecord
        self.tableDeceasedRecordDetails.reloadData()
        self.tablePlotDetails.reloadData()
        self.tablePlotImages.reloadData()
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let deceasedRecord = self.deceasedRecord {
            if (tableView.isEqual(tablePlotImages)) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("imageCell", forIndexPath: indexPath) as! UITableViewCell
                let image = deceasedRecord.plot.images.allObjects[indexPath.row] as! PlotImage
                var imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
                let hud = MBProgressHUD.showHUDAddedTo(imageView, animated: true, labelText: nil)
                imageView.sd_setImageWithURL(image.url, completed: { (image: UIImage!, error: NSError!, type: SDImageCacheType, url: NSURL!) -> Void in
                    hud.hide(true)
                })
                
                return cell
                
                // Do something
            } else if (tableView.isEqual(tablePlotDetails)) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
                
                switch (indexPath.row) {
                case 0:
                    cell.textLabel?.text = "DATE OF BURIAL"
                    cell.detailTextLabel?.text = deceasedRecord.burialDate
                case 1:
                    cell.textLabel?.text = "PLOT & CEMETERY"
                    var sectionLabelText: String = deceasedRecord.plot.sectionName
                    if !deceasedRecord.plot.row.isEmpty {
                        sectionLabelText += "/" +  deceasedRecord.plot.row
                    }
                    sectionLabelText += "/" + deceasedRecord.plot.plotNumber
                    
                    cell.detailTextLabel?.text = sectionLabelText + deceasedRecord.plot.facilityName
                case 2:
                    cell.textLabel?.text = "DEPTH REMAINING AFTER BURIAL"
                    cell.detailTextLabel?.text = deceasedRecord.plot.depthRemainingString
                case 3:
                    cell.textLabel?.text = "DEED OWNER"
                    cell.detailTextLabel?.text = deceasedRecord.plot.deedOwnerForename + " " + deceasedRecord.plot.deedOwnerSurname
                case 4:
                    cell.textLabel?.text = "PERSON MANAGING INTERMENT"
                    cell.detailTextLabel?.text = deceasedRecord.personManagingFuneral
                case 5:
                    cell.textLabel?.text = "NAME OF REGISTRAR"
                    cell.detailTextLabel?.text = deceasedRecord.registrarName
                default:
                    break
                }
                
                if cell.detailTextLabel!.text!.isEmpty {
                    cell.detailTextLabel?.text = " "
                }
                
                return cell
                
                
            } else if (tableView.isEqual(tableDeceasedRecordDetails)) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
                
                switch (indexPath.row) {
                case 0:
                    cell.textLabel?.text = "DIED"
                    cell.detailTextLabel?.text = deceasedRecord.deceasedDate + " (AGE: " + deceasedRecord.ageYears + " years)"
                case 1:
                    cell.textLabel?.text = "STATUS"
                    cell.detailTextLabel?.text = deceasedRecord.maritalStatus
                case 2:
                    cell.textLabel?.text = "RELIGION"
                    cell.detailTextLabel?.text = deceasedRecord.religionDenomination
                case 3:
                    cell.textLabel?.text = "OCCUPATION"
                    cell.detailTextLabel?.text = deceasedRecord.occupation
                case 4:
                    cell.textLabel?.text = "LAST PLACE OF RESIDENCE"
                    cell.detailTextLabel?.text = Utils.joinStringsWithSingleComma([
                        deceasedRecord.lastResidenceAddress1,
                        deceasedRecord.lastResidenceAddress2,
                        deceasedRecord.lastResidenceTown,
                        deceasedRecord.lastResidenceCountry,
                        deceasedRecord.lastResidencePostcode])
                case 5:
                    cell.textLabel?.text = "NOTES"
                    cell.detailTextLabel?.text = deceasedRecord.notes
                default:
                    break
                    
                }
                
                if cell.detailTextLabel!.text!.isEmpty {
                    cell.detailTextLabel?.text = " "
                }
                
                return cell
            }
        }
        
        return UITableViewCell() // Return empty cell to supress error
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let deceasedRecord = self.deceasedRecord {
            if (tableView.isEqual(tablePlotImages)) {
                return deceasedRecord.plot.images.count
                
            } else if (tableView.isEqual(tablePlotDetails)) {
                
                return 6;
                
            } else if (tableView.isEqual(tableDeceasedRecordDetails)) {
                return 6;
            }
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (tableView.isEqual(tablePlotImages)) {
            
            dispatch_async(dispatch_get_main_queue(), {
                if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                    var imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
                    if imageView.image != nil {
                        let imageInfo: JTSImageInfo = JTSImageInfo()
                        imageInfo.image = imageView.image
                        imageInfo.referenceRect = imageView.frame
                        imageInfo.referenceView = imageView.superview
                        
                        let imageViewer: JTSImageViewController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle:JTSImageViewControllerBackgroundOptions.Blurred)
                        
                        imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
                    }
                }
                
            })
        } else {
            
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                if let textLabel = cell.textLabel {
                    if let detailTextLabel = cell.detailTextLabel {
                        var detailText = detailTextLabel.text!
                        if detailText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).isEmpty {
                            detailText = "(Not specified)"
                        }
                        Utils.showTooltip(textLabel.text!, message: detailText, targetView: detailTextLabel, inView: self.view)
                    }
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueEditDeceasedRecord" {
            if let deceasedRecord = self.deceasedRecord {
                let viewController: EditDeceasedRecordViewController = segue.destinationViewController as! EditDeceasedRecordViewController
                viewController.setFormData(DeceasedRecordForm(deceasedRecord: deceasedRecord))
            }
        } else if segue.identifier == "segueEditImages" {
            if let deceasedRecord = self.deceasedRecord {
                let viewController: EditImagesViewController = segue.destinationViewController as! EditImagesViewController
                viewController.plot = deceasedRecord.plot
            }
        }
    }
    
    @IBAction func saveToRecordDetailsViewController(unwindSegue: UIStoryboardSegue) {
        //let dr = self.deceasedRecord
        if let viewController: EditDeceasedRecordViewController = unwindSegue.sourceViewController as? EditDeceasedRecordViewController {
            if let deceasedRecordForm = viewController.getForm() {
                let deceasedRecord: DeceasedRecord = deceasedRecordForm.toModel()
                
                let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Saving...")
                
                // Save record
                DeceasedRecord.save(deceasedRecord, completionHandler: {(result, error) in
                    if let error = error {
                        if error.domain == "unauthorized" {
                            if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                    
                    }
                        }
                        return
                    }
                    
                    // Update view data
                    self.reloadData(deceasedRecord)
                    hud.hide(true)
                })
            }
        }
    }
    
    @IBAction func cancelToRecordDetailsViewController(unwindSegue: UIStoryboardSegue) {
        // Do Nothing
    }
}
