//
//  APIConfig.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/15/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation

// ESRI Credentials
let esriUsername = "plotboxmapping"
let esriPassword = "edinburgh1"

// URIs
let domainResolverUrl = "http://168.63.54.123/service/public/api/v2/get-domain"

let loginUrl = "v2/login"
let logoutUrl = "v2/logout"
let deceasedRecordsUrl = "v2/deceased-record"

let plotUrl = "v2/plot"
let burialsUrl = "v2/burials"

// Search APIs
let searchDeceasedRecordsUrl = "v2/deceased-records"
let searchPlotsUrl = "v2/plots"

let viewPlotUrl = "v2/view-plot"
let editPlotUrl = "v2/edit-plot"
let createPlotUrl = "v2/create-plot-with-deceased-record"

let addPlotImagesUrl = "v2/add-plot-images"

let viewDeceasedRecordUrl = "v2/view-deceased-record"
let editDeceasedRecordUrl = "v2/edit-deceased-record"


let facilitiesUrl = "v2/facilities"
let deleteImageUrl = "v2/delete-plot-images"
let offlineModeDataUrl = "v2/offline-mode-data"


let validateTokenUrl = "v3/validate-token"

// Other Configs
let loginGrantType = "password"
let loginClientId = "1"
let loginClientSecret = "ipad"

var currentLoginSession: LoginSession?
var isOfflineMode: Bool = false

