//
//  SidebarTableViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/6/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class SidebarTableViewController:  UIViewController, UITableViewDelegate, UITableViewDataSource  {
    @IBOutlet weak var menusTable: UITableView!

    let menus = [["Deceased Records", "icon_records"], ["Plots", "icon_plots"], ["Link Plots", "icon_link_plots"], ["Create Plots", "icon_create_plots"], ["Risk Assessment", "icon_risk_assessment"], ["Logout", "icon_logout"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menusTable.delegate = self
        self.menusTable.dataSource = self
        self.menusTable.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! UITableViewCell
        
        var image : UIImageView = cell.viewWithTag(1) as! UIImageView
        var label : UILabel = cell.viewWithTag(2) as! UILabel
        
        let row = indexPath.row
        label.text = menus[row][0]
        image.image = UIImage(named: menus[row][1])
        
        return cell
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.row == 3 && !isOfflineMode {
            Utils.showAlert("Invalid Mode", message: "The application needs to be running in offline mode in order to go to this screen.")
            return nil
        }
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.row) {
        case 0:
            self.performSegueWithIdentifier("segueSearchDeceasedRecords", sender: self)
            break
        case 1:
            self.performSegueWithIdentifier("segueSearchPlots", sender: self)
            break
        case 2:
            self.performSegueWithIdentifier("segueLinkPlots", sender: self)
            break
        case 3:
            self.performSegueWithIdentifier("segueCreatePlots", sender: self)
            break
        case 4:
            KeychainService.delete("token")
            KeychainService.delete("companyId")
            if let session = currentLoginSession {
                LoginSession.logout(session)
            }
            currentLoginSession = nil
            self.performSegueWithIdentifier("segueLoginViewController", sender: self)
            break
        default:
            break
        }
    }
    
    func notifyTokenExpired() {
        Utils.showAlert("Session Expired", message: "Your login session has expired. Please login again.")
        self.performSegueWithIdentifier("segueLoginViewController", sender: self)
    }

}
