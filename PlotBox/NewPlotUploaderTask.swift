//
//  NewPlotUploaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class NewPlotUploaderTask: AsyncTask {
    var session: LoginSession!
    var facilityId: String!
    var images: [(filename: String, plotId: String)] = []
    
    init(session: LoginSession!, facilityId: String!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
        self.facilityId = facilityId
    }
    
    override func start()
    {
        self.notifyProgress("Creating new plots...", progressCurrent: 0, progressTotal: 0)
        let request: NSFetchRequest = NSFetchRequest(entityName: NewPlot.entityName() as String)
        
        //let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "plotId", ascending: true)
        //request.sortDescriptors = [sortDescriptor]
        
        var error: NSError? = nil
        let plots: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
        
        dispatch_async(dispatch_get_main_queue()) {
            if plots.count > 0 {
                self.upload(0, plots: plots)
            } else {
                self.beforeNotifyCompleted()
            }
        }
        
        
    }
    
    func upload(index: Int, plots: NSArray) {
        let plot: NewPlot = plots[index] as! NewPlot
        NewPlot.upload(session, plot: plot, facilityId: self.facilityId) { (plotId: String?, error: NSError?) -> Void in
            if error == nil && plotId != nil {
                // Store the new plot id
                for image in plot.images.allObjects as! [NewPlotImage] {
                    self.images.append(filename: String(image.filename), plotId: plotId!)
                }
            }
            
            let count = plots.count
            if index + 1 < count {
                self.notifyProgress("Creating new plots... \(index + 1) of \(count)", progressCurrent: Double(index + 1), progressTotal: Double(count))
                
                self.upload(index + 1, plots: plots)
            } else {
                self.beforeNotifyCompleted()
            }
        }
    }
    
    func beforeNotifyCompleted() {
        NewPlotImageUploaderTask(session: currentLoginSession!, images: self.images,progress:  self.progress, completed: { () -> Void in
            
            self.notifyCompleted()
            
            }, failed: self.failed).start()
    }
    
}
