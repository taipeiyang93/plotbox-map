//
//  ClearCacheTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/5/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class ClearCacheTask: AsyncTask {
    
    override func start() {
        dispatch_async(dispatch_get_main_queue()) {
            self.notifyProgress( "Clearing cache...", progressCurrent: 0, progressTotal: 0)
            
            CoreData.resetContext()
            OfflineModeManager.deleteAllEntities(Plot.entityName() as String)
            OfflineModeManager.deleteAllEntities(DeceasedRecord.entityName() as String)
            OfflineModeManager.deleteAllEntities(Facility.entityName() as String)
            OfflineModeManager.deleteAllEntities(FacilitySection.entityName() as String)
            OfflineModeManager.deleteAllEntities(PlotImage.entityName() as String)
            OfflineModeManager.deleteAllEntities(NewPlot.entityName() as String)
            OfflineModeManager.deleteAllEntities(NewPlotImage.entityName() as String)
            self.notifyCompleted()
        }
    }
}
