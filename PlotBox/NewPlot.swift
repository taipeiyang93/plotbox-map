//
//  NewPlot.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData
import Alamofire

@objc(NewPlot)
class NewPlot: NSManagedObject {

    @NSManaged var deceasedDate: String
    @NSManaged var deceasedForename: String
    @NSManaged var deceasedSurname: String
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged var plotId: String
    @NSManaged var plotNumber: String
    @NSManaged var row: String
    @NSManaged var sectionName: String
    @NSManaged var totalCapacity: NSNumber
    @NSManaged var notes: String
    @NSManaged var images: NSSet
    
    class func entityName() -> NSString {
        return "NewPlot"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> NewPlot {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(NewPlot.entityName() as String, inManagedObjectContext: context!)!
        var newPlot = NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! NewPlot
        
        newPlot.plotId = NSUUID().UUIDString
        newPlot.deceasedDate = ""
        newPlot.deceasedForename = ""
        newPlot.deceasedSurname = ""
        newPlot.latitude = 0
        newPlot.longitude = 0
        newPlot.plotNumber = ""
        newPlot.row = ""
        newPlot.sectionName = ""
        newPlot.totalCapacity = 3
        newPlot.notes = ""
        
        return newPlot
    }
    
    
    class func upload(session: LoginSession!, plot: NewPlot, facilityId: String, completionHandler: (String?, NSError?) -> Void) {
        Alamofire.request(.POST, session.apiBaseUrl + createPlotUrl,
            parameters: ["access_token": session.token, "company_identifier": session.companyId,
                "section_name": plot.sectionName,
                "row": plot.row,
                "plot_number": plot.plotNumber,
                "latitude": plot.latitude,
                "longitude": plot.longitude,
                "deceased_forename": plot.deceasedForename,
                "deceased_surname": plot.deceasedSurname,
                "data_of_death": plot.deceasedDate,
                "facility_id": facilityId,
                "total_capacity": plot.totalCapacity,
                "notes": plot.notes
            ]).responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                completionHandler(json["plot_id"].stringValue, nil)
        })
    }
    
    
    class func plotExists(plotNumber: String, sectionName: String, row: String) -> Bool {
        
        if isOfflineMode {
            let request: NSFetchRequest = NSFetchRequest(entityName: NewPlot.entityName() as String)
            
            var predicates: Array<NSPredicate> = []
            
            if sectionName != "" {
                predicates.append(NSPredicate(format: "sectionName contains[c] %@", sectionName))
            }
            
            if !row.isEmpty {
                predicates.append(NSPredicate(format: "row == %@", row))
            }
            
            if !plotNumber.isEmpty {
                predicates.append(NSPredicate(format: "plotNumber == %@", plotNumber))
            }
            
            var compound = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
            request.predicate = compound
            
            var error: NSError? = nil
            let totalRecords: Int = CoreData.getManagedObjectContext()!.countForFetchRequest(request, error: &error)
            
            return totalRecords > 0
        }
        return false
    }
}
