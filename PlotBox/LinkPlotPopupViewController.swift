//
//  LinkPlotPopupViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 6/17/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import WebImage

enum LinkPlotDialogResult {
    case Cancel
    case Save
    case Move
}

class LinkPlotPopupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var imagePlotHeadstone: UIImageView!
    
    @IBOutlet weak var labelPlotTitle: UILabel!
    @IBOutlet weak var labelPlotCapacity: UILabel!
    
    @IBOutlet weak var textViewNotes: UITextView!
    @IBOutlet weak var textviewNotes: UITextView!
    @IBOutlet weak var labelDeedHolder: UILabel!
    @IBOutlet weak var labelPlotRemainingCapacity: UILabel!
    @IBOutlet weak var tableBurials: UITableView!
    
    var completionHandler: ((result: LinkPlotDialogResult!) -> Void)?
    
    private var plot: Plot?
    //internal weak var searchViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableBurials.dataSource = self
        self.tableBurials.delegate = self
        self.tableBurials.tableFooterView = UIView()
        // create tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapGesture:")
        
        // add it to the image view;
        self.imagePlotHeadstone.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        self.imagePlotHeadstone.userInteractionEnabled = true
        self.imagePlotHeadstone.backgroundColor = UIColor.blackColor()
        self.textViewNotes.text = ""
        
        self.textViewNotes.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).CGColor
        self.textViewNotes.layer.borderWidth = 1.0
        self.textViewNotes.layer.cornerRadius = 5
        
    }
    
    func tapGesture(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if let imageView = gesture.view as? UIImageView {  // if you subclass UIImageView, then change "UIImageView" to your subclass
            if imageView.image != nil {
                
                let imageInfo: JTSImageInfo = JTSImageInfo()
                imageInfo.image = imageView.image
                imageInfo.referenceRect = imageView.frame
                imageInfo.referenceView = imageView.superview
                
                
                let imageViewer: JTSImageViewController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle:JTSImageViewControllerBackgroundOptions.Blurred)
                
                imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDataIntoView(plotId : String, newLocation: (latitude: Double, longitude: Double), completionHandler: ((result: LinkPlotDialogResult!) -> Void)?) {
        self.completionHandler = completionHandler
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading data...")
        
        Plot.getById(plotId, completionHandler: { (plot, error) in
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            // Set new lat/long
            plot!.latitude = newLocation.latitude
            plot!.longitude = newLocation.longitude
            
            self.loadDataIntoView(plot!)
            hud.hide(true)
        })
    }
    
    private func reloadPreviewImage() {
        let hud = MBProgressHUD.showHUDAddedTo(self.imagePlotHeadstone, animated: true, labelText: nil)
        self.imagePlotHeadstone.sd_setImageWithURL(self.plot!.primaryImageUrl, completed: { (image: UIImage!, error: NSError!, type: SDImageCacheType, url: NSURL!) -> Void in
            hud.hide(true)
        })
    }
    
    private func loadDataIntoView(plot: Plot) {
        self.plot = plot
        self.tableBurials.reloadData()
        
        self.labelPlotTitle.text = plot.sectionName + "/" + plot.plotNumber
        
        self.labelPlotCapacity.text = "CAP " + String(Int(plot.totalCapacity))
        self.labelPlotRemainingCapacity.text = "REM " + String(Int(plot.remainingCapacity))
        self.labelDeedHolder.text = plot.deedOwnerForename + " " + plot.deedOwnerSurname
        self.textViewNotes.text = plot.notes
        self.imagePlotHeadstone.image = nil
        self.reloadPreviewImage()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let plot = self.plot {
            return plot.deceasedRecords.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BurialCell", forIndexPath: indexPath)
            as! UITableViewCell
        if let plot = self.plot {
            let deceasedRecord: DeceasedRecord = plot.deceasedRecords.allObjects[indexPath.row] as! DeceasedRecord
            cell.textLabel?.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
            cell.detailTextLabel?.text = deceasedRecord.deceasedDate
        }
        
        return cell
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    @IBAction func segmentedScreenChanged(sender: AnyObject) {
        textViewNotes.hidden = sender.selectedSegmentIndex != 0
        tableBurials.hidden = sender.selectedSegmentIndex != 1
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        //self.savePlot()
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            if let completionHandler = self.completionHandler {
                completionHandler(result: LinkPlotDialogResult.Cancel)
            }
        })
    }
    
    @IBAction func moveButtonTapped(sender: AnyObject) {
        //self.savePlot()
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            if let completionHandler = self.completionHandler {
                completionHandler(result: LinkPlotDialogResult.Move)
            }
        })
    }
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        if let plot = self.plot {
            plot.notes = self.textViewNotes.text
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Saving...")
            Plot.save(plot, completionHandler: { (result: Bool?, error: NSError?) -> Void in
                hud.hide(true)
                if let error = error {
                    Utils.showAlert("Error Saving", message: error.domain)
                }
                
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    if let completionHandler = self.completionHandler {
                        completionHandler(result: LinkPlotDialogResult.Save)
                    }
                })
            })
        }
    }
    @IBAction func addImageButtonTapped(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            dispatch_async(dispatch_get_main_queue()) {
                
                var image = UIImagePickerController()
                image.modalPresentationStyle = UIModalPresentationStyle.FullScreen
                image.delegate = self
                image.sourceType = UIImagePickerControllerSourceType.Camera
                image.allowsEditing = true
                self.presentViewController(image, animated: true, completion: nil)
            }
        } else {
            Utils.showAlert("Unsupported", message: "The attempted action is unsupported on this device.")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if let plot = self.plot {
            let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Uploading...", mode: MBProgressHUDModeAnnularDeterminate)
            
            // image data
            let imageData = UIImageJPEGRepresentation(image, 1)
            PlotImage.uploadImage(plot, imageData: imageData, progress: { (progressCurrent) -> Void in
                hud.progress = progressCurrent
                }, completed: { (error: NSError?) -> Void in
                    hud.hide(true)
                    if error == nil {
                        self.imagePlotHeadstone.image = image
                        Utils.showAlert("Success", message: "Image uploaded successfully!")
                    } else {
                        Utils.showAlert("Error", message: error!.localizedDescription)
                    }
            })
        }
    }
}
