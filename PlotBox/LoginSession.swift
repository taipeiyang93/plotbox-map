//
//  LoginSession.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/13/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginSession {
    internal var token: String
    internal var companyId: String
    internal var apiDomainPrefix: String = "api-uk"
    
    internal lazy var apiBaseUrl: String = {
        return "http://\(self.apiDomainPrefix).plotbox.io/service/public/api/"
        }()
    
    init(token: String!, companyId: String!, apiDomainPrefix: String!) {
        self.token = token
        self.companyId = companyId
        self.apiDomainPrefix = apiDomainPrefix
    }
    
    class func validateToken(session : LoginSession!, completionHandler: (Bool?, NSError?) -> Void) -> Void {
        Alamofire.request(.GET, session.apiBaseUrl + validateTokenUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId]).responseSwiftyJSON ({ (req: NSURLRequest, res: NSHTTPURLResponse?, json: JSON, error: NSError?) -> Void in
            
            
            if error == nil {
                if json == JSON.nullJSON {
                    completionHandler(nil, NSError(domain: "Server has returned an empty reponse.", code: 0, userInfo: nil))
                    return
                }
                
                if let status = json["status"].bool {
                    completionHandler(status, nil)
                    return
                }
            } else {
                // Connection error
                completionHandler(nil, NSError(domain: "Cannot connect to the server", code: 0, userInfo: nil))
            }
        })
    }
    
    class func storeToken(session : LoginSession!) {
        currentLoginSession = session
        KeychainService.save("token", data: session.token.toNSData)
        KeychainService.save("companyId", data: session.companyId.toNSData)
        KeychainService.save("apiDomainPrefix", data: session.apiDomainPrefix.toNSData)
    }
    
    class func logout(session : LoginSession!) {
        Alamofire.request(.GET, session.apiBaseUrl + logoutUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId])
    }
    
    class func login(username: String!, password: String!, companyId: String!, completionHandler: (LoginSession?, NSError?) -> Void) -> Void {
        
        Alamofire.request(.GET, domainResolverUrl,
            parameters: [
                "company_identifier" : companyId,
            ]
            ).responseSwiftyJSON ({ (req: NSURLRequest, res: NSHTTPURLResponse?, json: JSON, error: NSError?) -> Void in
                
                if json == JSON.nullJSON {
                    completionHandler(nil, NSError(domain: "Server has returned an empty reponse.", code: 0, userInfo: nil))
                    return
                }
                
                if let errorMessage = json["error_description"].string {
                    completionHandler(nil, NSError(domain: errorMessage, code: 0, userInfo: nil))
                    return
                }
                
                let apiDomainPrefix = json["domain"].stringValue
                if apiDomainPrefix == "" {
                    completionHandler(nil, NSError(domain: "Couldn't find API endpoints for the given company.", code: 0, userInfo: nil))
                    return
                }
            
                
                // Login request
                Alamofire.request(.POST, "http://\(apiDomainPrefix).plotbox.io/service/public/api/" + loginUrl,
                    parameters: ["username": username,
                        "password" : password,
                        "company_identifier" : companyId,
                        "grant_type" : loginGrantType,
                        "client_id" : loginClientId,
                        "client_secret" : loginClientSecret
                    ]
                    ).responseJSON() {
                        (_, _, data, error) in
                        
                        if error == nil {
                            if let errorMessage: String = data!.valueForKey("error_description") as? String {
                                completionHandler(nil, NSError(domain: errorMessage, code: 0, userInfo: nil))
                            } else {
                                if let accessToken: String = data!.valueForKey("access_token") as? String {
                                    // Logged In
                                    completionHandler(LoginSession(token: accessToken, companyId: companyId, apiDomainPrefix: apiDomainPrefix), nil)
                                } else {
                                    // Token was not returned
                                    completionHandler(nil, NSError(domain: "Invalid server configuration", code: 0, userInfo: nil))
                                }
                            }
                        } else {
                            // Connection error
                            completionHandler(nil, NSError(domain: "Cannot connect to the server", code: 0, userInfo: nil))
                        }
                }
                
            })
        
    }
}
