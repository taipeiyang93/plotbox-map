//
//  NewPlotImage.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

@objc(NewPlotImage)
class NewPlotImage: NSManagedObject {

    @NSManaged var filename: String
    @NSManaged var newPlot: NewPlot
    
    class func entityName() -> NSString {
        return "NewPlotImage"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> NewPlotImage {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(NewPlotImage.entityName() as String, inManagedObjectContext: context!)!
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! NewPlotImage
    }

}
