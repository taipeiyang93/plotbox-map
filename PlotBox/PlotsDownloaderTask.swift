//
//  PlotsDownloaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/3/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class PlotsDownloaderTask: AsyncTask {
    var session: LoginSession!
    var facilityId: String!
    // var totalFetchedPlots: Int = 0
    
    init(session: LoginSession!, facilityId: String!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
        self.facilityId = facilityId
    }
    
    override func start() {
        self.notifyProgress("Getting plots...", progressCurrent: 0, progressTotal: 0)
        let context: NSManagedObjectContext = CoreData.getManagedObjectContext()!
        self.fetch(0, limit: 100, context: context)
    }
    
    func fetchResult(offset: Double, limit: Double, totalPlots: Double, context: NSManagedObjectContext, error: NSError?) {
        // let totalPlots: Double = 200 // For debugging
        if let error = error {
            self.notifyFailed(error.localizedDescription)
        } else {
            // Save downloaded data for offline use
            CoreData.saveContext(nil)
            CoreData.resetContext()
            
            if offset + limit <= totalPlots {
                self.notifyProgress(String(format: "Getting plots... %.0f of %.0f", offset + limit, totalPlots), progressCurrent: offset + limit, progressTotal: totalPlots)
                self.fetch(offset + limit, limit: limit, context: context)
            } else {
                self.notifyCompleted()
            }
        }
    }
    
    func fetch(offset: Double, limit: Double, context: NSManagedObjectContext) {
        
        Alamofire.request(.GET, self.session.apiBaseUrl +  offlineModeDataUrl, parameters: ["access_token": self.session.token, "company_identifier": self.session.companyId, "facility_id": self.facilityId, "offset": offset, "limit": limit])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    self.notifyFailed(e.domain)
                    return
                }
                                
                if let jsonPlots = json["plots"].array {
                    for jsonPlot in jsonPlots
                    {
                        // self.totalFetchedPlots++
                        if let plotsData = jsonPlot.dictionary {
                            let plot = Plot.managedInstance(context)
                            
                            plot.modified = false
                            plot.plotId = jsonPlot["plot_id"].stringValue
                            plot.sectionName = jsonPlot["section_name"].stringValue
                            plot.row = jsonPlot["row"].stringValue
                            plot.plotNumber = jsonPlot["plot_number"].stringValue
                            plot.remainingCapacity = jsonPlot["remaining_capacity"].intValue
                            plot.totalCapacity = jsonPlot["total_capacity"].intValue
                            plot.purchased = jsonPlot["purchased"].stringValue
                            
                            plot.deedOwnerForename = jsonPlot["deed_owner_forename"].stringValue
                            plot.deedOwnerSurname = jsonPlot["deed_owner_surname"].stringValue
                            
                            plot.latitude = jsonPlot["latitude"].doubleValue
                            plot.longitude = jsonPlot["longitude"].doubleValue
                            
                            plot.depthRemainingFeet = jsonPlot["depth_remaining_feet"].stringValue
                            plot.depthRemainingInches = jsonPlot["depth_remaining_inches"].stringValue
                            plot.depthRemainingString = jsonPlot["depth_remaining_string"].stringValue
                            plot.notes = jsonPlot["notes"].stringValue
                            
                            // Deceased Records
                            var deceasedRecords: NSMutableSet = NSMutableSet()
                            for (index: String, subJson) in jsonPlot["deceased_records"] {
                                var deceasedRecord: DeceasedRecord = DeceasedRecord.managedInstance(CoreData.getManagedObjectContext())
                                
                                deceasedRecord.modified = false
                                deceasedRecord.deceasedRecordId = subJson["deceased_id"].stringValue
                                deceasedRecord.deceasedTitle = subJson["deceased_title"].stringValue
                                deceasedRecord.deceasedForename = subJson["deceased_forename"].stringValue
                                deceasedRecord.deceasedSurname = subJson["deceased_surname"].stringValue
                                deceasedRecord.deceasedDate = subJson["deceased_date"].stringValue.toPlotBoxDate
                                deceasedRecord.religionDenomination = subJson["religion_denomination"].stringValue
                                deceasedRecord.occupation = subJson["occupation"].stringValue
                                deceasedRecord.gender = subJson["sex"].stringValue
                                deceasedRecord.maritalStatus = subJson["marital_status"].stringValue
                                deceasedRecord.ageYears = subJson["age_at_death_years"].stringValue
                                deceasedRecord.ageDays = subJson["age_at_death_days"].stringValue
                                deceasedRecord.ageWeeks = subJson["age_at_death_weeks"].stringValue
                                deceasedRecord.ageMonths = subJson["age_at_death_months"].stringValue
                                deceasedRecord.funeralType = subJson["funeral_type"].stringValue
                                deceasedRecord.burialType = subJson["burial_type"].stringValue
                                deceasedRecord.notes = subJson["notes"].stringValue
                                deceasedRecord.isProblematic = subJson["is_problematic"].stringValue == "1"
                                deceasedRecord.placeOfDeath = subJson["place_of_death"].stringValue
                                deceasedRecord.lastResidence = subJson["last_residence"].stringValue
                                deceasedRecord.lastResidenceAddress1 = subJson["last_residence_address_line_1"].stringValue
                                deceasedRecord.lastResidenceAddress2 = subJson["last_residence_address_line_2"].stringValue
                                deceasedRecord.lastResidenceTown = subJson["last_residence_town"].stringValue
                                deceasedRecord.lastResidenceCountry = subJson["last_residence_county"].stringValue
                                deceasedRecord.lastResidencePostcode = subJson["last_residence_postcode"].stringValue
                                deceasedRecord.personManagingFuneral = subJson["person_managing_funeral"].stringValue
                                deceasedRecord.registrarName = subJson["name_of_registrar"].stringValue
                                deceasedRecord.burialDate = subJson["burial_date"].stringValue.toPlotBoxDate
                                
                                deceasedRecords.addObject(deceasedRecord)
                            }
                            plot.deceasedRecords = deceasedRecords
                            
                            var images: NSMutableSet = NSMutableSet()
                            for (index: String, subJson) in jsonPlot["images"] {
                                let image = PlotImage.managedInstance(CoreData.getManagedObjectContext())
                                //image.filename = imageId.stringValue
                                image.plot = plot
                                image.availableOffline = false
                                images.addObject(image)
                            }
                            
                            plot.images = images
                        }
                    }
                }
                
                let totalPlots = json["total_plots"].doubleValue
                self.fetchResult(offset, limit: limit, totalPlots: totalPlots, context: context, error: nil)
        })
        
    }
    
}
