//
//  Setting.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/4/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

@objc(Setting)
class Setting: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var value: String

    class func entityName() -> NSString {
        return "Setting"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> Setting {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(Setting.entityName() as String, inManagedObjectContext: context!)!
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! Setting
    }
    
    class func getSetting(name: String, context: NSManagedObjectContext!) -> Setting? {
        let request: NSFetchRequest = NSFetchRequest(entityName: Setting.entityName() as String)
        
        let predicate = NSPredicate(format: "name == %@", name)
        request.predicate = predicate
        
        var error: NSError? = nil
        let results: NSArray = context.executeFetchRequest(request, error: &error)!
        
        if (error == nil && results.count > 0) {
            return results[0] as? Setting
        }
        return nil
    }
    
    
    class func getSettingValue(name: String) -> String? {
        if let setting = Setting.getSetting(name, context: CoreData.getManagedObjectContext()!) {
            return setting.value
        }
        return nil
    }
    
    class func saveSettingValue(name: String, value: String) -> Void {

        // Attempt to find existing
        var setting: Setting? = Setting.getSetting(name, context: CoreData.getManagedObjectContext()!)
        if setting == nil {
            // Create new
            setting = Setting.managedInstance(CoreData.getManagedObjectContext()!)
        }
        
        setting!.name = name
        setting!.value = value
        CoreData.saveContext(nil)
    }

}
