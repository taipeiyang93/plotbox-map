//
//  ImageUploaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/6/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class ImageUploaderTask: AsyncTask {
    var session: LoginSession!
    var images: NSArray!
    let totalRetries = 3
    let ofm: OfflineModeManager = OfflineModeManager.sharedInstance
    
    init(session: LoginSession!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.images = NSArray() // Initialize with empty array
        self.session = session
    }
    
    override func start()
    {
        self.notifyProgress("Uploading images...", progressCurrent: 0, progressTotal: 0)
        
        dispatch_async(dispatch_get_main_queue()) {
            let request: NSFetchRequest = NSFetchRequest(entityName: PlotImage.entityName() as String)
            let predicate = NSPredicate(format: "availableOffline == 1")
            request.predicate = predicate
            
            var error: NSError? = nil
            if let images = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error) {
                self.images = images
            }
            
            if self.images.count > 0 {
                self.upload(0, retry: 0)
            } else {
                self.notifyCompleted()
            }
        }
        
    }
    
    func upload(index: Int, retry: Int) {
        let count = self.images.count
        let plotImage: PlotImage = self.images[index] as! PlotImage
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
        let imageFilePath = documentsPath.stringByAppendingPathComponent(plotImage.filename)
        
        // Closure to check if all the images have been upload
        let cleanupAndRecurse = { () -> () in
            plotImage.availableOffline = false
            
            // Delete the temporary image file
            var error: NSError?
            NSFileManager.defaultManager().removeItemAtPath(imageFilePath, error: &error)
            
            if index + 1 < count {
                self.upload(index + 1, retry: 0)
            } else {
                self.notifyCompleted()
            }
        }
        
        let manualRetry = { (message: String) -> () in
            self.ofm.showRetryDialog("Unable to upload image" + (retry > 0 ? " (Retry #\(retry))" : ""), message: message, result: { (retryDialogResult: OfflineModeManager.RetryDialogResult) -> Void in
                
                switch(retryDialogResult) {
                case OfflineModeManager.RetryDialogResult.Retry:
                        self.upload(index, retry: retry + 1) // Retry uploading
                        break;
                case OfflineModeManager.RetryDialogResult.Skip:
                        cleanupAndRecurse() // Skip and continue with next
                        break;
                default:
                    self.notifyFailed("Stopped by the user.") // Stop the process for now
                }
            })
        }
        
        if (retry == 0) {
            self.notifyProgress("Uploading images... \(index + 1) of \(count)", progressCurrent: 0, progressTotal: 0)
            
        } else {
            self.notifyProgress("Uploading images (Retry # \(retry))... \(index + 1) of \(count)", progressCurrent: 0, progressTotal: 0)
        }
        
        if let imageData =  NSData(contentsOfFile: imageFilePath) {
            PlotImageOnline.uploadImage(self.session, plotId: plotImage.plot.plotId, imageData: imageData, progress: { (progressCurrent) -> Void in
                if (retry == 0) {
                    self.notifyProgress(String(format: "Uploading images... %d of %d : %.0f%%", index + 1, count, progressCurrent * 100), progressCurrent: Double(progressCurrent), progressTotal: 1)
                } else {
                    self.notifyProgress(String(format: "Uploading images (Retry # \(retry))... %d of %d : %.0f%%", index + 1, count, progressCurrent * 100), progressCurrent: Double(progressCurrent), progressTotal: 1)
                }
                
                }, completed: { (imageId, error) -> Void in
                    
                    // Is server response good
                    if (imageId == nil) {
                        manualRetry("Error uploading image: \(index + 1) of \(count)")
                    } else {
                        println("ImageId: \(imageId)")
                        return cleanupAndRecurse()
                    }
            })
        } else {
            manualRetry("Error uploading image (Image file missing): \(index + 1) of \(count)")
        }
    }
    
    override func notifyCompleted() {
        CoreData.saveContext(nil) // Save uploaded plots flags
        super.notifyCompleted()
    }
    
    override func notifyFailed(message: String) {
        CoreData.saveContext(nil) // Save uploaded plots flags
        super.notifyFailed(message)
    }
    
}
