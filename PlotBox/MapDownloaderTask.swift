//
//  MapDownloaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/21/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import ArcGIS

class MapDownloaderTask: AsyncTask {
    private var tileCacheTask:AGSExportTileCacheTask!
    var session: LoginSession!
    var facility: Facility!
    
    init(session: LoginSession!, facility: Facility!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
        self.facility = facility
        
    }
    
    override func start() {
        
        self.notifyProgress("Starting, please wait...", progressCurrent: 0, progressTotal: 0)
        // Check if map already exist
        let mapFile: String? = MapCache.getMapFile(self.facility.facilityId, companyId: self.session.companyId, context: CoreData.getManagedObjectContext())
        
        if let mapFile = mapFile {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
            let mapFilePath = documentsPath.stringByAppendingPathComponent(mapFile)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(mapFilePath)) {
                self.notifyCompleted()
                return
            }
        }
        
        self.tileCacheTask = AGSExportTileCacheTask(URL: NSURL(string: self.facility.mapUri), credential: AGSCredential(user: esriUsername, password: esriPassword, authenticationType: AGSAuthenticationType.Token))
        self.tileCacheTask.loadCompletion = {(error) in
            let strongSelf = self
            if error != nil {
                self.notifyFailed(error.localizedDescription)
                return
            }
            // Tile cache task loaded
            let params = AGSExportTileCacheParams(levelsOfDetail: MapDownloaderTask.getAllLevels(strongSelf.tileCacheTask.tiledServiceInfo.tileInfo.lods as! [AGSLOD]), areaOfInterest:strongSelf.tileCacheTask.tiledServiceInfo.fullEnvelope)
            
            self.tileCacheTask.exportTileCacheWithParameters(params, downloadFolderPath: nil, useExisting: true, status: { (status, userInfo) -> Void in
                var progressMessage: String = "\(AGSResumableTaskJobStatusAsString(status)), \(userInfo)..."
                var progressCurrent: Double = 0
                var progressTotal: Double = 0
                
                if userInfo != nil {
                    
                    let allMessages =  userInfo["messages"] as? [AGSGPMessage]
                    
                    if status == .FetchingResult {
                        let totalBytesDownloaded = userInfo["AGSDownloadProgressTotalBytesDownloaded"] as? Double
                        let totalBytesExpected = userInfo["AGSDownloadProgressTotalBytesExpected"] as? Double
                        if totalBytesDownloaded != nil && totalBytesExpected != nil {
                            progressMessage = String(format: "Downloading... (%.2f MB of %.2f MB)", totalBytesDownloaded!/1048576, totalBytesExpected!/1048576)
                            progressCurrent = totalBytesDownloaded!
                            progressTotal = totalBytesExpected!
                        }
                    }
                    else if allMessages != nil && allMessages!.count > 0 {
                        progressMessage = String("Downloading...")
                        progressCurrent = 0
                        progressTotal = 0
                    }
                    
                    
                    self.notifyProgress(progressMessage, progressCurrent: progressCurrent, progressTotal:progressTotal)
                }
                }) { (localTiledLayer: AGSLocalTiledLayer!,error) -> Void in
                    if error != nil {
                        self.notifyFailed(error.localizedDescription)
                        return
                    }
                    
                    MapCache.insertData(self.facility.facilityId, companyId: self.session.companyId, mapFile: localTiledLayer.cachePath.lastPathComponent, context: CoreData.getManagedObjectContext()!)
                    
                    self.notifyProgress("Download Complete", progressCurrent: 0, progressTotal:0)
                    self.notifyCompleted()
            }
            
        }
        
    }
    
    class func getAllLevels(fromLODs:[AGSLOD]) -> [UInt] {
        var levels = [UInt]()
        for LOD  in fromLODs {
            levels.append(LOD.level)
        }
        return levels
    }
}
