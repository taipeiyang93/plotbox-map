//
//  PlotImage.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/6/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData
import Alamofire

@objc(PlotImage)
class PlotImage: NSManagedObject {
    
    @NSManaged var filename: String
    @NSManaged var plot: Plot
    @NSManaged var availableOffline: NSNumber
    
    lazy var url: NSURL = {
        if isOfflineMode {
            if self.availableOffline == 1 {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
                return NSURL(fileURLWithPath: documentsPath.stringByAppendingPathComponent(self.filename))!
            } else {
                return NSBundle.mainBundle().URLForResource("offline-mode", withExtension: "jpg")!
            }
        } else {
            return NSURL(string: self.plot.imageBaseUrl + "/" + self.plot.plotId + "/" + self.filename + ".jpg")!
        }
        }()
    
    class func entityName() -> NSString {
        return "PlotImage"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> PlotImage {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(PlotImage.entityName() as String, inManagedObjectContext: context!)!
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! PlotImage
    }
    
    class func uploadImage(plot: Plot, imageData: NSData, progress: ((progressCurrent: Float)->Void)?, completed: ((error: NSError?)->Void)?) {
        
        if let session = currentLoginSession {
            if isOfflineMode {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
                let imageFileName = "\(NSUUID().UUIDString).jpg"
                
                //let imageData = UIImageJPEGRepresentation(image, 1)
                imageData.writeToFile(documentsPath.stringByAppendingPathComponent(imageFileName), atomically: true)
                
                let images: NSMutableSet = plot.images.mutableCopy() as! NSMutableSet
                let image = PlotImage.managedInstance(CoreData.getManagedObjectContext())
                image.filename = imageFileName
                image.plot = plot
                image.availableOffline = true
                images.addObject(image)
                plot.images = images
                
                CoreData.saveContext({ (success: Bool?, error: NSError?) -> Void in
                    if let completed = completed {
                        completed(error: error)
                    }
                })
            } else {
                
                PlotImageOnline.uploadImage(session, plotId: plot.plotId, imageData: imageData, progress: progress, completed: { (imageId, error) -> Void in
                    if let imageId = imageId {
                        let images: NSMutableSet = plot.images.mutableCopy() as! NSMutableSet
                        let image = PlotImage.managedInstance(CoreData.getTemporaryContext())
                        image.filename = imageId
                        image.plot = plot
                        images.addObject(image)
                        plot.primaryImage = imageId
                        plot.images = images
                    }
                    
                    if let completed = completed {
                        completed(error: error)
                    }
                })
            }
        }
    }
    
    class func deleteImage(image: PlotImage, completionHandler: (String?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                let plot = image.plot
                CoreData.getManagedObjectContext()!.deleteObject(image)
                
                let images: NSMutableSet = plot.images.mutableCopy() as! NSMutableSet
                images.removeObject(image)
                plot.images = images
                
                completionHandler("", nil)
            } else {
                PlotImageOnline.deleteImage(session, image: image, completionHandler: completionHandler)
            }
        }
    }
}
