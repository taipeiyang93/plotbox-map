//
//  EditPlotsViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/7/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class EditPlotViewController: FXFormViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var editTableView: SHTableView!
    @IBOutlet weak var burialsTableView: SHTableView!
    @IBOutlet weak var segmentedScreenSelector: UISegmentedControl!
    private var lastScreenIndex = 0
    
    internal weak var plot: Plot?
    internal weak var facility: Facility?
    internal var plotForm: PlotForm?
    
    
    /*private func fillPlotNumberOptions(sectionName: String!) {
        if let plot = self.plot {
            let sectionId = PlotForm.sectionIdFromName(sectionName, facility: self.facility!)
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Getting available plots...")
            Plot.getUnusedPlots(self.facility!.facilityId, facilitySectionId: sectionId, completionHandler: { (results, error) -> Void in
                hud.hide(true)
                if let error = error {
                    if error.domain == "unauthorized" {
                        if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                    
                    }
                    }
                    return
                }
                
                let plotNumberOptions: NSMutableArray = []
                if let results = results {
                    if results.count > 0 {
                        
                        
                        for plot in results as [Plot] {
                            plotNumberOptions.addObject(plot.plotNumber)
                        }
                    }
                }
                if sectionName == plot.sectionName {
                    plotNumberOptions.addObject(plot.plotNumber)
                } else if plotNumberOptions.count == 0 {
                    Utils.showAlert("Unable to select", message: "No unsused plots are available in this section.")
                    self.plotForm!.sectionName = plot.sectionName
                    plotNumberOptions.addObject(plot.plotNumber)
                }
                
                self.plotForm!.plotNumber = plotNumberOptions.objectAtIndex(0) as String
                self.plotForm!.plotNumberOptions = plotNumberOptions
                self.formController.form = self.plotForm
                self.editTableView.reloadData()
            })
        }
    }
    
    func sectionNameChanged() {
        if let plotForm = self.plotForm {
            self.fillPlotNumberOptions(plotForm.sectionName)
        }
    }*/
    
    override func viewDidLoad() {
        if self.plot == nil || self.facility == nil {
            Utils.showAlert("Error", message: "Invalid parameters")
            if let navController = self.navigationController {
                navController.popViewControllerAnimated(true)
            }
            return
        }
        
        let plot = self.plot!
        let facility = self.facility!
        self.formController.tableView = editTableView
        self.view.backgroundColor = editTableView.backgroundColor
        super.viewDidLoad()
        
        self.burialsTableView.dataSource = self
        self.burialsTableView.delegate = self
        
        let plotId: String = plot.plotId
        self.plot = nil // #REF fix for iOS 8 bug: http://stackoverflow.com/questions/25793074/subtitles-of-uitableviewcell-wont-update
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading...")
        
        // Get more details
        Plot.getById(plotId, completionHandler: { (plot, error) in
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                    
                    }
                }
                return
            }
            
            if let plot = plot {
                self.plot = plot
                self.burialsTableView.reloadData()
                hud.hide(true)
                
                self.plotForm = PlotForm(plot: plot, facility: facility)
                self.formController.form = self.plotForm
                self.editTableView.reloadData()
                //self.fillPlotNumberOptions(plot.sectionName)
            }
        })
        
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        segmentedScreenSelector.selectedSegmentIndex = lastScreenIndex
        editTableView.hidden = segmentedScreenSelector.selectedSegmentIndex != 0
        burialsTableView.hidden = segmentedScreenSelector.selectedSegmentIndex != 1
    }
    
    @IBAction func segmentedScreenChanged(sender: AnyObject) {
        editTableView.hidden = sender.selectedSegmentIndex != 0
        burialsTableView.hidden = sender.selectedSegmentIndex != 1
        
        if sender.selectedSegmentIndex == 2 {
            self.performSegueWithIdentifier("segueEditImages", sender: sender)
        } else {
            lastScreenIndex = sender.selectedSegmentIndex
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        let plotForm: PlotForm = self.formController.form as! PlotForm
        
        if plotForm.totalCapacity.toInt() == nil {
            Utils.showAlert("Validation Error(s)", message: "- Total Capacity field should be an integer")
            return
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Saving...")
        let plot: Plot = plotForm.toModel()
        
        // Save record
        Plot.save(plot, completionHandler: {(result, error) in
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                    
                    }
                }
                return
            }
            
            hud.hide(true)
            if let navController = self.navigationController {
                navController.popViewControllerAnimated(true)
            }
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueEditImages" {
            if let plot = self.plot {
                let viewController: EditImagesViewController = segue.destinationViewController as! EditImagesViewController
                viewController.plot = plot
            }
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let plot = self.plot {
            if section == 0 {
                return plot.deceasedRecords.count
            } else if section == 1 {
                return 1
            }
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        if let plot = self.plot {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("BurialCell", forIndexPath: indexPath)
                    as! UITableViewCell
                let deceasedRecord: DeceasedRecord = plot.deceasedRecords.allObjects[indexPath.row] as! DeceasedRecord
                cell.textLabel?.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
                cell.detailTextLabel?.text = deceasedRecord.deceasedDate
                return cell
            } else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("DeedHistoryCell", forIndexPath: indexPath)
                   as! UITableViewCell
                cell.textLabel?.text = plot.deedOwnerForename + " " + plot.deedOwnerSurname
                return cell
            }
        }
        
        // Default cell
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Burial Records"
        } else if section == 1 {
            return "Deed History"
        }
        return nil
    }
}
