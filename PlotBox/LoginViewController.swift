//
//  ViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 2/25/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate, TTTAttributedLabelDelegate {
    
    
    
    @IBOutlet weak var contentBottomMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentTopMarginConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var errorMessageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var footerLabel: TTTAttributedLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Facility.clearCache()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        companyTextField.delegate = self
        
        let footerText : NSString = "View our Terms and Privacy Policy"
        footerLabel.text = footerText as String
        footerLabel.linkAttributes = [NSUnderlineStyleAttributeName  as NSString : NSUnderlineStyle.StyleSingle.rawValue,
            kCTForegroundColorAttributeName as NSString : UIColor(red: 44, green: 63, blue: 80).CGColor] as  [NSObject : AnyObject]
        
        footerLabel.addLinkToURL(NSURL(string: "action://show-terms"), withRange: footerText.rangeOfString("Terms"))
        footerLabel.addLinkToURL(NSURL(string: "action://show-policy"), withRange: footerText.rangeOfString("Privacy Policy"))
        footerLabel.delegate = self
    }
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        
        if let urlHost = url.host {
            switch(urlHost) {
            case "show-terms":
                self.performSegueWithIdentifier("segueTermsAndConditions", sender: self)
                break
            case "show-policy":
                self.performSegueWithIdentifier("seguePrivacyPolicy", sender: self)
                break
            default:
                break
            }
        }
        
    }
    
    func keyboardWillShow(sender: NSNotification) {
        contentTopMarginConstraint.constant = 100
        if let userInfo = sender.userInfo {
            if let keyboardHeight = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size.height {
                contentBottomMarginConstraint.constant += keyboardHeight
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        contentTopMarginConstraint.constant = 300
        if let userInfo = sender.userInfo {
            if let keyboardHeight = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size.height {
                contentBottomMarginConstraint.constant -= keyboardHeight
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func login() -> Void {
        self.errorMessageViewHeightConstraint.constant = 0
        if (usernameTextField.isBlank() || passwordTextField.text.isEmpty || companyTextField.isBlank()) {
            Utils.showAlert("Invalid Parameters", message: "Please enter your username, password and company identifier.")
            return
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Logging In...")
        LoginSession.login(usernameTextField.text, password: passwordTextField.text, companyId: companyTextField.text, completionHandler: {
            (session: LoginSession?, error: NSError?) in
            
            // Clear password field
            self.passwordTextField.text = ""
            
            // Remove the loading icon
            hud.hide(true)
            
            if let e = error {
                self.onLoginFailed(e.domain)
            } else if let session = session {
                self.onLoginSuccessful(session)
            }
        })
    }
    
    func onLoginFailed(errorMessage: String!) {
        self.errorMessageViewHeightConstraint.constant = 50
        self.errorMessageLabel.text = errorMessage
    }
    
    func onLoginSuccessful(session: LoginSession!) {
        LoginSession.storeToken(session)
        self.revealViewController().rearViewController.performSegueWithIdentifier("segueSearchDeceasedRecords", sender: self)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.login()
        return true
    }
    
    @IBAction func loginButtonPressed(sender: AnyObject) {
        self.login()
    }
}