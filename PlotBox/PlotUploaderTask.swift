//
//  PlotUploaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/4/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class PlotUploaderTask: AsyncTask {
    var session: LoginSession!
    
    init(session: LoginSession!, facilityId: String!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
    }
    
    override func start()
    {
        self.notifyProgress("Uploading plots...", progressCurrent: 0, progressTotal: 0)

        dispatch_async(dispatch_get_main_queue()) {
            let request: NSFetchRequest = NSFetchRequest(entityName: Plot.entityName() as String)
            let predicate = NSPredicate(format: "modified == 1")
            request.predicate = predicate
            
            let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "plotId", ascending: true)
            request.sortDescriptors = [sortDescriptor]
            
            var error: NSError? = nil
            let plots: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
            
            dispatch_async(dispatch_get_main_queue()) {
                if plots.count > 0 {
                    println(plots.count)
                    self.upload(0, plots: plots)
                } else {
                    self.notifyCompleted()
                }
            }
        }
        
    }
    
    func upload(index: Int, plots: NSArray) {
        let plot: Plot = plots[index] as! Plot
        PlotOnline.save(session, plot: plot) { (result: Bool!, error: NSError?) -> Void in
            plot.modified = !result // Set modified to false if the Plot has been uploaded now
            
            let count = plots.count
            if index + 1 < count {
                self.notifyProgress("Uploading plots... \(index + 1) of \(count)", progressCurrent: Double(index + 1), progressTotal: Double(count))
                self.upload(index + 1, plots: plots)
            } else {
                self.notifyCompleted()
            }
        }
    }
    
    override func notifyCompleted() {
        CoreData.saveContext(nil) // Save modified flags for uploaded plots
        super.notifyCompleted()
    }
}
