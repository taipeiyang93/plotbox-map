//
//  FacilitySection.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

@objc(FacilitySection)
class FacilitySection: NSManagedObject {

    @NSManaged var facilitySectionId: String
    @NSManaged var facilityId: String
    @NSManaged var name: String
    
    class func entityName() -> NSString {
        return "FacilitySection"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> FacilitySection {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(FacilitySection.entityName() as String, inManagedObjectContext: context!)!
        
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context!) as! FacilitySection
    }
}
