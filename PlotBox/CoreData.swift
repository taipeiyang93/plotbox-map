//
//  CoreData.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

class CoreData {
    
    class var sharedInstance: CoreData {
        struct Static {
            static var instance: CoreData?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = CoreData()
        }
        
        return Static.instance!
    }
    
    // MARK: - Core Data stack
    private lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.plotbox.PlotBox" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
        }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("PlotBox", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("PlotBox.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict as [NSObject : AnyObject])
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    private lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    private lazy var temporaryManagedObjectContext: NSManagedObjectContext? = {
        let temporaryContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        temporaryContext.parentContext = self.managedObjectContext
        return temporaryContext
        }()
    
    // MARK: - Core Data Saving support
    class func saveContext(completionHandler: ((Bool?, NSError?) -> Void)?) {
        CoreData.saveContext(CoreData.sharedInstance.managedObjectContext, completionHandler: completionHandler)
    }
    
    class func saveContext(context: NSManagedObjectContext?, completionHandler: ((Bool?, NSError?) -> Void)?) {
        
        if let moc = context {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Error saving
                if let completionHandler = completionHandler {
                    completionHandler(false, error)
                }
                return
            }
        }
        
        if let completionHandler = completionHandler {
            completionHandler(true, nil)
        }
    }
    
    class func resetContext() {
        if let context = CoreData.sharedInstance.managedObjectContext {
            context.reset()
        }
    }
    
    class func getManagedObjectContext() -> NSManagedObjectContext? {
        return CoreData.sharedInstance.managedObjectContext
    }
    
    class func getTemporaryContext() -> NSManagedObjectContext? {
        return CoreData.sharedInstance.temporaryManagedObjectContext
    }
}