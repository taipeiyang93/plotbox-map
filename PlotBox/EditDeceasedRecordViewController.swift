//
//  EditDeceasedRecordViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/31/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class EditDeceasedRecordViewController: FXFormViewController {
    
    /* IB Outlets */
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var editTableView: SHTableView!
    
    override func viewDidLoad() {
        self.formController.tableView = editTableView
        self.view.backgroundColor = editTableView.backgroundColor

        super.viewDidLoad()

        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFormData(deceasedRecordForm: DeceasedRecordForm) {
        self.formController.form = deceasedRecordForm
    }
    
    func getForm()->DeceasedRecordForm? {
        if let deceasedRecordForm = self.formController.form as? DeceasedRecordForm {
            return deceasedRecordForm
        }
        return nil
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if identifier == "segueSave" {
            if let deceasedRecordForm = self.formController.form as? DeceasedRecordForm {
                var errors: Array<String> = []
                if let dateOfDeath = deceasedRecordForm.dateOfDeath {
                    if dateOfDeath.compare(NSDate()) == NSComparisonResult.OrderedDescending {
                        errors.append(" - Date of death can not be set to a future date.")
                    }
                    
                    if let dateOfBurial = deceasedRecordForm.dateOfBurial {
                        if dateOfDeath.compare(dateOfBurial) == NSComparisonResult.OrderedDescending {
                            errors.append(" - Date of Burial should be after or on Date of Death.")
                        }
                    }
                }

                if errors.count > 0 {
                    Utils.showAlert("Validation Error(s)", message: "\n".join(errors))
                    return false
                }
            }
            
        }
        return true
    }


}
