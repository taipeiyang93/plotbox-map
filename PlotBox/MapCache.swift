//
//  MapCache.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

@objc(MapCache)
class MapCache: NSManagedObject {

    @NSManaged var companyId: String
    @NSManaged var facilityId: String
    @NSManaged var mapFile: String

    class func entityName() -> NSString {
        return "MapCache"
    }
    
    class func getMapFile(facilityId: String, companyId: String, context: NSManagedObjectContext!) -> String? {
        var request = NSFetchRequest(entityName: MapCache.entityName() as String)
        request.returnsObjectsAsFaults = false;
        
        let resultPredicate1 = NSPredicate(format: "facilityId = %@", facilityId)
        let resultPredicate2 = NSPredicate(format: "companyId = %@", companyId)
        
        var compound = NSCompoundPredicate.andPredicateWithSubpredicates([resultPredicate1, resultPredicate2])
        request.predicate = compound
        
        var error: NSError? = nil
        var results:NSArray = context.executeFetchRequest(request, error: &error)!
        
        if (error == nil && results.count > 0) {
            let mapCache: MapCache = results[0] as! MapCache
            return mapCache.mapFile
        }
        return nil
    }
    
    class func insertData(facilityId: String, companyId: String, mapFile: String, context : NSManagedObjectContext) -> Void {
        let mapCache = NSEntityDescription.insertNewObjectForEntityForName(self.entityName() as String, inManagedObjectContext:context) as! MapCache;
        mapCache.facilityId = facilityId
        mapCache.companyId = companyId
        mapCache.mapFile = mapFile
        
        CoreData.saveContext(nil)
    }

}
