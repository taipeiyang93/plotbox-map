//
//  SearchMapManager.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/13/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import ArcGIS

class SearchMapManager: MapManager {
    private var calloutViewController : CalloutViewController!
    internal weak var selectedPin: AGSGraphic?
    internal var movePinMode: Bool = false
    internal var showCallout: Bool = true
    
    override init(mapAreaView: UIView!, facility: Facility, touchDelegate: AGSMapViewTouchDelegate? = nil, calloutDelegate: AGSCalloutDelegate? = nil, layerDelegate: AGSMapViewLayerDelegate? = nil) {
        super.init(mapAreaView: mapAreaView, facility: facility, touchDelegate: touchDelegate, calloutDelegate: calloutDelegate, layerDelegate: layerDelegate)
        //
        let storyboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let frame = CGRect(x: 0, y: 0, width: 250, height: 280)
        self.calloutViewController = storyboard.instantiateViewControllerWithIdentifier("callout") as! CalloutViewController
        self.calloutViewController.view.frame = frame
        self.calloutViewController.view.clipsToBounds = true
        self.calloutViewController.searchViewController = Utils.getParentViewController(mapAreaView)
    }
    
    override func callout(callout: AGSCallout!, willShowForFeature feature: AGSFeature!, layer: AGSLayer!, mapPoint: AGSPoint!) -> Bool {
        if !self.showCallout {
            return false
        }
        
        if let mapView = self.mapView, graphic = feature as? AGSGraphic {
            if graphic.hasAttributeForKey("plot_id") {
                mapView.callout.customView = self.calloutViewController.view
                self.calloutViewController.loadDataIntoView(graphic.attributeForKey("plot_id") as! String)
                self.selectPlot(graphic.attributeForKey("plot_id") as! String)
                return true
            }
        }
        return false
    }
    
    func pinPlots(plots: NSArray) {
        self.pinsLayer.removeAllGraphics()
        for plot in plots as! [Plot] {
            let agsPoint = self.getAGSPoint(Double(plot.latitude), longitude: Double(plot.longitude))
            self.addPinToPoint(agsPoint, attributes: ["plot_id": plot.plotId, "total_capacity": Int(plot.totalCapacity), "remaining_capacity": Int(plot.remainingCapacity)])
        }
    }
    
    func pinDeceasedRecords (deceasedRecords: NSArray) {
        self.pinsLayer.removeAllGraphics()
        for deceasedRecord in deceasedRecords as! [DeceasedRecord] {
            let agsPoint = self.getAGSPoint(Double(deceasedRecord.plot.latitude), longitude: Double(deceasedRecord.plot.longitude))
            
            self.addPinToPoint(agsPoint, attributes: ["plot_id": deceasedRecord.plot.plotId, "total_capacity": Int(deceasedRecord.plot.totalCapacity), "remaining_capacity": Int(deceasedRecord.plot.remainingCapacity)])
        }
    }
    
    func zoomToPlot(plot : Plot, animated: Bool = true){
        if let mapView = self.mapView {
            mapView.zoomToScale(500, withCenterPoint: self.getAGSPoint(Double(plot.latitude), longitude: Double(plot.longitude)), animated: animated)
            self.selectPlot(plot.plotId)
        }
    }
    
    func selectPlot(plotId: String!) {
        
        if let selectedPin = self.selectedPin {
            let pinSymbol = AGSPictureMarkerSymbol()
            
            //pinSymbol.color = UIColor.blueColor()
            pinSymbol.image = UIImage(named: "icon_pin")
            
            
            // Callibrate the pin location so the pin point is at the center of the plot
            pinSymbol.offset = CGPoint(x: 5.8, y: 20)
            pinSymbol.leaderPoint = CGPoint(x: -5.8, y: -20)
            selectedPin.symbol = pinSymbol
        }
        
        for graphic in pinsLayer.graphics as! [AGSGraphic] {
            if graphic.hasAttributeForKey("plot_id") {
                if graphic.attributeForKey("plot_id") as! String == plotId {
                    let pinSymbol = AGSPictureMarkerSymbol()
                    let totalCapacity: Int = graphic.attributeForKey("total_capacity") as! Int
                    let remainingCapacity: Int = graphic.attributeForKey("remaining_capacity") as! Int
                    
                    if totalCapacity == remainingCapacity {
                        pinSymbol.image = UIImage(named: "icon_pin_green")
                    } else if remainingCapacity > 0 {
                        pinSymbol.image = UIImage(named: "icon_pin_blue")
                    } else {
                        pinSymbol.image = UIImage(named: "icon_pin_red")
                    }
                    
                    // Callibrate the pin location so the pin point is at the center of the plot
                    pinSymbol.offset = CGPoint(x: 5.8, y: 20)
                    pinSymbol.leaderPoint = CGPoint(x: -5.8, y: -20)
                    graphic.symbol = pinSymbol
                    self.selectedPin = graphic
                    return
                }
            }
        }
        self.selectedPin = nil
    }
    
    override func clearPins() {
        super.clearPins()
        self.selectedPin = nil
    }
    
    func moveSelectedPinTo(latitude: Double, longitude: Double)  {
        if let selectedPin = self.selectedPin {
            selectedPin.geometry = self.getAGSPoint(latitude, longitude: longitude)
        }
    }
    
    func getSelectedPinCoordinates()->(latitude: Double, longitude: Double) {
        if let selectedPin = self.selectedPin {
            if let mapView = self.mapView {
                let engine : AGSGeometryEngine = AGSGeometryEngine.defaultGeometryEngine()
                let gpsPoint : AGSPoint = engine.projectGeometry(selectedPin.geometry, toSpatialReference: AGSSpatialReference.wgs84SpatialReference()) as! AGSPoint
                return (gpsPoint.y + Double(facility.latitudeOffset), gpsPoint.x + Double(self.facility.longitudeOffset))
            }
        }
        return (0, 0)
    }

    func dismissCallout() -> Void {
        if let mapView = self.mapView {
            mapView.callout.dismiss()
        }
    }
}
