//
//  FacilityDownloaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/3/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class FacilityDownloaderTask: AsyncTask {
    var session: LoginSession!
    var facility: Facility!
    
    init(session: LoginSession!, facility: Facility!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
        self.facility = facility
    }
    
    override func start() {
        let context: NSManagedObjectContext = CoreData.getManagedObjectContext()!
        
        self.notifyProgress("Storing facility details..", progressCurrent: 0, progressTotal: 0)
        
        dispatch_async(dispatch_get_main_queue()) { // 2
            let managedFacility = Facility.managedInstance(CoreData.getManagedObjectContext())
            managedFacility.facilityId = self.facility.facilityId
            managedFacility.name = self.facility.name
            managedFacility.latitude = self.facility.latitude
            managedFacility.longitude = self.facility.longitude
            managedFacility.latitudeOffset = self.facility.latitudeOffset
            managedFacility.longitudeOffset = self.facility.longitudeOffset
            managedFacility.type = self.facility.type
            managedFacility.mapId = self.facility.mapId
            managedFacility.mapUri = self.facility.mapUri
            
            let sections: NSMutableSet = NSMutableSet()
            
            for facilitySection in self.facility.sections.allObjects as! [FacilitySection] {
                let managedFacilitySection: FacilitySection = FacilitySection.managedInstance(CoreData.getManagedObjectContext())
                managedFacilitySection.facilitySectionId = facilitySection.facilitySectionId
                managedFacilitySection.facilityId = facilitySection.facilityId
                managedFacilitySection.name = facilitySection.name
                sections.addObject(managedFacilitySection)
            }
            managedFacility.sections = sections

            var error: NSError? = nil
            if !context.save(&error) {
                // println(error?.localizedDescription)
            }
            
            self.notifyCompleted()
        }
    }
}
