//
//  AsyncTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/23/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation

class AsyncTask {
    internal var completed: (() -> Void)?
    internal var failed: ((message: String) -> Void)?
    internal var progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?
    
   init (progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        
        self.progress = progress
        self.completed = completed
        self.failed = failed
    }
    
    func notifyProgress(message: String, progressCurrent: Double, progressTotal: Double) {
        if let progress = self.progress {
            dispatch_async(dispatch_get_main_queue()) {
                progress(message: message, progressCurrent: progressCurrent, progressTotal: progressTotal)
            }
        }
    }
    
    func notifyCompleted() {
        if let completed = self.completed {
            dispatch_async(dispatch_get_main_queue()) {
                completed()
            }
        }
    
    }
    
    func notifyFailed(message: String) {
        if let failed = self.failed {
            dispatch_async(dispatch_get_main_queue()) {
                failed(message: message)
            }
        }
    }
    
    func start() {
    }
}