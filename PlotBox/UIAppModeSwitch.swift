//
//  UIAppModeSwitch.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/23/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class UIAppModeSwitch: UISwitch {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    private weak var parentViewController: UIViewController?
    
    override
    init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setOn(!isOfflineMode, animated: false)
        self.addTarget(self, action: "didChange:", forControlEvents: .ValueChanged)
    }
    
    
    private func showLoginDialog(title: String, message: String, completionHandler: (Bool) -> Void) -> Void {
        println("Login Dialog")
        Utils.showAlertWithLoginDialog(self.parentViewController!, title: title, message: message, okActionHandler: { (username: String, password: String) -> Void in
            
            let hud = MBProgressHUD.showHUDAddedTo(self.parentViewController!.view, animated: true, labelText: "Logging In...")
            LoginSession.login(username, password: password, companyId: currentLoginSession!.companyId, completionHandler: { (loginSession, error) -> Void in
                
                hud.hide(true)
                if let e = error {
                    Utils.showAlert("Error", message: e.domain)
                    return completionHandler(false)
                } else if let session = loginSession {
                    LoginSession.storeToken(loginSession)
                    return completionHandler(true)
                }
            })
            }, cancelActionHandler: { (Void) -> Void in
                return completionHandler(false)
        })
    }
    
    private func validateToken(completionHandler: (Bool) -> Void) -> Void {
        let hud = MBProgressHUD.showHUDAddedTo(self.parentViewController!.view, animated: true, labelText: "Validating login...")
        
        
        LoginSession.validateToken(currentLoginSession!, completionHandler: { (status: Bool?, error: NSError?) -> Void in
            hud.hide(true)
            
            if let error = error {
                Utils.showAlert("Error", message: error.localizedDescription)
                return completionHandler(false)
            }
            
            if status! {
                return completionHandler(true)
            } else {
                self.showLoginDialog("Token Expired", message: "Please login again to company: \(currentLoginSession!.companyId)", completionHandler: { (status: Bool) -> Void in
                    completionHandler(status);
                })
                
            }
        })
    }
    
    func didChange(sender:UISwitch!) {
        // Get the parent VC
        if self.parentViewController == nil {
            self.parentViewController = Utils.getParentViewController(self)
        }
        
        if let parentViewController = self.parentViewController {
            let offlineModeManager: OfflineModeManager = OfflineModeManager.sharedInstance
            offlineModeManager.setParent(parentViewController)
            
            if self.on {
                
                Utils.showAlertWithOkCancel(self.parentViewController, title: "Confirm Switch", message: "Are you sure you want to switch to online mode? Plotbox will send all data that have been modified to the server and then clear the local cache.", okActionHandler: { (action: UIAlertAction!) -> Void in
                    
                    self.validateToken({ (valid: Bool) -> Void in
                        if(valid) {
                            Facility.list({ (facilities: NSArray?, error: NSError?) -> Void in
                                if facilities != nil && facilities!.count > 0 {
                                    offlineModeManager.startUpload(facilities![0] as! Facility)
                                } else {
                                    self.setOn(false, animated: false)
                                }
                            })
                        } else {
                            self.setOn(false, animated: true)
                        }
                    })
                    }, cancelActionHandler: { (action: UIAlertAction!) -> Void in
                        self.setOn(false, animated: true)
                }
                )
            } else {
                Facility.list({(results: NSArray?, error) in
                    if let facilities = results {
                        Utils.showFacilityChooser(self, parent: self.parentViewController, title: "Switch to offline mode", message: "Please select a facility in order to proceed", facilities: facilities as! [Facility], selected: { (selectedFacility) -> Void in
                            offlineModeManager.startDownload(selectedFacility)
                            },canceled: { () -> Void in
                                self.setOn(true, animated: true)
                        })
                    } else {
                        self.setOn(true, animated: false)
                    }
                })
            }
            
        }
    }
    
}
