//
//  LinkPlotsViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/27/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import ArcGIS

class LinkPlotsViewController: UIViewController,  UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, SHDropDownDelegate, UICollectionViewDelegate, UICollectionViewDataSource, AGSMapViewTouchDelegate {
    
    
    /* UI Outlets */
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var dropDownFacilities: SHDropdown!
    @IBOutlet weak var dropDownSections: SHDropdown!
    @IBOutlet weak var textFieldPlotNumber: UITextField!
    @IBOutlet weak var textFieldRow: UITextField!
    @IBOutlet weak var textFieldFirstname: UITextField!
    @IBOutlet weak var textFieldLastname: UITextField!
    @IBOutlet weak var textFieldDateofdeath: UITextField!
    
    @IBOutlet weak var labelLinkPlotStarted: UILabel!
    
    @IBOutlet weak var pageNavigatorView: UIView!
    @IBOutlet weak var paginator: UICollectionView!
    @IBOutlet weak var viewMapArea: UIView!
    @IBOutlet weak var tableSearchResults: UITableView!
    @IBOutlet weak var searchResultsView: UIView!
    
    @IBOutlet weak var constriantSearchResultsTop: NSLayoutConstraint!
    @IBOutlet weak var constriantSearchResultsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var buttonExpandToolbar: UIButton!
    @IBOutlet weak var constriantSearchBarHeight: NSLayoutConstraint!
    
    /* Private members */
    private var mapManager: SearchMapManager?
    private var searchResults: NSArray = NSArray()
    private weak var facilities: NSArray!
    private var tableSearchResultsLaunched : Bool = false
    
    private var lastPlotNumber: String = ""
    private var lastRow: String = ""
    private var lastFacilityId: String = ""
    private var lastSectionName: String = ""
    private var lastForename: String = ""
    private var lastSurname: String = ""
    private var lastDateOfDeath: String = ""
    private var limit = 200
    private var pageCount = 0
    private var longestSearchResultTitle : NSString = ""
    
    /* Private methods */
    func startMovingPlot() {
        self.searchResultsView.hidden = true // Hide the search list
        
        self.labelLinkPlotStarted.alpha = 0.0
        self.labelLinkPlotStarted.hidden = false
        UIView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
            self.labelLinkPlotStarted.alpha = 0.85
            }, completion: nil)
        
        
        if let mapManager = self.mapManager {
            mapManager.movePinMode = true
            mapManager.showCallout = false
            mapManager.mapView?.showMagnifierOnTapAndHold = true
        }
    }
    
    func endMovingPlot() {
        self.searchResultsView.alpha = 0 // Hide the search list
        self.searchResultsView.hidden = false // Hide the search list
        
        UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseOut, animations: {
            self.searchResultsView.alpha = 0.9
            }, completion: nil)
        
        self.labelLinkPlotStarted.hidden = true
        if let mapManager = mapManager {
            mapManager.showCallout = false
            mapManager.movePinMode = false
            mapManager.mapView?.showMagnifierOnTapAndHold = false
        }
    }
    
    /* Overridden methods */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading...")
        self.dropDownFacilities.dropDownDelegate = self
        
        // Get a list of facilities and their sections
        Facility.list({(results: NSArray?, error) in
            hud.hide(true)
            
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            if let results = results {
                
                self.facilities = results
                
                for var index = 0; index < results.count; ++index  {
                    let facility: Facility = results[index] as! Facility
                    self.dropDownFacilities.addItem(SHDropdownItem(facility.name, tag: facility.facilityId))
                    if index == 0 {
                        
                        // Load the Map
                        self.mapManager = SearchMapManager(mapAreaView: self.viewMapArea, facility: facility, touchDelegate: self, calloutDelegate: nil, layerDelegate: nil)
                        
                        self.dropDownSections.addItem(SHDropdownItem("All", tag: ""))
                        for item in facility.sortedSections {
                            if let facilitySection = item as? FacilitySection {
                                self.dropDownSections.addItem(SHDropdownItem(facilitySection.name, tag: facilitySection.facilitySectionId))
                            }
                        }
                    }
                }
                
            }
            
        })
        
        paginator.delegate = self
        paginator.dataSource = self
        pageNavigatorView.layer.borderWidth = 1
        pageNavigatorView.layer.cornerRadius = 2
        pageNavigatorView.clipsToBounds = true
        pageNavigatorView.layer.borderColor = UIColor(red: 221,green: 221,blue: 221).CGColor
        
        // Set delegates
        textFieldRow.delegate = self
        textFieldPlotNumber.delegate = self
        textFieldFirstname.delegate = self
        textFieldLastname.delegate = self
        textFieldDateofdeath.delegate = self
        
        tableSearchResults.delegate = self
        tableSearchResults.dataSource = self
        
        // Lose textfield focus if tapped anywhere else
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "handleSingleTap:")
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /* Class methods */
    // Begin searching for plots
    private func search(plotNumber: String, row: String, facilityId: String, sectionName: String, forename: String, surname: String, dateOfDeath: String,offset: Int, limit: Int, newSearch: Bool) {
        
        if let mapManager = self.mapManager {
            mapManager.clearPins()
            mapManager.dismissCallout()
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Searching...")
        
        Plot.search(plotNumber, facilityId: facilityId, sectionName: sectionName, row: row, forename: forename, surname: surname, dateOfDeath: dateOfDeath, unlinked: true, offset: offset, limit: limit, completionHandler: { (totalRecords, results, error) -> Void in
            hud.hide(true)
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            if let results = results {
                if results.count > 0 {
                    self.searchResults = results
                    self.textFieldRow.toggleHighlightShadow(true)
                    self.textFieldPlotNumber.toggleHighlightShadow(true)
                    
                    if let mapManager = self.mapManager {
                        mapManager.pinPlots(results)
                    }
                    self.tableSearchResults.reloadData()
                    
                    if !self.tableSearchResultsLaunched {
                        self.searchResultsView.alpha = 0.0
                        self.searchResultsView.hidden = false
                        self.tableSearchResultsLaunched = true
                        
                        
                        
                        UIView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                            self.searchResultsView.alpha = 1
                            }, completion: { finished in
                                self.searchResultsView.alpha = 0.9
                        })
                        
                    }
                    
                    if newSearch {
                        self.pageCount = (totalRecords! + limit - 1) / limit
                        self.paginator.reloadData()
                        if totalRecords > 0 {
                            self.paginator.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                        }
                        self.constriantSearchResultsTop.constant = self.pageCount > 1 ? 50 : 0
                    }
                    
                    // Resize the search list's height based on the number of results
                    self.constriantSearchResultsHeight.constant = CGFloat(min(5, results.count)  * 60) +  self.constriantSearchResultsTop.constant
                    
                    return
                }
            }
            
            
            Utils.showAlert("No results", message: "The search didn't return any results.")
        })
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        let df = NSDateFormatter()
        df.dateStyle = NSDateFormatterStyle.LongStyle
        df.locale = NSLocale.currentLocale()
        
        if textField.isEqual(self.textFieldDateofdeath) {
            let vc = UIViewController()
            
            // Date picker
            let datePicker = UIDatePicker(frame: CGRectMake(0, 30, 300, 150))
            datePicker.datePickerMode = UIDatePickerMode.Date
            datePicker.hidden = false
            
            if let selectedDate = df.dateFromString(self.textFieldDateofdeath.text!) {
                datePicker.date = selectedDate
            } else {
                datePicker.date = NSDate()
            }
            
            datePicker.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents:UIControlEvents.ValueChanged)
            vc.view.addSubview(datePicker)
            
            // Clear button
            let button   = UIButton.buttonWithType(UIButtonType.System) as! UIButton
            button.frame = CGRectMake(200, 0, 100, 40)
            button.setTitle("Clear", forState: UIControlState.Normal)
            button.addTarget(self, action: "clearDatePicker:", forControlEvents: UIControlEvents.TouchUpInside)
            
            vc.view.addSubview(button)
            
            
            let popOverForDatePicker = UIPopoverController(contentViewController: vc)
            popOverForDatePicker.setPopoverContentSize(CGSizeMake(300, 200), animated: false)
            
            popOverForDatePicker.presentPopoverFromRect(textField.bounds, inView: textField, permittedArrowDirections: UIPopoverArrowDirection.Up, animated: true)
            
            return false
        }
        return true
    }
    
    
    /* Delegate messages */
    func handleDatePicker(sender: UIDatePicker) {
        let df = NSDateFormatter()
        df.dateStyle = NSDateFormatterStyle.LongStyle
        df.locale = NSLocale.currentLocale()
        
        self.textFieldDateofdeath.text = df.stringFromDate(sender.date)
    }
    
    func clearDatePicker(sender:UIButton!) {
        self.textFieldDateofdeath.text = ""
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if self.tableSearchResultsLaunched {
            self.searchResultsView.hidden = false
        }
    }
    
    // User hits enter on the search field
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.searchButtonTapped(textField)
        return false
    }
    
    func dropDown(dropDown: SHDropdown!, didChangeSelection newItem: SHDropdownItem!, newRow: Int, oldRow: Int) {
        if let facilities = self.facilities {
            if newRow < facilities.count {
                let facility: Facility = facilities[newRow] as! Facility
                
                // Change Map
                if let mapManager = self.mapManager {
                    mapManager.changeFacility(facility)
                }
                // Change available sections
                self.dropDownSections.removeAllItems()
                self.dropDownSections.addItem(SHDropdownItem("All", tag: ""))
                for item in facility.sortedSections {
                    if let facilitySection = item as? FacilitySection {
                        self.dropDownSections.addItem(SHDropdownItem(facilitySection.name, tag: facilitySection.facilitySectionId))
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        longestSearchResultTitle = ""
        for var index = 0; index < searchResults.count; ++index  {
            let plot : Plot = self.searchResults[index] as! Plot
            var label: String = plot.sectionName
            if !plot.row.isEmpty {
                label += "/" +  plot.row
            }
            label += "/" + plot.plotNumber
            if longestSearchResultTitle.length < count(label) {
                longestSearchResultTitle = label
            }
        }
        return self.searchResults.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let plot : Plot = self.searchResults[indexPath.row] as! Plot
        
        var cell: UITableViewCell
        if plot.deceasedRecords.count == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        } else {
            let deceasedRecord = plot.deceasedRecords.allObjects[0] as! DeceasedRecord
            cell = tableView.dequeueReusableCellWithIdentifier("cellWithDate", forIndexPath: indexPath) as! UITableViewCell
            
            var labelDeceasedFullname: UILabel = cell.viewWithTag(1) as! UILabel
            var labelDeceasedDate: UILabel = cell.viewWithTag(2) as! UILabel
            labelDeceasedFullname.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
            labelDeceasedDate.text = deceasedRecord.deceasedDate
        }
        
        var labelTitle: UILabel = cell.viewWithTag(3) as! UILabel
        var moveButton : UIButton = cell.viewWithTag(4) as! UIButton
        //var viewButton : UIButton = cell.viewWithTag(3) as UIButton
        
        
        var label: String = plot.sectionName
        if !plot.row.isEmpty {
            label += "/" +  plot.row
        }
        label += "/" + plot.plotNumber
        labelTitle.text = label
        
        let labelWidthConstraint = labelTitle.constraints()[1] as! NSLayoutConstraint // Width constriant
        // Autosize Section/Row/Plot
        let width = longestSearchResultTitle.sizeWithAttributes([NSFontAttributeName: labelTitle.font]).width + 15
        labelWidthConstraint.constant = min(320, width)
        
        moveButton.addTarget(self, action: "actionLinkPlot:", forControlEvents: UIControlEvents.TouchDown)
        //viewButton.addTarget(self, action: "actionEditPlot:", forControlEvents: UIControlEvents.TouchDown)
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(red: 240,green: 240,blue: 240)
        } else {
            cell.backgroundColor  = UIColor.whiteColor()
        }
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let plot : Plot = self.searchResults[indexPath.row] as! Plot
        if let mapManager = self.mapManager {
            if plot.longitude != 0 && plot.latitude != 0 {
                mapManager.zoomToPlot(plot)
            } else {
                mapManager.selectPlot(plot.plotId)
            }
        }
        
        // Colors
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            var moveButton : UIButton = cell.viewWithTag(4) as! UIButton
            moveButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            var moveButton : UIButton = cell.viewWithTag(4) as! UIButton
            moveButton.setTitleColor(UIColor(red: 48, green: 131, blue: 251), forState: UIControlState.Normal)
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 52, green: 163, blue: 215)
        bgColorView.layer.masksToBounds = true
        cell.selectedBackgroundView = bgColorView
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pageCount
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
        
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        label.text = String(indexPath.item + 1)
        label.layer.borderWidth = 1
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        
        // Default selection
        if cell.selected {
            label.textColor = UIColor.whiteColor()
            label.backgroundColor =  UIColor(red: 52,green: 163,blue: 215)
            label.layer.borderColor = UIColor(red: 52,green: 163,blue: 215).CGColor
        } else {
            label.textColor = UIColor.blackColor()
            label.backgroundColor =  UIColor.whiteColor()
            label.layer.borderColor = UIColor(red: 240,green: 240,blue: 240).CGColor
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            let label : UILabel = cell.viewWithTag(1) as! UILabel
            label.textColor = UIColor.whiteColor()
            label.backgroundColor =  UIColor(red: 52,green: 163,blue: 215)
            label.layer.borderColor = UIColor(red: 52,green: 163,blue: 215).CGColor
            self.search(self.lastPlotNumber, row: self.lastRow, facilityId: self.lastFacilityId, sectionName: self.lastSectionName, forename: self.lastForename, surname: self.lastSurname, dateOfDeath: self.lastDateOfDeath, offset: indexPath.item * self.limit, limit: self.limit, newSearch: false)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            let label : UILabel = cell.viewWithTag(1) as! UILabel
            label.textColor = UIColor.blackColor()
            label.backgroundColor =  UIColor.whiteColor()
            label.layer.borderColor = UIColor(red: 240,green: 240,blue: 240).CGColor
        }
    }
    
    /* UI Actions */
    
    @IBAction func searchButtonTapped(sender: AnyObject) {
        
        self.lastPlotNumber = self.textFieldPlotNumber.text
        self.lastRow = self.textFieldRow.text
        
        // Get selected facility and section id
        self.lastFacilityId = ""
        if let selectedFacility = dropDownFacilities.getSelectedItem() {
            self.lastFacilityId = selectedFacility.tag as! String
        }
        
        self.lastSectionName = ""
        if let selectedSection = dropDownSections.getSelectedItem() {
            if dropDownSections.selectedIndex > 0 {
                self.lastSectionName = selectedSection.title as String
            }
        }
        
        self.lastForename = self.textFieldFirstname.text
        self.lastSurname = self.textFieldLastname.text
        self.lastDateOfDeath = self.textFieldDateofdeath.text
        
        self.tableSearchResultsLaunched = false
        self.searchResultsView.hidden = true
        textFieldRow.toggleHighlightShadow(false)
        textFieldPlotNumber.toggleHighlightShadow(false)
        
        self.search(self.lastPlotNumber, row: self.lastRow, facilityId: self.lastFacilityId, sectionName: self.lastSectionName, forename: self.lastForename, surname: self.lastSurname, dateOfDeath: self.lastDateOfDeath, offset: 0, limit: self.limit, newSearch: true)
    }
    
    
    @IBAction func zoomInButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomIn(true)
        }
    }
    @IBAction func zoomOutButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomOut(true)
        }
    }
    
    @IBAction func prevButtonTapped(sender: AnyObject) {
        let selectedItems = paginator.indexPathsForSelectedItems()
        if selectedItems.count > 0 {
            let selectedIndexPath = (selectedItems[0] as! NSIndexPath)
            if selectedIndexPath.item > 0 {
                paginator.selectItemAtIndexPath(NSIndexPath(forItem: selectedIndexPath.item - 1, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                
                self.collectionView(paginator, didSelectItemAtIndexPath: NSIndexPath(forItem: selectedIndexPath.item - 1, inSection: 0))
                self.collectionView(paginator, didDeselectItemAtIndexPath: selectedIndexPath)
            }
        }
    }
    
    @IBAction func nextButtonTapped(sender: AnyObject) {
        let selectedItems = paginator.indexPathsForSelectedItems()
        if selectedItems.count > 0 {
            let selectedIndexPath = (selectedItems[0] as! NSIndexPath)
            if selectedIndexPath.item < self.pageCount - 1 {
                paginator.selectItemAtIndexPath(NSIndexPath(forItem: selectedIndexPath.item + 1, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)
                
                self.collectionView(paginator, didSelectItemAtIndexPath: NSIndexPath(forItem: selectedIndexPath.item + 1, inSection: 0))
                self.collectionView(paginator, didDeselectItemAtIndexPath: selectedIndexPath)
            }
        }
    }
    
    @IBAction func expandToolbarButtonTapped(sender: AnyObject) {
        
        if constriantSearchBarHeight.constant == 100 {
            constriantSearchBarHeight.constant = 50
            self.buttonExpandToolbar.setImage(UIImage(named:"icon_expand")!, forState: UIControlState.Normal)
        } else {
            constriantSearchBarHeight.constant  = 100
            self.buttonExpandToolbar.setImage(UIImage(named:"icon_collapse")!, forState: UIControlState.Normal)
        }
        
    }
    
    /* Selectors */
    func actionLinkPlot(sender: UIButton!) {
        let buttonPosition: CGPoint  = (sender as UIButton).convertPoint(CGPointZero, toView: self.tableSearchResults)
        let indexPath: NSIndexPath? = self.tableSearchResults.indexPathForRowAtPoint(buttonPosition)
        if let indexPath = indexPath
        {
            let cell = tableSearchResults.cellForRowAtIndexPath(indexPath)!
            tableSearchResults.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
            
            // Fire select event
            tableView(tableSearchResults, didSelectRowAtIndexPath: indexPath)
            
            self.startMovingPlot()
        }
    }
    
    func actionEditPlot(sender: UIButton!) {
        self.performSegueWithIdentifier("segueEditPlot", sender: sender)
    }
    
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        // Close keyboard
        self.view.endEditing(true)
        // Close Sidebar
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            revealViewController.setFrontViewPosition(FrontViewPosition.Left, animated: true)
        }
        // Close Result list if required
        let location : CGPoint = recognizer.locationInView(self.searchResultsView)
        if self.searchResultsView.hidden || !self.searchResultsView.pointInside(location, withEvent: nil) {
            self.searchResultsView.hidden = true
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let mapManager = self.mapManager {
            mapManager.dismissCallout()
        }
        if segue.identifier == "segueEditPlot" {
            let buttonPosition: CGPoint  = (sender as! UIButton).convertPoint(CGPointZero, toView: self.tableSearchResults)
            let indexPath: NSIndexPath? = self.tableSearchResults.indexPathForRowAtPoint(buttonPosition)
            if let indexPath = indexPath
            {
                if let facilities = self.facilities {
                    if  dropDownFacilities.selectedIndex > -1 {
                        let plot : Plot = self.searchResults[indexPath.row] as! Plot
                        let editPlotViewController: EditPlotViewController = segue.destinationViewController as! EditPlotViewController
                        
                        editPlotViewController.plot = plot
                        editPlotViewController.facility = facilities[dropDownFacilities.selectedIndex] as? Facility
                        return
                    }
                }
                Utils.showAlert("Select a facility", message: "Please select a facility to continue.")
            }
        } else if segue.identifier == "segueViewDeceasedRecord" {
            if let deceasedRecord = sender as? DeceasedRecord {
                let recordDetailsViewController: RecordDetailsViewController = segue.destinationViewController as! RecordDetailsViewController
                
                recordDetailsViewController.deceasedRecord = deceasedRecord
                
                // Assign facility for viewing this deceased record
                if dropDownFacilities.selectedIndex > -1 &&  dropDownFacilities.selectedIndex < facilities.count {
                    let facility: Facility = facilities[dropDownFacilities.selectedIndex] as! Facility
                    recordDetailsViewController.facility = facility
                }
            }
        }
    }
    
    func mapView(mapView: AGSMapView!, didEndTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        self.mapView(mapView, didClickAtPoint: screen, mapPoint: mappoint, features: features)
    }
    
    func mapView(mapView: AGSMapView!, didMoveTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        self.mapView(mapView, didTapAndHoldAtPoint: screen, mapPoint: mappoint, features: features)
    }
    
    func mapView(mapView: AGSMapView!, didTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        if self.mapManager!.movePinMode {
            if let selectedPin = self.mapManager!.selectedPin {
                selectedPin.geometry = mappoint
            }
        }
    }
    
    func mapView(mapView: AGSMapView!, didClickAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        if self.mapManager!.movePinMode {
            if let selectedPin = self.mapManager!.selectedPin {
                selectedPin.geometry = mappoint
                self.showLinkPlotPopup(selectedPin, mapPoint: mappoint)
            }
        }
    }
    
    func showLinkPlotPopup(pin: AGSGraphic!, mapPoint: AGSPoint!) {
        self.labelLinkPlotStarted.hidden = true
        if pin.hasAttributeForKey("plot_id") {
            let storyboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let linkPlotPopup = storyboard.instantiateViewControllerWithIdentifier("linkPlotPopup") as! LinkPlotPopupViewController
            //linkPlotPopup.searchViewController = self
            linkPlotPopup.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            linkPlotPopup.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
            linkPlotPopup.preferredContentSize = CGSizeMake(350, 350)
            linkPlotPopup.loadDataIntoView(pin.attributeForKey("plot_id") as! String, newLocation: mapManager!.getGPSCoordinates(mapPoint), completionHandler: { (result) -> Void in
                
                if result == LinkPlotDialogResult.Save {
                    self.endMovingPlot()
                } else if result == LinkPlotDialogResult.Cancel {
                    pin.geometry = self.mapManager!.getAGSPoint(0, longitude: 0)
                    self.endMovingPlot()
                } else if result == LinkPlotDialogResult.Move {
                    self.labelLinkPlotStarted.hidden = false
                }
                
            })
            self.presentViewController(linkPlotPopup, animated: true, completion: nil)
        }
    }
    
}
