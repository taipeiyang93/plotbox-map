//
//  DeedOwner.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/14/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class DeedOwner {
    internal var title: String = ""
    internal var forename: String = ""
    internal var surname: String = ""
    internal var address1: String = ""
    internal var address2: String = ""
    internal var country: String = ""
    internal var postalCode: String = ""
    internal var activeOwner: String = ""
}
