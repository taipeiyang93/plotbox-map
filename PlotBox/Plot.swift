//
//  Plot.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/3/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData

@objc(Plot)
class Plot: NSManagedObject {
    
    @NSManaged var depthRemainingFeet: String
    @NSManaged var depthRemainingInches: String
    @NSManaged var depthRemainingString: String
    @NSManaged var modified: NSNumber
    @NSManaged var plotId: String
    @NSManaged var plotNumber: String
    @NSManaged var purchased: String
    @NSManaged var remainingCapacity: NSNumber
    @NSManaged var row: String
    @NSManaged var sectionName: String
    @NSManaged var totalCapacity: NSNumber
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged var notes: String
    @NSManaged var deceasedRecords: NSSet
    @NSManaged var images: NSSet
    
    internal var facilityName: String = ""
    
    // Deed owner details
    // internal var deedOwnertitle: String = ""
    @NSManaged var deedOwnerForename: String
    @NSManaged var deedOwnerSurname: String
    // internal var deedOwnerAddress1: String = ""
    // internal var deedOwnerAddress2: String = ""
    // internal var deedOwnerCountry: String = ""
    // internal var deedOwnerPostalCode: String = ""
    // internal var deedOwnerActiveOwner: String = ""
    
    // Images
    internal var imageBaseUrl: String = ""
    internal var primaryImage: String = ""
    //internal var images: [String] = [String]()
    
    var primaryImageUrl: NSURL   {
        get {
            if isOfflineMode {
                return NSBundle.mainBundle().URLForResource("offline-mode", withExtension: "jpg")!
            } else {
                return NSURL(string: self.imageBaseUrl + "/" + self.plotId + "/" + self.primaryImage + ".jpg")!
            }
        }
    }
    
    class func entityName() -> NSString {
        return "Plot"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> Plot {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(Plot.entityName() as String, inManagedObjectContext: context!)!
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! Plot
    }
    
    
    class func plotExists(plotNumber: String, sectionName: String, row: String) -> Bool {
        
        if isOfflineMode {
            let request: NSFetchRequest = NSFetchRequest(entityName: Plot.entityName() as String)
            
            var predicates: Array<NSPredicate> = []
            
            if sectionName != "" {
                predicates.append(NSPredicate(format: "sectionName contains[c] %@", sectionName))
            }
            
            if !row.isEmpty {
                predicates.append(NSPredicate(format: "row == %@", row))
            }
            
            if !plotNumber.isEmpty {
                predicates.append(NSPredicate(format: "plotNumber == %@", plotNumber))
            }
            
            var compound = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
            request.predicate = compound
            
            var error: NSError? = nil
            let totalRecords: Int = CoreData.getManagedObjectContext()!.countForFetchRequest(request, error: &error)
            
            return totalRecords > 0
        }
        return false
    }
    
    
    class func search(plotNumber: String, facilityId: String, sectionName: String, row: String, forename: String, surname: String, dateOfDeath: String, unlinked: Bool, offset: Int, limit: Int, completionHandler: (Int?, NSArray?, NSError?) -> Void) {
        
        if let session = currentLoginSession {
            if isOfflineMode {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let request: NSFetchRequest = NSFetchRequest(entityName: Plot.entityName() as String)
                    
                    var predicates: Array<NSPredicate> = []
                    
                    if sectionName != "" {
                        predicates.append(NSPredicate(format: "sectionName contains[c] %@", sectionName))
                    }
                    
                    if !row.isEmpty {
                        predicates.append(NSPredicate(format: "row == %@", row))
                    }
                    
                    if !plotNumber.isEmpty {
                        predicates.append(NSPredicate(format: "plotNumber == %@", plotNumber))
                    }
                    
                    if unlinked {
                        predicates.append(NSPredicate(format: "latitude == 0 AND longitude == 0"))
                    }
                    
                    if !forename.isEmpty {
                        predicates.append(NSPredicate(format: "ANY deceasedRecords.deceasedForename contains[c] %@", forename))
                    }
                    
                    if !surname.isEmpty {
                        predicates.append(NSPredicate(format: "ANY deceasedRecords.deceasedSurname contains[c] %@", surname))
                    }
                    
                    if !dateOfDeath.isEmpty {
                        let df = NSDateFormatter()
                        df.dateStyle = NSDateFormatterStyle.LongStyle
                        df.locale = NSLocale.currentLocale()
                        if let nsDateOfDeath = df.dateFromString(dateOfDeath) {
                            let dfCoreData = NSDateFormatter()
                            dfCoreData.dateFormat = "dd MMM yyyy"
                            predicates.append(NSPredicate(format: "ANY deceasedRecords.deceasedDate contains[c] %@", dfCoreData.stringFromDate(nsDateOfDeath)))
                        }
                    }
                    
                    var compound = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
                    request.predicate = compound
                    
                    
                    let sortSection: NSSortDescriptor = NSSortDescriptor(key: "sectionName", ascending: true)
                    let sortRow: NSSortDescriptor = NSSortDescriptor(key: "row", ascending: true)
                    let sortPlotNumber: NSSortDescriptor = NSSortDescriptor(key: "plotNumber", ascending: true)
                    
                    request.sortDescriptors = [sortSection, sortRow, sortPlotNumber]
                    
                    
                    var error: NSError? = nil
                    let totalRecords: Int = CoreData.getManagedObjectContext()!.countForFetchRequest(request, error: &error)
                    
                    request.fetchOffset = offset;
                    request.fetchLimit = limit;
                    
                    let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if error == nil {
                            completionHandler(totalRecords, results, nil)
                        } else {
                            completionHandler(nil, nil, error)
                        }
                    }
                }
                
            } else {
                PlotOnline.search(session, plotNumber: plotNumber, facilityId: facilityId, sectionName: sectionName, row: row, forename: forename, surname: surname, dateOfDeath: dateOfDeath, unlinked: unlinked, offset: offset, limit: limit, completionHandler: completionHandler)
            }
        }
    }
    
    class func getUnusedPlots(facilityId: String, facilitySectionId: String, completionHandler: (NSArray?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                let request: NSFetchRequest = NSFetchRequest(entityName: Plot.entityName() as String)
                let predicate = NSPredicate(format: "purchased != %@", "1")
                request.predicate = predicate
                
                var error: NSError? = nil
                let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
                
                completionHandler(results, nil)
            } else {
                PlotOnline.getUnusedPlots(session, facilityId: facilityId, facilitySectionId: facilitySectionId, completionHandler: completionHandler)
            }
        }
    }
    
    class func getById(plotId: String, completionHandler: (Plot?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                let request: NSFetchRequest = NSFetchRequest(entityName: Plot.entityName() as String)
                
                let predicate = NSPredicate(format: "plotId == %@", plotId)
                request.predicate = predicate
                
                var error: NSError? = nil
                let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
                let plot: Plot! = results[0] as! Plot
                completionHandler(plot, nil)
            } else {
                PlotOnline.getById(session, plotId: plotId, completionHandler: completionHandler)
            }
        }
    }
    
    
    // POST
    class func save(plot: Plot, completionHandler: (Bool?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                plot.modified = true
                CoreData.saveContext(completionHandler)
            } else {
                PlotOnline.save(session, plot: plot, completionHandler: completionHandler)
            }
        }
    }
}
