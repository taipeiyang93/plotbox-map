//
//  NewPlotImageUploaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class NewPlotImageUploaderTask: AsyncTask {
    var session: LoginSession!
    var images: [(filename: String, plotId: String)]!
    
    init(session: LoginSession!, images: [(filename: String, plotId: String)], progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
        self.images = images
    }
    
    override func start()
    {
        self.notifyProgress("Uploading new plot images...", progressCurrent: 0, progressTotal: 0)
        
        dispatch_async(dispatch_get_main_queue()) {
            let count = self.images.count
            
            if count > 0 {
                self.upload(0, count: count)
            } else {
                self.notifyCompleted()
            }
        }
    }
    
    func upload(index: Int, count: Int) {
        
        let image = self.images[index]
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let imageFilePath = documentsPath.stringByAppendingPathComponent(image.filename)
        self.notifyProgress("Uploading images... \(index + 1) of \(count)", progressCurrent: 0, progressTotal: 0)
        
        if let imageData =  NSData(contentsOfFile: imageFilePath) {
            //let plotId =
            PlotImageOnline.uploadImage(self.session, plotId: image.plotId, imageData: imageData, progress: { (progressCurrent) -> Void in
                self.notifyProgress(String(format: "Uploading images... %d of %d : %.0f%%", index + 1, count, progressCurrent * 100), progressCurrent: Double(progressCurrent), progressTotal: 1)
                }, completed: { (imageId, error) -> Void in
                    
                    // Delete the temporary image file
                    var error: NSError?
                    NSFileManager.defaultManager().removeItemAtPath(imageFilePath, error: &error)
                    
                    if index + 1 < count {
                        self.upload(index + 1, count: count)
                    } else {
                        self.notifyCompleted()
                    }
            })
        } else {
            if index + 1 < count {
                self.upload(index + 1, count: count)
            } else {
                self.notifyCompleted()
            }
        }
    }
}
