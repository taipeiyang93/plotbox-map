//
//  CreatePlotsViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/14/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import ArcGIS
import CoreData


class CreatePlotsViewController: UIViewController, AGSMapViewTouchDelegate, AGSCalloutDelegate, AGSMapViewLayerDelegate {
    
    /*UI Outlets*/
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var dropDownFacilities: SHDropdown!
    @IBOutlet weak var viewMapArea: UIView!
    @IBOutlet weak var buttonMovePlots: UIButton!
    @IBOutlet weak var buttonCancelMovePlots: UIButton!
    
    /* Private members */
    private var mapManager: MapManager?
    private weak var facilities: NSArray!
    private var incrementNextPlotNumber: Bool = true
    
    private var lastPlotNumber: Int?
    private var lastRow: String?
    private var lastSectionName: String?
    private var plots: Dictionary<String, NewPlot> = Dictionary()
    
    private var isMovingPlots: Bool = false
    private var selectedPin: AGSGraphic?
    private var changedPins: Dictionary<String, AGSGraphic>!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //OfflineModeManager.deleteAllEntities(NewPlot.entityName())
        //OfflineModeManager.deleteAllEntities(NewPlotImage.entityName())
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading...")
        
        
        // Get a list of facilities and their sections
        Facility.list({(results: NSArray?, error) in
            hud.hide(true)
            
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            if let results = results {
                
                self.facilities = results
                
                for var index = 0; index < results.count; ++index  {
                    let facility: Facility = results[index] as! Facility
                    self.dropDownFacilities.addItem(SHDropdownItem(facility.name, tag: facility.facilityId))
                    if index == 0 {
                        // Load the Map
                        self.mapManager = MapManager(mapAreaView: self.viewMapArea, facility: facility, touchDelegate: self, calloutDelegate: self, layerDelegate: self)
                        self.mapManager!.mapView!.showMagnifierOnTapAndHold = true
                        self.mapManager!.mapView!.locationDisplay.startDataSource()
                        self.mapManager!.mapView!.locationDisplay.autoPanMode  = AGSLocationDisplayAutoPanMode.Navigation
                    }
                }
                
            }
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func zoomInButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomIn(true)
        }
    }
    
    @IBAction func zoomOutButtonTapped(sender: AnyObject) {
        if let mapManager = self.mapManager {
            mapManager.zoomOut(true)
        }
    }
    
    @IBAction func movePlotsTapped(sender: AnyObject) {
        if self.isMovingPlots {
            self.stopMovingPlots(true)
            // Save changes
        } else {
            // Start moving plots
            self.startMovingPlots()
        }
    }
    
    @IBAction func cancelMovePlotsTapped(sender: AnyObject) {
        // Cancel changes
        self.stopMovingPlots(false)
    }
    
    
    func loadPopupDialog() -> CreatePlotPopupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let createPlotPopup = storyboard.instantiateViewControllerWithIdentifier("createPlotPopup") as! CreatePlotPopupViewController
        
        createPlotPopup.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        createPlotPopup.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        createPlotPopup.preferredContentSize = CGSizeMake(500, 560)
        self.presentViewController(createPlotPopup, animated: true, completion: nil)
        return createPlotPopup
    }
    
    
    
    func showUpdatePopup(plot: NewPlot, pin: AGSGraphic) {
        let createPlotPopup: CreatePlotPopupViewController  = loadPopupDialog()
        createPlotPopup.updatePlotScreen(plot, fnCompletionHandler: { (result) -> Void in
                if result == NewPlotDialogResult.Save {
                    CoreData.saveContext(nil)
                } else if result == NewPlotDialogResult.Delete {
                    self.plots.removeValueForKey(plot.plotId)
                    
                    // Delete plot images
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
                    for image in plot.images.allObjects as! [NewPlotImage] {
                        let imageFilePath = documentsPath.stringByAppendingPathComponent(image.filename)
                        var error: NSError?
                        NSFileManager.defaultManager().removeItemAtPath(imageFilePath, error: &error)
                        CoreData.getManagedObjectContext()!.deleteObject(image)
                    }
                    
                    // Delete plot
                    CoreData.getManagedObjectContext()!.deleteObject(plot)
                    self.mapManager!.pinsLayer.removeGraphic(pin)
                    self.selectedPin = nil
                    CoreData.saveContext(nil)
                }
            
        })
    }
    
    func showCreatePopup(plot: NewPlot, pin: AGSGraphic) {
        let createPlotPopup: CreatePlotPopupViewController  = loadPopupDialog()
        
        var defaultPlotNumber: String?
        if let lastPlotNumber = self.lastPlotNumber {
            defaultPlotNumber = String(lastPlotNumber + (self.incrementNextPlotNumber ? 1 : -1))
        }
        
        createPlotPopup.createPlotScreen(plot, defaultPlotNumber: defaultPlotNumber, defaultRow: self.lastRow, defaultSectionName: self.lastSectionName, incrementNextPlotNumber: self.incrementNextPlotNumber) { (result, incrementNextPlotNumber) -> Void in
            if result == NewPlotDialogResult.Save {
                CoreData.saveContext(nil)
                if let plotNumber = plot.plotNumber.toInt() {
                    self.incrementNextPlotNumber = incrementNextPlotNumber
                    self.lastPlotNumber = plotNumber
                }
                self.lastRow = plot.row
                self.lastSectionName = plot.sectionName
                self.plots[plot.plotId] = plot
                
            } else if result == NewPlotDialogResult.Cancel {
                CoreData.getManagedObjectContext()!.deleteObject(plot)
                self.mapManager!.pinsLayer.removeGraphic(pin)
                self.selectedPin = nil
            }
        }
    }
    
    
    func mapViewDidLoad(mapView: AGSMapView!) {
        let request: NSFetchRequest = NSFetchRequest(entityName: NewPlot.entityName() as String)
        request.returnsObjectsAsFaults = false
        
        var error: NSError? = nil
        let plots: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
        
        for plot in plots as! [NewPlot] {
            self.plots[plot.plotId] = plot
        }
        
        self.pinPlots(self.plots)
    }
    
    func callout(callout: AGSCallout!, willShowForFeature feature: AGSFeature!, layer: AGSLayer!, mapPoint: AGSPoint!) -> Bool {
        
        return false
    }
    
    func mapView(mapView: AGSMapView!, didTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        if features.isEmpty {
            let pin = mapManager!.addPinToPoint(mappoint, attributes: ["new": "1"])
            self.selectNewPin(pin, changeSymbol: false)
        } else {
            if features["markers"]?.count > 0 {
                self.selectNewPin(features["markers"]![0] as! AGSGraphic, changeSymbol: false)
            }
        }
    }
    
    func mapView(mapView: AGSMapView!, didMoveTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!)
    {
        if let selectedPin = self.selectedPin {
            selectedPin.geometry = mappoint
        }
    }
    
    func mapView(mapView: AGSMapView!, didEndTapAndHoldAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        if let selectedPin = self.selectedPin {
            // Is existing pin?
            if selectedPin.hasAttributeForKey("id") {
                self.selectNewPin(selectedPin, changeSymbol: true)
                self.savePlotPosition(selectedPin.attributeAsStringForKey("id"), position: mappoint)
            } else {
                let plot: NewPlot = NewPlot.managedInstance(CoreData.getManagedObjectContext())
                let gps = mapManager!.getGPSCoordinates(mappoint)
                plot.latitude = gps.latitude
                plot.longitude = gps.longitude
                
                selectedPin.setAttribute(plot.plotId, forKey: "id")
                self.selectNewPin(selectedPin, changeSymbol: true)
                self.showCreatePopup(plot, pin: selectedPin)
            }
        }
    }
    
    func mapView(mapView: AGSMapView!, didClickAtPoint screen: CGPoint, mapPoint mappoint: AGSPoint!, features: [NSObject : AnyObject]!) {
        
        if features.isEmpty {
            if self.isMovingPlots {
                if let selectedPin = self.selectedPin {
                    self.movePin(selectedPin, mapPoint: mappoint)
                } else {
                    Utils.showTooltip("Select a pin to move first", message: "Please click on a pin to select it then click on any empty space on the map to move the pin to that location.", targetView: buttonMovePlots, inView: self.view)
                }
            } else {
                let plot: NewPlot = NewPlot.managedInstance(CoreData.getManagedObjectContext())
                let gps = mapManager!.getGPSCoordinates(mappoint)
                plot.latitude = gps.latitude
                plot.longitude = gps.longitude
                
                
                let pin = mapManager!.addPinToPoint(mappoint, attributes: ["id": plot.plotId])
                self.selectNewPin(pin)
                self.showCreatePopup(plot, pin: pin)
            }
        } else {
            if features["markers"]?.count > 0 {
                let pin = features["markers"]![0] as! AGSGraphic
                if pin.hasAttributeForKey("id") {
                    self.selectNewPin(pin)
                    if !self.isMovingPlots {
                        let plotId = pin.attributeAsStringForKey("id")
                        if let plot = self.getPlotById(plotId) {
                            self.showUpdatePopup(plot, pin: pin)
                        }
                    }
                }
            }
            
        }
    }
    
    func getPlotById(plotId: String) -> NewPlot? {
        return self.plots[plotId]
    }
    
    func selectNewPin(newPin: AGSGraphic!, changeSymbol: Bool = true) {
        if let selectedPin = self.selectedPin {
            self.mapManager!.changePinSymbol(selectedPin, iconName: "icon_pin")
        }
        if changeSymbol {
            self.mapManager!.changePinSymbol(newPin, iconName: "icon_pin_green")
        }
        self.selectedPin = newPin
    }
    
    func movePin(pin: AGSGraphic!, mapPoint: AGSPoint!) {
        pin.geometry = mapPoint
        // Add the pin to changed list
        if pin.hasAttributeForKey("id") {
            self.changedPins.updateValue(pin, forKey: pin.attributeAsStringForKey("id"))
        }
    }
    
    func startMovingPlots() {
        self.isMovingPlots = true
        self.buttonMovePlots.setTitle("Save", forState: UIControlState.Normal)
        self.buttonCancelMovePlots.hidden = false
        self.changedPins = Dictionary()
    }
    
    func stopMovingPlots(save: Bool) {
        self.isMovingPlots = false
        self.buttonMovePlots.setTitle("Move Plots", forState: UIControlState.Normal)
        self.buttonCancelMovePlots.hidden = true
        
        if save {
            if !self.changedPins.isEmpty {
                for (plotId, pin)  in self.changedPins {
                    self.savePlotPosition(plotId, position: pin.geometry as! AGSPoint)
                }
            }
        } else {
            self.pinPlots(self.plots)
        }
        
        self.changedPins = nil
    }
    
    func pinPlots(plots: Dictionary<String, NewPlot>) {
        if let mapManager = self.mapManager {
            mapManager.clearPins()
            for (plotId, plot) in plots {
                let agsPoint = mapManager.getAGSPoint(Double(plot.latitude), longitude: Double(plot.longitude))
                mapManager.addPinToPoint(agsPoint, attributes: ["id": plotId])
            }
        }
    }
    
    func savePlotPosition(plotId: String, position: AGSPoint) {
        if let plot = self.getPlotById(plotId) {
            let gps = mapManager!.getGPSCoordinates(position)
            plot.latitude = gps.latitude
            plot.longitude = gps.longitude
        }
    }
    
    
}
