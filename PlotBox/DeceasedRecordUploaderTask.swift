//
//  DeceasedRecordUploaderTask.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/4/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class DeceasedRecordUploaderTask: AsyncTask {
    var session: LoginSession!
    
    init(session: LoginSession!, facilityId: String!, progress: ((message: String, progressCurrent: Double, progressTotal: Double) -> Void)?, completed: (() -> Void)?,  failed: ((message: String) -> Void)?) {
        super.init(progress: progress, completed: completed, failed: failed)
        self.session = session
    }
    
    override func start()
    {
        dispatch_async(dispatch_get_main_queue()) {
            let request: NSFetchRequest = NSFetchRequest(entityName: DeceasedRecord.entityName() as String)
            
            //let predicate = NSPredicate(format: "deceasedSurname contains[c] %@", text)
            let predicate = NSPredicate(format: "modified == 1")
            request.predicate = predicate
            
            let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "deceasedRecordId", ascending: true)
            request.sortDescriptors = [sortDescriptor]
            
            var error: NSError? = nil
            let deceasedRecords: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
            
            
            dispatch_async(dispatch_get_main_queue()) {
                if deceasedRecords.count > 0 {
                    self.upload(0, deceasedRecords: deceasedRecords)
                } else {
                    self.notifyCompleted()
                }
            }
        }
    }
    
    func upload(index: Int, deceasedRecords: NSArray) {
        let deceasedRecord: DeceasedRecord = deceasedRecords[index] as! DeceasedRecord
        DeceasedRecordOnline.save(session, deceasedRecord: deceasedRecord) { (result: Bool?, error: NSError?) -> Void in
            let count = deceasedRecords.count
            if index + 1 < count {
                self.notifyProgress(String(format: "Uploading deceased records... \(index + 1) of \(count)", index, count), progressCurrent: Double(index + 1), progressTotal: Double(count))
                self.upload(index + 1, deceasedRecords: deceasedRecords)
                deceasedRecord.modified = false // Deceased records has been uploaded so modified set to false
            } else {
                self.notifyCompleted()
            }
        }
    }
    
    override func notifyCompleted() {
        CoreData.saveContext(nil) // Save modified flags for uploaded deceased records
        super.notifyCompleted()
    }
}

