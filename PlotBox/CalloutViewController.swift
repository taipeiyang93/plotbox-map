//
//  CalloutViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/18/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import WebImage

class CalloutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var labelPlotCapacity: UILabel!
    
    @IBOutlet weak var labelDeedHolder: UILabel!
    @IBOutlet weak var tableBurials: UITableView!
    @IBOutlet weak var labelPlotRemainingCapacity: UILabel!
    @IBOutlet weak var labelSectionAndPlotNumber: UILabel!
    @IBOutlet weak var imagePlotHeadstone: UIImageView!
    
    @IBOutlet weak var textViewNotes: UITextView!
    @IBOutlet weak var viewWrapper: UIView!
    
    
    //var burialResults : BurialResults?
    private var plot: Plot?
    internal weak var searchViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableBurials.dataSource = self
        self.tableBurials.delegate = self
        self.tableBurials.tableFooterView = UIView()
        // create tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapGesture:")
        
        // add it to the image view;
        self.imagePlotHeadstone.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        self.imagePlotHeadstone.userInteractionEnabled = true
        self.imagePlotHeadstone.backgroundColor = UIColor.blackColor()
        self.textViewNotes.text = ""
        
        self.textViewNotes.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).CGColor
        self.textViewNotes.layer.borderWidth = 1.0
        self.textViewNotes.layer.cornerRadius = 5
        
    }
    
    func tapGesture(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if let imageView = gesture.view as? UIImageView {  // if you subclass UIImageView, then change "UIImageView" to your subclass
            if imageView.image != nil {
                
                let imageInfo: JTSImageInfo = JTSImageInfo()
                imageInfo.image = imageView.image
                imageInfo.referenceRect = imageView.frame
                imageInfo.referenceView = imageView.superview
                
                
                let imageViewer: JTSImageViewController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle:JTSImageViewControllerBackgroundOptions.Blurred)
                
                imageViewer.showFromViewController(self.searchViewController, transition: JTSImageViewControllerTransition._FromOriginalPosition)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDataIntoView(plotId : String) {
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Loading data...")
        var frame: CGRect = self.view.frame
        frame.size.height = 280
        frame.size.width = 250
        self.view.frame = frame
        self.viewWrapper.hidden = true
        
        Plot.getById(plotId, completionHandler: { (plot, error) in
            if let error = error {
                if error.domain == "unauthorized" {
                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                        
                    }
                }
                return
            }
            
            self.loadDataIntoView(plot!)
            hud.hide(true)
            self.viewWrapper.hidden = false
        })
    }
    
    private func reloadPreviewImage() {
        let hud = MBProgressHUD.showHUDAddedTo(self.imagePlotHeadstone, animated: true, labelText: nil)
        self.imagePlotHeadstone.sd_setImageWithURL(self.plot!.primaryImageUrl, completed: { (image: UIImage!, error: NSError!, type: SDImageCacheType, url: NSURL!) -> Void in
            hud.hide(true)
        })
    }
    
    private func loadDataIntoView(plot: Plot) {
        self.plot = plot
        self.tableBurials.reloadData()
        
        self.labelSectionAndPlotNumber.text = plot.sectionName + "/" + plot.plotNumber
        
        self.labelPlotCapacity.text = "CAP " + String(Int(plot.totalCapacity))
        self.labelPlotRemainingCapacity.text = "REM " + String(Int(plot.remainingCapacity))
        self.labelDeedHolder.text = plot.deedOwnerForename + " " + plot.deedOwnerSurname
        self.textViewNotes.text = plot.notes
        
        if plot.deceasedRecords.count > 0 {
            var frame: CGRect = self.view.frame
            frame.size.height = 400
            self.view.frame = frame
        }
        
        self.imagePlotHeadstone.image = nil
        self.reloadPreviewImage()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let plot = self.plot {
            return plot.deceasedRecords.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BurialCell", forIndexPath: indexPath)
            as! UITableViewCell
        if let plot = self.plot {
            let deceasedRecord: DeceasedRecord = plot.deceasedRecords.allObjects[indexPath.row] as! DeceasedRecord
            cell.textLabel?.text = deceasedRecord.deceasedForename + " " + deceasedRecord.deceasedSurname
            cell.detailTextLabel?.text = deceasedRecord.deceasedDate
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let plot = self.plot {
            if let searchViewController = self.searchViewController {
                if (searchViewController.isKindOfClass(SearchPlotsViewController) || searchViewController.isKindOfClass(SearchDeceasedRecordsViewController)) {
                    let deceasedRecord: DeceasedRecord = plot.deceasedRecords.allObjects[indexPath.row] as! DeceasedRecord
                    searchViewController.performSegueWithIdentifier("segueViewDeceasedRecord", sender: deceasedRecord)
                }
            }
        }
    }
    
    @IBAction func saveNotesButtonTapped(sender: AnyObject) {
        if let plot = self.plot {
            plot.notes = self.textViewNotes.text
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Saving...")
            Plot.save(plot, completionHandler: { (result: Bool?, error: NSError?) -> Void in
                hud.hide(true)
            })
        }
    }
    
    @IBAction func addImageButtonTapped(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            dispatch_async(dispatch_get_main_queue()) {
                
                var image = UIImagePickerController()
                image.modalPresentationStyle = UIModalPresentationStyle.FullScreen
                image.delegate = self
                image.sourceType = UIImagePickerControllerSourceType.Camera
                image.allowsEditing = true
                self.searchViewController!.presentViewController(image, animated: true, completion: nil)
            }
        } else {
            Utils.showAlert("Unsupported", message: "The attempted action is unsupported on this device.")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.searchViewController!.dismissViewControllerAnimated(true, completion: nil)
        
        if let plot = self.plot {
            let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(self.searchViewController!.view, animated: true, labelText: "Uploading...", mode: MBProgressHUDModeAnnularDeterminate)
            
            // image data
            let imageData = UIImageJPEGRepresentation(image, 1)
            PlotImage.uploadImage(plot, imageData: imageData, progress: { (progressCurrent) -> Void in
                hud.progress = progressCurrent
                }, completed: { (error: NSError?) -> Void in
                    hud.hide(true)
                    if error == nil {
                        self.imagePlotHeadstone.image = image
                        Utils.showAlert("Success", message: "Image uploaded successfully!")
                    } else {
                        Utils.showAlert("Error", message: error!.localizedDescription)
                    }
            })
        }
    }
}
