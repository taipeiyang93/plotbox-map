//
//  DeceasedRecordForm.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit


class DeceasedRecordForm: NSObject, FXForm {
    var title: String
    var forename: String
    var surname: String
    var religion: String
    var occupation: String
    var gender: String
    var maritalStatus: String
    var funeralType: String
    var burialType: String
    var notes: String
    var isProblematic: Bool
    
    // Age at death
    var ageYears: NSNumber?
    var ageMonths: String
    var ageWeeks: String
    var ageDays: String
    
    // Last residence
    var placeOfDeath: String
    var addressLine1: String
    var addressLine2: String
    var town: String
    var country: String
    var postcode: String
    
    
    
    // Dates
    var dateOfDeath: NSDate?
    var dateOfBurial: NSDate?

    
    weak var deceasedRecord: DeceasedRecord?
    
    func fields() -> [AnyObject]! {
        
        return [
            // Personal Details
            [FXFormFieldKey: "title", FXFormFieldHeader: "Personal Details"],
            [FXFormFieldKey: "forename",
                "textField.autocapitalizationType": UITextAutocapitalizationType.Words.rawValue],
            
            [FXFormFieldKey: "surname",
                "textField.autocapitalizationType": UITextAutocapitalizationType.Words.rawValue],
            "religion",
            "occupation",
            [FXFormFieldKey: "gender", FXFormFieldOptions: ["Male", "Female", "Others"], FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            
            [FXFormFieldKey: "maritalStatus", FXFormFieldOptions: ["Single", "Married", "Separated", "Divorced", "Widowed", "N/A"], FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            
            // Age at death
            [FXFormFieldKey: "ageYears", FXFormFieldTitle: "Years", FXFormFieldType: FXFormFieldTypeInteger, FXFormFieldHeader: "Age at death"],
            [FXFormFieldKey: "ageMonths", FXFormFieldTitle: "Months", FXFormFieldOptions: self.stringArrayOfNumbers(0, end: 11), FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "ageWeeks", FXFormFieldTitle: "Weeks", FXFormFieldOptions: self.stringArrayOfNumbers(0, end: 51), FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "ageDays", FXFormFieldTitle: "Days", FXFormFieldOptions:
                self.stringArrayOfNumbers(0, end: 30),
                FXFormFieldType: FXFormFieldTypeOption, FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "dateOfDeath", FXFormFieldHeader: ""],
            [FXFormFieldKey: "placeOfDeath"],
            
            [FXFormFieldKey: "addressLine1", FXFormFieldHeader: "Last Residence"],
            "addressLine2",
            "town",
            "country",
            "postcode",
            
            // Funerial Details
            [FXFormFieldKey: "funeralType", FXFormFieldTitle: "Funeral Type", FXFormFieldHeader: "Funeral Details", FXFormFieldOptions: ["Burial", "Cremation"], FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "burialType", FXFormFieldTitle: "Burial Type", FXFormFieldOptions: ["Adult", "Child", "Cremated remains", "Retained organs"], FXFormFieldType: FXFormFieldTypeOption,
                FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "dateOfBurial", FXFormFieldTitle: "Date of Burial"],
            
            // Notes
            [FXFormFieldKey: "isProblematic", FXFormFieldTitle: "Is Problematic", FXFormFieldHeader: "Notes" , FXFormFieldType: FXFormFieldTypeBoolean],
            [FXFormFieldKey: "notes", FXFormFieldTitle: "", FXFormFieldType: FXFormFieldTypeLongText]
            
        ]
    }
    
    override init() {
        // Initialize to empty
        self.title = ""
        
        self.forename = ""
        self.surname = ""
        self.religion = ""
        self.occupation = ""
        self.gender = ""
        self.maritalStatus = ""
        self.funeralType = ""
        self.burialType = ""
        self.notes = ""
        self.isProblematic = false
        
        
        // Age at death
        self.ageYears = 0
        self.ageMonths = ""
        self.ageWeeks = ""
        self.ageDays = ""
        
        // Last residence
        self.placeOfDeath = ""
        self.addressLine1 = ""
        self.addressLine2 = ""
        self.town = ""
        self.country = ""
        self.postcode = ""
        
        
        super.init()

    }
    
    init(deceasedRecord: DeceasedRecord) {
        self.deceasedRecord = deceasedRecord

        // Initialize the form with existing data
        self.title = deceasedRecord.deceasedTitle
        
        self.forename = deceasedRecord.deceasedForename
        self.surname = deceasedRecord.deceasedSurname
        self.religion = deceasedRecord.religionDenomination
        self.occupation = deceasedRecord.occupation
        self.gender = deceasedRecord.gender
        self.maritalStatus = deceasedRecord.maritalStatus
        self.funeralType = deceasedRecord.funeralType
        self.burialType = deceasedRecord.burialType
        self.notes = deceasedRecord.notes
        self.isProblematic = deceasedRecord.isProblematic.boolValue
        
        // Age at death
        if let ageYears = deceasedRecord.ageYears.toInt() {
            self.ageYears = ageYears
        } else {
            self.ageYears = 0
        }
        
        self.ageMonths = deceasedRecord.ageMonths
        self.ageWeeks = deceasedRecord.ageWeeks
        self.ageDays = deceasedRecord.ageDays
        
        // Last residence
        self.placeOfDeath = deceasedRecord.placeOfDeath
        self.addressLine1 = deceasedRecord.lastResidenceAddress1
        self.addressLine2 = deceasedRecord.lastResidenceAddress2
        self.town = deceasedRecord.lastResidenceTown
        self.country = deceasedRecord.lastResidenceCountry
        self.postcode = deceasedRecord.lastResidencePostcode
        
        let df: NSDateFormatter = NSDateFormatter()
        df.dateFormat = "dd MMM yyyy"
        
        // Dates
        self.dateOfDeath = df.dateFromString(deceasedRecord.deceasedDate)
        self.dateOfBurial = df.dateFromString(deceasedRecord.burialDate)
        
        super.init()
    }
    
    private func stringArrayOfNumbers(start: Int, end: Int) -> Array<String> {
        var stringArray: Array<String> = []
        for var index = start; index <= end; ++index {
            stringArray.append(String(index))
        }
        return stringArray
    }
    
    func toModel() -> DeceasedRecord {
        var deceasedRecord: DeceasedRecord
        if self.deceasedRecord != nil {
            deceasedRecord = self.deceasedRecord!
        } else {
            deceasedRecord = DeceasedRecord()
        }
        
        // Initialize the form with existing data
        deceasedRecord.deceasedTitle = self.title
        
        deceasedRecord.deceasedForename = self.forename
        deceasedRecord.deceasedSurname = self.surname
        deceasedRecord.religionDenomination = self.religion
        deceasedRecord.occupation = self.occupation
        deceasedRecord.gender = self.gender
        deceasedRecord.maritalStatus = self.maritalStatus
        deceasedRecord.notes = self.notes
        deceasedRecord.funeralType = self.funeralType
        deceasedRecord.burialType = self.burialType
        deceasedRecord.isProblematic = self.isProblematic
        
        // Age at death
        if let ageYears = self.ageYears {
            deceasedRecord.ageYears = String(Int(ageYears))
        } else {
            deceasedRecord.ageYears  = "0"
        }
        
        //deceasedRecord.ageYears = String(ageYears)
        deceasedRecord.ageMonths = self.ageMonths
        deceasedRecord.ageWeeks = self.ageWeeks
        deceasedRecord.ageDays = self.ageDays
        
        // Last residence
        deceasedRecord.placeOfDeath = self.placeOfDeath
        deceasedRecord.lastResidenceAddress1 = self.addressLine1
        deceasedRecord.lastResidenceAddress2 = self.addressLine2
        deceasedRecord.lastResidenceTown = self.town
        deceasedRecord.lastResidenceCountry = self.country
        deceasedRecord.lastResidencePostcode = self.postcode
        
        // Dates
        let df: NSDateFormatter = NSDateFormatter()
        df.dateFormat = "dd MMM yyyy"
        if let dateOfDeath =  self.dateOfDeath {
            deceasedRecord.deceasedDate = df.stringFromDate(dateOfDeath)
        }
        if let dateOfBurial =  self.dateOfBurial {
            deceasedRecord.burialDate = df.stringFromDate(dateOfBurial)
        }
        
        return deceasedRecord
    }
}
