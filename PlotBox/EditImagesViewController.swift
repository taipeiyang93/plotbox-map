//
//  EditImagesViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/6/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import Alamofire
import WebImage

class EditImagesViewController: UIViewController, UICollectionViewDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate {
    
    /* Internal vars */
    internal weak var plot: Plot?
    
    /* IB Outlets */
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var imageGridView: UICollectionView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageGridView.delegate = self
        self.imageGridView.dataSource = self
        
        // UI Stuff
        self.wrapperView.layer.borderWidth = 1
        self.wrapperView.layer.cornerRadius = 5
        self.wrapperView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.imagePreview.layer.borderWidth = 2
        self.imagePreview.layer.cornerRadius = 10
        self.imagePreview.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.imagePreview.clipsToBounds = true
        
        if let revealViewController : SWRevealViewController = self.revealViewController()
        {
            menuButton.addTarget(revealViewController, action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
            revealViewController.rearViewRevealWidth = 300
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapGesture:")
        
        // add it to the image view;
        imagePreview.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        imagePreview.userInteractionEnabled = true
    }
    
    func tapGesture(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if let imageView = gesture.view as? UIImageView {  // if you subclass UIImageView, then change "UIImageView" to your subclass
            if imageView.image != nil {
                let imageInfo: JTSImageInfo = JTSImageInfo()
                imageInfo.image = imageView.image
                imageInfo.referenceRect = imageView.frame
                imageInfo.referenceView = imageView.superview
                
                
                let imageViewer: JTSImageViewController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle:JTSImageViewControllerBackgroundOptions.Blurred)
                
                imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func captureImageButtonTapped(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            dispatch_async(dispatch_get_main_queue()) {
                
                var image = UIImagePickerController()
                image.modalPresentationStyle = UIModalPresentationStyle.FullScreen
                image.delegate = self
                image.sourceType = UIImagePickerControllerSourceType.Camera
                image.allowsEditing = true
                self.presentViewController(image, animated: true, completion: nil)
            }
        } else {
            Utils.showAlert("Unsupported", message: "The attempted action is unsupported on this device.")
        }
    }
    
    @IBAction func addImageButtonTapped(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            var image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        } else {
            Utils.showAlert("Unsupported", message: "The attempted action is unsupported on this device.")
        }
    }
    
    @IBAction func uploadImageButtonTapped(sender: AnyObject) {
        if let plot = self.plot {
            if let image = self.imagePreview.image {
                let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true, labelText: "Uploading...", mode: MBProgressHUDModeAnnularDeterminate)
                
                // image data
                let imageData = UIImageJPEGRepresentation(image, 1)
                PlotImage.uploadImage(plot, imageData: imageData, progress: { (progressCurrent) -> Void in
                    hud.progress = progressCurrent
                    }, completed: { (error: NSError?) -> Void in
                        hud.hide(true)
                        if error == nil {
                            self.imageGridView.reloadData()
                            self.imagePreview.image = nil
                            Utils.showAlert("Success", message: "Image uploaded successfully!")
                        } else {
                            Utils.showAlert("Error", message: error!.localizedDescription)
                        }
                })
            } else {
                Utils.showAlert("Error", message: "Please select an image to upload.")
            }
        }
    }
    
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        let selectedImage : UIImage = image
        //var tempImage:UIImage = editingInfo[UIImagePickerControllerOriginalImage] as UIImage
        imagePreview.image = selectedImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let plot = self.plot {
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
            let imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
            imageView.backgroundColor = UIColor.blackColor()
            
            let image: PlotImage = plot.images.allObjects[indexPath.row] as! PlotImage
            
            let hud = MBProgressHUD.showHUDAddedTo(imageView, animated: true, labelText: nil)
            imageView.sd_setImageWithURL(image.url, completed: { (image: UIImage!, error: NSError!, type: SDImageCacheType, url: NSURL!) -> Void in
                hud.hide(true)
            })

            let deleteButton : UIButton = cell.viewWithTag(2) as! UIButton
            if isOfflineMode && image.availableOffline != 1 {
                deleteButton.hidden = true
            } else {
                deleteButton.addTarget(self, action: "actionDeleteImage:", forControlEvents: UIControlEvents.TouchDown)
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        dispatch_async(dispatch_get_main_queue(), {
            if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
                var imageView : UIImageView = cell.viewWithTag(1) as! UIImageView
                if imageView.image != nil {
                    let imageInfo: JTSImageInfo = JTSImageInfo()
                    imageInfo.image = imageView.image
                    imageInfo.referenceRect = imageView.frame
                    imageInfo.referenceView = imageView.superview
                    
                    let imageViewer: JTSImageViewController = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle:JTSImageViewControllerBackgroundOptions.Blurred)
                    
                    imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
                }
            }
            
        })
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let plot = self.plot {
            return plot.images.count
        }
        return 0
    }
    
    func actionDeleteImage(sender: UIButton!) {
        if let plot = self.plot {
            let buttonPosition: CGPoint  = (sender as UIButton).convertPoint(CGPointZero, toView: self.imageGridView)
            let indexPath: NSIndexPath? = self.imageGridView.indexPathForItemAtPoint(buttonPosition)
            if let indexPath = indexPath
            {
                if (indexPath.row > -1 && indexPath.row < plot.images.count) {
                    let image: PlotImage = plot.images.allObjects[indexPath.row] as! PlotImage
                    //let imageId:String = image.filename
                    Utils.showAlertWithOkCancel(self, title: "Confirm Delete", message: "Are you sure you want to delete this image?", okActionHandler: { (action: UIAlertAction!) in
                        let cellToDelete: UICollectionViewCell = self.imageGridView.cellForItemAtIndexPath(indexPath) as UICollectionViewCell!
                        let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(cellToDelete, animated: true, labelText: "")
                        
                        PlotImage.deleteImage(image, completionHandler: {(result, error) in
                            hud.hide(true)
                            if let error = error {
                                if error.domain == "unauthorized" {
                                    if let sidebarVC = self.revealViewController().rearViewController as? SidebarTableViewController {
                        sidebarVC.notifyTokenExpired()
                    
                    }
                                }
                                return
                            }
                            
                            if let result = result {
                                self.imageGridView.reloadData()
                            }
                        })
                        
                    })
                }
            }
        }
    }
}
