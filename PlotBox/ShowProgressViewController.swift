//
//  ShowProgressViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/4/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import WebImage

class ShowProgressViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var imageProgress: UIImageView!
    @IBOutlet weak var buttonDone: UIButton!
    var offlineMode: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var url : NSURL = NSBundle.mainBundle().URLForResource("headstone-animation", withExtension: "gif")!
        imageProgress.sd_setImageWithURL(url)
        //imageProgress.
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setOperationTitle(title: String) {
        labelTitle.text = title
    }
    
    func setProgressMessage(message: String, progress: Double, total: Double) {
        labelMessage.text = message
        
        progressBar.hidden = (total == 0)
        imageProgress.hidden  = (total != 0)
        var newProgressValue = Float(progress/total)
        
        if (total != 0) {
            progressBar.setProgress(newProgressValue, animated: progressBar.progress < newProgressValue)
        }
    }
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        // Get the sidebar
        if let rvc = self.presentingViewController as? SWRevealViewController {
            // Clear facilities cache
            CoreData.resetContext()
            isOfflineMode = (Setting.getSettingValue("offline") == "1")
            Facility.clearCache()
            self.dismissViewControllerAnimated(false, completion: { () -> Void in
                rvc.rearViewController.performSegueWithIdentifier("segueSearchDeceasedRecords", sender: self)
            })
        }
    }

}
