//
//  SearchDeceasedRecords.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/14/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import ArcGIS
import SwiftyJSON

class MapManager: NSObject, AGSMapViewTouchDelegate, AGSCalloutDelegate, AGSMapViewLayerDelegate {
    
    internal var mapView: AGSMapView?
    internal weak var mapArea: UIView!
    internal weak var facility: Facility!
    
    internal var pinsLayer : AGSGraphicsLayer!
    
    internal weak var touchDelegate: AGSMapViewTouchDelegate?
    internal weak var calloutDelegate: AGSCalloutDelegate?
    internal weak var layerDelegate: AGSMapViewLayerDelegate?
    
    init(mapAreaView: UIView!, facility: Facility, touchDelegate: AGSMapViewTouchDelegate? = nil, calloutDelegate: AGSCalloutDelegate? = nil, layerDelegate: AGSMapViewLayerDelegate? = nil) {
        super.init()
        
        self.mapArea = mapAreaView
        
        
        self.touchDelegate = self
        self.calloutDelegate = self
        self.layerDelegate = self
        
        if let touchDelegate = touchDelegate {
            self.touchDelegate = touchDelegate
        }
        if let calloutDelegate = calloutDelegate {
            self.calloutDelegate = calloutDelegate
        }
        if let layerDelegate = layerDelegate {
            self.layerDelegate = layerDelegate
        }
        
        
        
        // Load map layers
        self.changeFacility(facility)
    }
    
    func changeFacility(facility: Facility) {
        self.facility = facility
        
        if let mapView = self.mapView {
            mapView.removeFromSuperview()
            self.mapView = nil
        }
        
        let mapView: AGSMapView! = AGSMapView(frame: self.mapArea.bounds)
        self.mapView = mapView
        self.mapArea.addSubview(mapView)
        self.mapArea.sendSubviewToBack(mapView)
        
        
        if let touchDelegate = touchDelegate {
            mapView.touchDelegate = touchDelegate
        }
        if let calloutDelegate = calloutDelegate {
            mapView.callout.delegate = calloutDelegate
        }
        if let layerDelegate = layerDelegate {
            mapView.layerDelegate = layerDelegate
        }
        
        
        mapView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.mapArea.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[mapView]-0-|",
            options:NSLayoutFormatOptions.DirectionLeadingToTrailing,
            metrics: nil,
            views: ["mapView": mapView]))
        
        self.mapArea.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[mapView]-0-|",
            options:NSLayoutFormatOptions.DirectionLeadingToTrailing,
            metrics: nil,
            views: ["mapView": mapView]))
        
        // Add Base map
        self.mapView!.insertMapLayer(self.getTiledLayer(facility), withName: "Basemap Tiled Layer", atIndex: 0)
        
        self.pinsLayer = AGSGraphicsLayer()
        mapView.addMapLayer(self.pinsLayer, withName:"markers")
    }
    
    
    func callout(callout: AGSCallout!, willShowForFeature feature: AGSFeature!, layer: AGSLayer!, mapPoint: AGSPoint!) -> Bool {
        return false
    }
    
    private func getTiledLayer(facility: Facility) -> AGSTiledLayer {
        // Attempt to get from the cache
        let mapFile: String? = MapCache.getMapFile(facility.facilityId, companyId: currentLoginSession!.companyId, context: CoreData.getManagedObjectContext())
        
        if let mapFile = mapFile {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
            let cachedTiledLayer: AGSLocalTiledLayer = AGSLocalTiledLayer(path: documentsPath + "/" + mapFile)
            if cachedTiledLayer.error == nil {
                return cachedTiledLayer
            }
        }
        
        let tiledUrl = NSURL(string: facility.mapUri)
        let tiledLayer = AGSTiledMapServiceLayer(URL: tiledUrl)
        return tiledLayer
    }
    
    func getGPSCoordinates(agsPoint: AGSPoint)->(latitude: Double, longitude: Double) {
        if let mapView = self.mapView {
            let engine : AGSGeometryEngine = AGSGeometryEngine.defaultGeometryEngine()
            let gpsPoint : AGSPoint = engine.projectGeometry(agsPoint, toSpatialReference: AGSSpatialReference.wgs84SpatialReference()) as! AGSPoint
            return (gpsPoint.y + Double(facility.latitudeOffset), gpsPoint.x + Double(self.facility.longitudeOffset))
        }
        
        return (0, 0)
    }
    
    func getAGSPoint(latitude: Double, longitude: Double) -> AGSPoint {
        
        // create a AGSPoint from the GPS coordinates
        let gpsPoint : AGSPoint = AGSPoint(x: longitude - Double(self.facility.longitudeOffset), y: latitude - Double(self.facility.latitudeOffset), spatialReference: AGSSpatialReference.wgs84SpatialReference())
        if let mapView = self.mapView {
            let engine : AGSGeometryEngine = AGSGeometryEngine.defaultGeometryEngine()
            
            let mapPoint : AGSPoint = engine.projectGeometry(gpsPoint, toSpatialReference: mapView.spatialReference) as! AGSPoint
            
            return mapPoint
        }
        return gpsPoint
    }
    
    func zoomToPoint(latitude: Double, longitude: Double) {
        if let mapView = self.mapView {
            mapView.zoomToScale(500, withCenterPoint: self.getAGSPoint(latitude, longitude: longitude), animated: true)
        }
    }
    
    func zoomIn(animated: Bool) {
        if let mapView = self.mapView {
            mapView.zoomIn(animated)
        }
    }
    
    func zoomOut(animated: Bool) {
        if let mapView = self.mapView {
            mapView.zoomOut(animated)
        }
    }
    
    func addPinToPoint(latitude: Double, longitude: Double, attributes: [NSObject : AnyObject]!)  {
        addPinToPoint(self.getAGSPoint(latitude, longitude: longitude), attributes: attributes)
    }
    
    func addPinToPoint(mappoint: AGSPoint!, attributes: [NSObject : AnyObject]!) -> AGSGraphic {
        let pinSymbol = AGSPictureMarkerSymbol()
        
        //pinSymbol.color = UIColor.blueColor()
        pinSymbol.image = UIImage(named: "icon_pin")
        
        
        // Callibrate the pin location so the pin point is at the center of the plot
        pinSymbol.offset = CGPoint(x: 5.8, y: 20)
        pinSymbol.leaderPoint = CGPoint(x: -5.8, y: -20)
        
        //Create the Graphic, using the symbol and
        //geometry created earlier
        let myGraphic = AGSGraphic(geometry: mappoint, symbol: pinSymbol, attributes: attributes)
        
        //Add the graphic to the Graphics layer
        self.pinsLayer.addGraphic(myGraphic)
        
        return myGraphic
    }
    
    func clearPins() {
        self.pinsLayer.removeAllGraphics()
    }
    
    
    func changePinSymbol(pin: AGSGraphic!, iconName: String) {
        let pinSymbol = AGSPictureMarkerSymbol()
        pinSymbol.image = UIImage(named: iconName)
        
        // Callibrate the pin location so the pin point is at the center of the plot
        pinSymbol.offset = CGPoint(x: 5.8, y: 20)
        pinSymbol.leaderPoint = CGPoint(x: -5.8, y: -20)
        pin.symbol = pinSymbol
    }
    
}