//
//  DeceasedRecord.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/3/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import CoreData


@objc(DeceasedRecord)
class DeceasedRecord: NSManagedObject {
    
    @NSManaged var deceasedForename: String
    @NSManaged var deceasedRecordId: String
    @NSManaged var deceasedTitle: String
    @NSManaged var deceasedSurname: String
    @NSManaged var deceasedDate: String
    @NSManaged var religionDenomination: String
    @NSManaged var occupation: String
    @NSManaged var gender: String
    @NSManaged var maritalStatus: String
    @NSManaged var ageYears: String
    @NSManaged var ageDays: String
    @NSManaged var ageWeeks: String
    @NSManaged var ageMonths: String
    @NSManaged var notes: String
    @NSManaged var placeOfDeath: String
    @NSManaged var lastResidence: String
    @NSManaged var lastResidenceAddress1: String
    @NSManaged var lastResidenceAddress2: String
    @NSManaged var lastResidenceTown: String
    @NSManaged var lastResidenceCountry: String
    @NSManaged var lastResidencePostcode: String
    @NSManaged var personManagingFuneral: String
    @NSManaged var registrarName: String
    @NSManaged var burialDate: String
    @NSManaged var modified: NSNumber
    @NSManaged var plot: Plot
    @NSManaged var funeralType: String
    @NSManaged var burialType: String
    @NSManaged var isProblematic: NSNumber
    
    class func entityName() -> NSString {
        return "DeceasedRecord"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> DeceasedRecord {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(DeceasedRecord.entityName() as String, inManagedObjectContext: context!)!
        
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context) as! DeceasedRecord
    }
    
    class func search(text: String, facilityId: String, sectionName: String, offset: Int, limit: Int, completionHandler: (Int?, NSArray?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let request: NSFetchRequest = NSFetchRequest(entityName: DeceasedRecord.entityName() as! String)
                    
                    var predicates: Array<NSPredicate> = []
                    let names = split(text) {$0 == " "}
                    if names.count == 1 {
                        predicates.append(NSPredicate(format: "deceasedForename contains[c] %@ OR deceasedSurname contains[c] %@", names[0], names[0]))
                    } else if names.count == 2 {
                        predicates.append(NSCompoundPredicate.orPredicateWithSubpredicates(
                            [NSPredicate(format: "deceasedForename contains[c] %@ AND deceasedSurname contains[c] %@", names[0], names[1]),
                            NSPredicate(format: "deceasedForename contains[c] %@ AND deceasedSurname contains[c] %@", names[1], names[0])
                            ]))
                    }
            
                    if sectionName != "" {
                        predicates.append(NSPredicate(format: "plot.sectionName contains[c] %@", sectionName))
                    }
                    
                    var compound = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
                    request.predicate = compound
                    
                    let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "deceasedRecordId", ascending: true)
                    request.sortDescriptors = [sortDescriptor]
                    
                    var error: NSError? = nil
                    let totalRecords: Int = CoreData.getManagedObjectContext()!.countForFetchRequest(request, error: &error)
                
                    request.fetchOffset = offset;
                    request.fetchLimit = limit;
                    
                    let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        if error == nil {
                            completionHandler(totalRecords, results, nil)
                        } else {
                            completionHandler(nil, nil, error)
                        }
                    }
                }
            } else {
                DeceasedRecordOnline.search(session, text: text, facilityId: facilityId, sectionName: sectionName, offset: offset, limit: limit, completionHandler: completionHandler)
            }
        }
    }
    
    // GET
    class func getById(deceasedRecordId: String, completionHandler: (DeceasedRecord?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                let request: NSFetchRequest = NSFetchRequest(entityName: DeceasedRecord.entityName() as String)
                
                let predicate = NSPredicate(format: "deceasedRecordId == %@", deceasedRecordId)
                request.predicate = predicate
                
                var error: NSError? = nil
                let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
                
                completionHandler(results[0] as? DeceasedRecord, nil)
            } else {
                DeceasedRecordOnline.getById(session, deceasedRecordId: deceasedRecordId, completionHandler: completionHandler)
            }
        }
    }
    
    // POST
    class func save(deceasedRecord: DeceasedRecord, completionHandler: (Bool?, NSError?) -> Void) {
        if let session = currentLoginSession {
            if isOfflineMode {
                deceasedRecord.modified = true
                CoreData.saveContext(completionHandler)
            } else {
                DeceasedRecordOnline.save(session, deceasedRecord: deceasedRecord, completionHandler: completionHandler)
            }
        }
    }
}
