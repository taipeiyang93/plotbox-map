//
//  Facility.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

private var facilitiesCache: NSArray?

@objc(Facility)
class Facility: NSManagedObject {
    
    @NSManaged var facilityId: String
    @NSManaged var name: String
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged var latitudeOffset: NSNumber
    @NSManaged var longitudeOffset: NSNumber
    @NSManaged var type: String
    @NSManaged var mapId: String
    @NSManaged var mapUri: String
    @NSManaged var sections: NSSet
    
    // Get ordered list of all the sections
    lazy var sortedSections: NSArray = {
        return self.sections.sortedArrayUsingDescriptors([NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")])
    }()
    
    class func entityName() -> NSString {
        return "Facility"
    }
    
    class func managedInstance(context: NSManagedObjectContext?) -> Facility {
        let entity: NSEntityDescription = NSEntityDescription.entityForName(Facility.entityName() as String, inManagedObjectContext: context!)!
        
        return NSManagedObject(entity: entity, insertIntoManagedObjectContext: context!) as! Facility
    }
    
    private class func listOnline(session: LoginSession!, completionHandler: (NSArray?, NSError?) -> Void)  {
        
        Alamofire.request(.GET, session.apiBaseUrl + facilitiesUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                var facilities: NSMutableArray = NSMutableArray()
                if let jsonPlots = json["facilities"].array {
                    for jsonPlot in jsonPlots
                    {
                        
                        if let deceasedRecordsData = jsonPlot.dictionary {
                            let facility = Facility.managedInstance(CoreData.getTemporaryContext())
                            
                            facility.facilityId = jsonPlot["facility_id"].stringValue
                            facility.name = jsonPlot["facility_name"].stringValue
                            facility.latitude = jsonPlot["latitude"].doubleValue
                            facility.longitude = jsonPlot["longitude"].doubleValue
                            facility.latitudeOffset = jsonPlot["point_conversion_latitude_offset"].doubleValue
                            facility.longitudeOffset = jsonPlot["point_conversion_longitude_offset"].doubleValue
                            facility.type = jsonPlot["facility_type"].stringValue
                            facility.mapId = jsonPlot["map_id"].stringValue
                            facility.mapUri = jsonPlot["map_uri"].stringValue
                            
                            let sections: NSMutableSet = NSMutableSet()
                            // Add sections in this facility
                            for jsonFacilitySection in jsonPlot["facility_sections"].arrayValue
                            {
                                let facilitySection: FacilitySection = FacilitySection.managedInstance(CoreData.getTemporaryContext())
                                
                                
                                //let facilitySection = FacilitySection()
                                facilitySection.facilitySectionId = jsonFacilitySection["facility_section_id"].stringValue
                                facilitySection.facilityId = jsonFacilitySection["FacilityID"].stringValue
                                facilitySection.name = jsonFacilitySection["section_name"].stringValue
                                sections.addObject(facilitySection)
                            }
                            facility.sections = sections
                            facilities.addObject(facility)
                        }
                    }
                }
                
                
                // Keep cache for later
                facilitiesCache = facilities
                
                completionHandler(facilities, nil)
        })
    }
    
    private class func listOffline(session: LoginSession!, completionHandler: (NSArray?, NSError?) -> Void)  {
        
        let request: NSFetchRequest = NSFetchRequest(entityName: Facility.entityName() as String)
        let sortDescriptor: NSSortDescriptor = NSSortDescriptor(key: "facilityId", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        var error: NSError? = nil
        let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
        
        // Keep cache for later
        facilitiesCache = results
        
        completionHandler(results, nil)
    }
    
    // GET
    class func list(completionHandler: (NSArray?, NSError?) -> Void) {
        if let session = currentLoginSession {
            
            if let facilities = facilitiesCache {
                completionHandler(facilities, nil)
                return
            }
            
            if isOfflineMode {
                Facility.listOffline(session, completionHandler: completionHandler)
            } else {
                Facility.listOnline(session, completionHandler: completionHandler)
            }
        }
    }
    
    class func clearCache() {
        facilitiesCache = nil
    }
    
    class func getSectionNameForId(sectionId: String) -> String? {
        if !sectionId.isEmpty && facilitiesCache != nil && facilitiesCache!.count > 0 {
            if let facility = facilitiesCache![0] as? Facility {
                for s in facility.sortedSections as! [FacilitySection] {
                    if s.facilitySectionId == sectionId {
                        return s.name
                    }
                }
            }
        }
        
        return nil
    }

}
