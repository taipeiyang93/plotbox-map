//
//  Utilities.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 3/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//
import  Alamofire
import Foundation
import SwiftyJSON

extension Request {
    
    public func responseSwiftyJSONPlotBox(completionHandler: (NSURLRequest, NSHTTPURLResponse?, SwiftyJSON.JSON, NSError?) -> Void) -> Self {
       
        return responseSwiftyJSON({ (request: NSURLRequest, response: NSHTTPURLResponse?, json: JSON, error: NSError?) -> Void in
            
            if json == JSON.nullJSON {
                completionHandler(request, response, json, NSError(domain: "Server has returned an empty reponse.", code: 0, userInfo: nil))
                return
            }
    
            // Token expired
            if let errorId: String = json["error"].string {
                // Check if token was invalid
                if errorId == "access_denied" || errorId == "unauthorized" {
                    completionHandler(request, response, json, NSError(domain: "unauthorized", code: 0, userInfo: nil))
                    return
                    
                }
            }
            
            completionHandler(request, response, json, error)
        })
    }
}

extension UIColor
{
    convenience init(red: Int, green: Int, blue: Int)
    {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

extension UITextField
{
    var trimmedText: String {
        let trimmedText: String = self.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        return trimmedText
    }
    
    func isBlank() -> Bool {
        return self.trimmedText.isEmpty
    }
    
    func toggleHighlightShadow(enabled: Bool) {
        if enabled {
            self.layer.masksToBounds = false;
            self.layer.shadowColor = UIColor.whiteColor().CGColor
            self.layer.shadowOffset = CGSizeZero;
            self.layer.shadowRadius = 7.0;
            self.layer.shadowOpacity = 1.0;
        } else {
            self.layer.masksToBounds = false;
            self.layer.shadowColor = UIColor.blackColor().CGColor
            self.layer.shadowOffset = CGSizeMake(0, -3);
            self.layer.shadowRadius = 3.0;
            self.layer.shadowOpacity = 0;
        }
    }
    
    func highlightWithShadow() {
        
    }
    
    func unHighlightWithShadow() {
        self.layer.masksToBounds = false;
        self.layer.shadowColor = UIColor.greenColor().CGColor
        self.layer.shadowOffset = CGSizeZero;
        self.layer.shadowRadius = 10.0;
        self.layer.shadowOpacity = 1.0;
    }
}


extension String {
    public var toNSData: NSData {
        return dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
    }
    
    public var toPlotBoxDate: String {
        if self.isEmpty {
            return "Unknown"
        } else {
            return self
        }
    }
}

extension NSData {
    public var toString: String {
        return NSString(data: self, encoding: NSUTF8StringEncoding)! as String
    }
}

extension MBProgressHUD {
    
    class func showHUDAddedTo(view: UIView, animated: Bool, labelText: String?, mode: MBProgressHUDMode =  MBProgressHUDModeIndeterminate) -> MBProgressHUD {
        let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(view, animated: animated)
        hud.mode = mode
        if let labelText = labelText {
            hud.labelText = labelText
        }
        //hud.margin = 10.0
        hud.removeFromSuperViewOnHide = true
        return hud
    }

    /*
    
    
    let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(view, animated: true)
    hud.mode = MBProgressHUDModeAnnularDeterminate
    hud.labelText = "Uploading..."
    //hud.margin = 10.0
    hud.removeFromSuperViewOnHide = true
    */
    
}

class Utils {
    class func showAlert (title: String, message: String, buttonTitle: String = "Ok") {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButtonWithTitle(buttonTitle)
        alert.show()
        
    }
    
    class func showAlertWithCompletion (parent: UIViewController!, title: String, message: String, okActionHandler: ((UIAlertAction!) -> Void)!) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: okActionHandler))
        parent.presentViewController(alert, animated: true, completion: nil)
    }
    
    class func showAlertWithOkCancel (parent: UIViewController!, title: String, message: String, okActionHandler: ((UIAlertAction!) -> Void)!, cancelActionHandler: ((UIAlertAction!) -> Void)? = nil) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: okActionHandler))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: cancelActionHandler))
        parent.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    class func showAlertWithRetrySkipStop (parent: UIViewController!, title: String, message: String, retryActionHandler: ((UIAlertAction!) -> Void)!, skipActionHandler: ((UIAlertAction!) -> Void)? = nil, stopActionHandler: ((UIAlertAction!) -> Void)? = nil) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: retryActionHandler))
        alert.addAction(UIAlertAction(title: "Skip", style: .Default, handler: skipActionHandler))
        alert.addAction(UIAlertAction(title: "Stop", style: .Default, handler: stopActionHandler))
        parent.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    class func showAlertWithLoginDialog (parent: UIViewController!, title: String, message: String, okActionHandler: ((String, String) -> Void)!, cancelActionHandler: ((Void) -> Void)!) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            cancelActionHandler()
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let username = alert.textFields![0] as! UITextField
            let password = alert.textFields![1] as! UITextField
            
            okActionHandler(username.text!, password.text!)
        }))
        
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Username"
        })
        
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Password"
            textField.secureTextEntry = true
        })
        
        parent.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    class func showActionSheet (button: UIView!, parent: UIViewController!, title: String, message: String, buttons: Array<String>!, selected: ((selectedIndex: Int) -> Void)!, canceled: (() -> Void)!) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        for var index = 0; index < buttons.count; ++index {
            alert.addAction(UIAlertAction(title: buttons[index], style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                selected(selectedIndex: index)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) -> Void in
            canceled()
        }))
        
        alert.popoverPresentationController?.sourceView = button
        alert.popoverPresentationController?.sourceRect = button.bounds

        parent.presentViewController(alert, animated: false, completion: nil)
    }
    
    class func showFacilityChooser (button: UIView!, parent: UIViewController!, title: String, message: String, facilities: [Facility], selected: ((selectedFacility: Facility) -> Void)!, canceled: (() -> Void)!) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for facility in facilities {
            alert.addAction(UIAlertAction(title: facility.name, style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                selected(selectedFacility: facility)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) -> Void in
            canceled()
        }))
        
        alert.popoverPresentationController?.sourceView = button
        alert.popoverPresentationController?.sourceRect = button.bounds
        
        parent.presentViewController(alert, animated: false, completion: nil)
    }
    
    class func getParentViewController(view: UIView!) -> UIViewController? {
        
        
        for (var next: UIView? = view.superview; next != nil; next = next!.superview)
        {
            if let nextResponder: UIResponder = next!.nextResponder() {
                if nextResponder.isKindOfClass(UIViewController)
                {
                    return nextResponder as? UIViewController
                }
            }
        }
        
        return nil
    }
    
    class func joinStringsWithSingleComma(strings: [String]) -> String {
        var j: String = ""
        
        for var index = 0; index < strings.count; ++index {
            let s: String = strings[index]
            if (!j.isEmpty && !s.isEmpty && !j.hasSuffix(",")) {
                j += ", "
            }
            j += s
        }
        
        return j
    }
    
    class func showTooltip(title: String, message: String, targetView: UIView, inView: UIView, animated: Bool = true) {
        let tooltip: CMPopTipView = CMPopTipView(title: title, message: message)
        tooltip.animation = CMPopTipAnimation.Pop
        tooltip.dismissTapAnywhere = true
        tooltip.backgroundColor = UIColor.blackColor()
        tooltip.has3DStyle = false
        tooltip.presentPointingAtView(targetView, inView: inView, animated: animated)
    }
}