//
//  CreatePlotPopupViewController.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/14/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

enum NewPlotDialogResult {
    case Cancel
    case Save
    case Delete
}

class CreatePlotPopupViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    @IBOutlet var plotImages: [UIImageView]!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonOk: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
    @IBOutlet weak var checkBoxIncrementNextPlot: M13Checkbox!
    @IBOutlet weak var buttonDeletePlot: UIButton!
    
    @IBOutlet weak var deceasedForename: UITextField!
    @IBOutlet weak var deceasedSurname: UITextField!
    @IBOutlet weak var dateOfDeath: UITextField!
    @IBOutlet weak var row: UITextField!
    @IBOutlet weak var plotNumber: UITextField!
    @IBOutlet weak var sectionName: UITextField!
    @IBOutlet weak var totalCapacity: UITextField!
    @IBOutlet weak var notes: UITextView!
    
    lazy var dateFormatter: NSDateFormatter = {
        let df = NSDateFormatter()
        df.dateStyle = NSDateFormatterStyle.LongStyle
        df.locale = NSLocale.currentLocale()
        return df
        }()
    
    lazy var apiDateFormatter: NSDateFormatter = {
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        return df
        }()
    
    var selectedImageView: UIImageView?
    
    weak var plot: NewPlot?
    var completionHandler: ((result: NewPlotDialogResult) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateOfDeath.delegate = self
        
        notes.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).CGColor
        notes.layer.borderWidth = 1.0
        notes.layer.cornerRadius = 5
        
        checkBoxIncrementNextPlot.titleLabel.text = "Increment Next Plot Number"
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.isEqual(dateOfDeath) {
            let vc = UIViewController()
            let datePicker = UIDatePicker(frame: CGRectMake(0, 0, textField.frame.width, 150))
            datePicker.datePickerMode = UIDatePickerMode.Date
            datePicker.hidden = false
            if let selectedDate = dateFormatter.dateFromString(self.dateOfDeath.text!) {
                datePicker.date = selectedDate
            } else {
                datePicker.date = NSDate()
            }
            
            datePicker.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents:UIControlEvents.ValueChanged)
            vc.view.addSubview(datePicker)
            
            let popOverForDatePicker = UIPopoverController(contentViewController: vc)
            popOverForDatePicker.setPopoverContentSize(CGSizeMake(textField.frame.width, 150), animated: false)
            popOverForDatePicker.presentPopoverFromRect(textField.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Up, animated: true)
            
            return false
        }
        return true
    }
    
    
    func handleDatePicker(sender: UIDatePicker) {
        dateOfDeath.text = self.dateFormatter.stringFromDate(sender.date)
    }
    
    func createPlotScreen (plot: NewPlot, defaultPlotNumber: String?, defaultRow: String?, defaultSectionName: String?, incrementNextPlotNumber: Bool, fnCompletionHandler: ((result: NewPlotDialogResult, incrementNextPlotNumber: Bool) -> Void)?) {
        
        self.completionHandler =  { (result) -> Void in
            if let fnCompletionHandler = fnCompletionHandler {
                fnCompletionHandler(result: result, incrementNextPlotNumber: self.checkBoxIncrementNextPlot.checkState == M13CheckboxState.Checked)
            }
        }
        self.labelTitle.text = "Create plot"
        self.buttonDeletePlot.hidden = true
        self.checkBoxIncrementNextPlot.hidden = false
        self.checkBoxIncrementNextPlot.checkState = incrementNextPlotNumber ? M13CheckboxState.Checked : M13CheckboxState.Unchecked
        self.loadPlot(plot)
        
        if let defaultPlotNumber = defaultPlotNumber {
            self.plotNumber.text = defaultPlotNumber
        }
        
        if let defaultRow = defaultRow {
            self.row.text = defaultRow
        }
        
        if let defaultSectionName = defaultSectionName {
            self.sectionName.text = defaultSectionName
        }
    }
    
    func updatePlotScreen (plot: NewPlot, fnCompletionHandler: ((result: NewPlotDialogResult) -> Void)?) {
        self.completionHandler = fnCompletionHandler
        self.labelTitle.text = "Update plot"
        self.buttonDeletePlot.hidden = false
        self.checkBoxIncrementNextPlot.hidden = true
        self.loadPlot(plot)
    }
    
    private func loadPlot(plot: NewPlot) {
        self.plot = plot
        self.deceasedForename.text = plot.deceasedForename
        self.deceasedSurname.text = plot.deceasedSurname
        self.dateOfDeath.text = ""
        if let apiDateOfDeath = apiDateFormatter.dateFromString(plot.deceasedDate) {
            self.dateOfDeath.text = dateFormatter.stringFromDate(apiDateOfDeath)
        }
        
        self.row.text = plot.row
        self.plotNumber.text = plot.plotNumber
        self.totalCapacity.text = String(Int(plot.totalCapacity))
        self.sectionName.text = plot.sectionName
        self.notes.text = plot.notes
        
        for i in 0...3 {
            plotImages[i].userInteractionEnabled = true
            plotImages[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: "buttonTapped:"))
            
            // Load existing plot images if available
            if plot.images.count > i {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
                let imageFilePath = documentsPath.stringByAppendingPathComponent((plot.images.allObjects[i] as! NewPlotImage).filename)
                
                plotImages[i].contentMode = UIViewContentMode.ScaleAspectFit
                plotImages[i].tag = 1
                plotImages[i].image = UIImage(contentsOfFile: imageFilePath)
            }
        }
    }
    
    func buttonTapped(sender: UITapGestureRecognizer) {
        
        self.selectedImageView = sender.view?.hitTest(sender.locationInView(sender.view), withEvent: nil) as? UIImageView
        dispatch_async(dispatch_get_main_queue()) {
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                var image = UIImagePickerController()
                image.delegate = self
                image.sourceType = UIImagePickerControllerSourceType.Camera
                image.allowsEditing = true
                self.presentViewController(image, animated: true, completion: nil)
            } else {
                Utils.showAlert("Unsupported", message: "The attempted action is unsupported on this device.")
            }
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        if let selectedImageView = self.selectedImageView {
            selectedImageView.contentMode = UIViewContentMode.ScaleAspectFit
            selectedImageView.tag = 1
            selectedImageView.image = image
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func validateForm() -> Bool {
        var errors: Array<String> = []
        /*if self.deceasedForename.isBlank() {
            errors.append(" - Deceased First name is required.")
        }
        if self.deceasedSurname.isBlank() {
            errors.append(" - Deceased Surname is required.")
        }
        if self.row.isBlank() {
            errors.append(" - Row is required.")
        }*/
        if self.plotNumber.isBlank() {
            errors.append(" - Plot Number is required.")
        }
        if self.sectionName.isBlank() {
            errors.append(" - Section Name is required.")
        }
        
        if errors.count == 0 {
            var isNewEntry: Bool = false
            if let plot = self.plot {
                isNewEntry = !(plot.plotNumber == self.plotNumber.text &&
                    plot.sectionName == self.sectionName.text &&
                    plot.row == self.row.text)
            }
            
            if isNewEntry && (Plot.plotExists(self.plotNumber.text, sectionName: self.sectionName.text, row: self.row.text) || NewPlot.plotExists(self.plotNumber.text, sectionName: self.sectionName.text, row: self.row.text)) {
                errors.append(" - Plot already exists.")
            }
        }
        
        if errors.count > 0 {
            Utils.showAlert("Validation Error(s)", message: "\n".join(errors))
            return false
        }
        return true
    }
    
    func savePlot() {
        if let plot = self.plot {
            plot.deceasedForename = self.deceasedForename.text
            plot.deceasedSurname = self.deceasedSurname.text
            plot.deceasedDate = ""
            if let localeDateOfDeath = dateFormatter.dateFromString(self.dateOfDeath.text!) {
                plot.deceasedDate = apiDateFormatter.stringFromDate(localeDateOfDeath)
            }
            
            plot.row = self.row.text
            plot.plotNumber = self.plotNumber.text
            plot.sectionName = self.sectionName.text
            if let totalCapacity = self.totalCapacity.text.toInt() {
                plot.totalCapacity = totalCapacity
            }
            plot.notes = self.notes.text
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
            
            // Keep and array of old images for deleting them later
            var oldImages: [NewPlotImage] = []
            for image in plot.images.allObjects as! [NewPlotImage] {
                oldImages.append(image)
            }
            
            // Add new images
            var images: NSMutableSet = NSMutableSet()
            for i in 0...3 {
                if plotImages[i].tag == 1 {
                    let imageFileName = "\(NSUUID().UUIDString).jpg"
                    UIImageJPEGRepresentation(plotImages[i].image, 1).writeToFile(documentsPath.stringByAppendingPathComponent(imageFileName), atomically: true)
                    
                    let image = NewPlotImage.managedInstance(CoreData.getManagedObjectContext())
                    image.filename = imageFileName
                    images.addObject(image)
                }
            }
            
            // Delete old images
            for image in oldImages {
                let imageFilePath = documentsPath.stringByAppendingPathComponent(image.filename)
                var error: NSError?
                NSFileManager.defaultManager().removeItemAtPath(imageFilePath, error: &error)
                CoreData.getManagedObjectContext()?.deleteObject(image)
            }
            
            plot.images = images
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        if self.validateForm() {
            self.savePlot()
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                if let completionHandler = self.completionHandler {
                    completionHandler(result: NewPlotDialogResult.Save)
                }
            })
        }
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            if let completionHandler = self.completionHandler {
                completionHandler(result: NewPlotDialogResult.Cancel)
            }
        })
    }
    
    @IBAction func deleteButtonTapped(sender: AnyObject) {
        Utils.showAlertWithOkCancel(self, title: "Confirm Delete?", message: "Are you sure you want to delete this plot?", okActionHandler: { (action: UIAlertAction!) -> Void in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                if let completionHandler = self.completionHandler {
                    completionHandler(result: NewPlotDialogResult.Delete)
                }
            })
            }, cancelActionHandler: nil)
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
