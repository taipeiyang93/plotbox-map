//
//  OfflineModeManager.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/23/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import CoreData

class OfflineModeManager {
    private weak var parentViewController: UIViewController?
    private var showProgressView: ShowProgressViewController!

    class var sharedInstance: OfflineModeManager {
        struct Static {
            static var instance: OfflineModeManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = OfflineModeManager()
        }
        
        return Static.instance!
    }
    
    enum RetryDialogResult {
        case Retry
        case Skip
        case Stop
    }
    
    func showRetryDialog(title: String, message: String, result: ((RetryDialogResult) -> Void)!) {
        Utils.showAlertWithRetrySkipStop(self.showProgressView, title: title, message: message, retryActionHandler: { (alertAction: UIAlertAction!) -> Void in
            result(RetryDialogResult.Retry)
        }, skipActionHandler: { (alertAction: UIAlertAction!) -> Void in
            result(RetryDialogResult.Skip)
        }) { (alertAction: UIAlertAction!) -> Void in
            result(RetryDialogResult.Stop)
        }
    }
    
    private func initProgressViewDialog() {
        if let parentViewController = self.parentViewController {
            let storyboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            self.showProgressView = storyboard.instantiateViewControllerWithIdentifier("showProgress") as! ShowProgressViewController
            
            self.showProgressView.modalPresentationStyle = UIModalPresentationStyle.FormSheet;
            self.showProgressView.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
            self.showProgressView.preferredContentSize = CGSizeMake(450, 250);
            parentViewController.presentViewController(showProgressView, animated: true, completion: nil)
        }
    }
    
    
    func setParent(parentViewController: UIViewController) {
        self.parentViewController = parentViewController
    }
    
    func startUpload(facility: Facility) {
        startUpload(facility, loginSession: currentLoginSession!)
    }
    
    func startUpload(facility: Facility, loginSession: LoginSession) {
        if let parentViewController = self.parentViewController {
            initProgressViewDialog()
            
            showProgressView.setOperationTitle(String(format: "Uploading data for %@", facility.name))
            showProgressView.setProgressMessage("Starting, please wait...", progress: 0, total: 0)
            
            PlotUploaderTask(session: loginSession, facilityId: facility.facilityId, progress: self.progress, completed: { () -> Void in
                DeceasedRecordUploaderTask(session: loginSession, facilityId: facility.facilityId, progress: self.progress, completed: { () -> Void in
                    ImageUploaderTask(session: loginSession, progress: self.progress, completed: { () -> Void in
                        NewPlotUploaderTask(session: loginSession, facilityId: facility.facilityId, progress: self.progress, completed: { () -> Void in
                            
                            Setting.saveSettingValue("offline", value: "0")
                            self.showProgressView.setOperationTitle("Finished!")
                            self.showProgressView.setProgressMessage("Please tap done to start using the app in online mode.", progress: 1, total: 1)
                            
                            ClearCacheTask(progress: self.progress, completed: { () -> Void in
                                Setting.saveSettingValue("offline", value: "0")
                                self.showProgressView.setOperationTitle("Finished!")
                                self.showProgressView.setProgressMessage("Please tap done to start using the app in online mode.", progress: 1, total: 1)
                                self.completed()
                                
                                
                                
                                }, failed: self.failed).start()
                            }, failed: self.failed).start()
                        
                        
                        }, failed: self.failed).start()
                    }, failed: self.failed).start()
                
                }, failed: self.failed).start()
        }
    }
    
    func startDownload(facility: Facility) {
        UIApplication.sharedApplication().idleTimerDisabled = true // Disable sleep mode
        if let parentViewController = self.parentViewController {
            initProgressViewDialog()
            
            self.showProgressView.setOperationTitle(String(format: "Initializing the process for %@", facility.name))
            self.showProgressView.setProgressMessage("Clearing previous cache...", progress: 0, total: 0)
            // 0.
            ClearCacheTask(progress: self.progress, completed: { () -> Void in
                self.showProgressView.setOperationTitle(String(format: "Downloading map for %@", facility.name))
                // 1.
                MapDownloaderTask(session: currentLoginSession!, facility: facility, progress: self.progress, completed: { () -> Void in
                    // 2.
                    self.showProgressView.setOperationTitle(String(format: "Downloading data for %@", facility.name))
                    FacilityDownloaderTask(session: currentLoginSession!, facility: facility, progress: self.progress,
                        completed: { () -> Void in
                            
                            // 3.
                            self.showProgressView.setOperationTitle(String(format: "Downloading plots for %@", facility.name))
                            PlotsDownloaderTask(session: currentLoginSession!, facilityId: facility.facilityId, progress: self.progress, completed: { () -> Void in
                                Setting.saveSettingValue("offline", value: "1")
                                self.showProgressView.setOperationTitle("Finished!")
                                self.showProgressView.setProgressMessage("Please tap done to start using the app in offline mode.", progress: 1, total: 1)
                                self.completed()
                                
                                }, failed: self.failed).start()
                            
                        }, failed: self.failed).start()
                    
                    }, failed: self.failed).start()
                
                }, failed: self.failed).start()
        }
        
    }
    
    class func deleteAllEntities(entityName: String) {
        let request: NSFetchRequest = NSFetchRequest(entityName: entityName)
        request.includesPropertyValues = false
        var error: NSError? = nil
        let results: NSArray = CoreData.getManagedObjectContext()!.executeFetchRequest(request, error: &error)!
        for result in results {
            CoreData.getManagedObjectContext()!.deleteObject(result as! NSManagedObject)
        }
    }
    
    func progress(message: String, progressCurrent: Double, progressTotal: Double) -> Void {
        self.showProgressView.setProgressMessage(message, progress: progressCurrent, total: progressTotal)
    }
    
    func failed(message: String) -> Void {
        self.showProgressView.setProgressMessage("Error: " + message, progress: 0, total: 1)
        self.showProgressView.buttonDone.hidden = false
    }
    
    func completed() -> Void {
        UIApplication.sharedApplication().idleTimerDisabled = false // Enable sleep mode again
        self.showProgressView.buttonDone.hidden = false
    }
}
