//
//  PlotImageOnline.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 5/6/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PlotImageOnline {
    
    
    class func uploadImage(session: LoginSession!, plotId: String, imageData: NSData, progress: ((progressCurrent: Float)->Void)?, completed: ((imageId: String?, error: NSError?)->Void)?) {
        
        // init paramters Dictionary
        var parameters = [
            "access_token": session.token,
            "company_identifier": session.companyId,
            "plot_id" : plotId,
        ]
        
        // CREATE AND SEND REQUEST ----------
        let urlRequest = PlotImageOnline.urlRequestWithComponents(session.apiBaseUrl + addPlotImagesUrl, parameters: parameters, imageData: imageData)
        
        Alamofire.upload(urlRequest.0, data: urlRequest.1)
            .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                if let progress = progress {
                    progress(progressCurrent: Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))
                }
            }
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                if let completed = completed {
                    if let e = error
                    {
                        completed(imageId: nil, error: e)
                        return
                    }
                    
                    if json["status"].stringValue == "success" {
                        let newImageId = json["image_id"].stringValue
                        if newImageId != "" {
                            completed(imageId: newImageId, error: nil)
                            return
                        }
                    } else if json["error"] != nil {
                        completed(imageId: nil, error: NSError(domain: json["error_description"].stringValue, code: 0, userInfo: nil))
                        return
                    }
                    completed(imageId: nil, error: NSError(domain: "Unexpected Error", code: 0, userInfo: nil))
                }
                
        })
    }
    
    class func deleteImage(session: LoginSession!, image: PlotImage, completionHandler: (String?, NSError?) -> Void) {
        let plot = image.plot
        Alamofire.request(.GET, session.apiBaseUrl + deleteImageUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId, "plot_id": image.plot.plotId, "image_ids": image.filename])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                if json["status"].stringValue == "success" {
                    let images: NSMutableSet = plot.images.mutableCopy() as! NSMutableSet
                    images.removeObject(image)
                    plot.images = images
                    
                    completionHandler(json["message"].stringValue, nil)
                } else {
                    completionHandler(nil, NSError(domain: "unexpected", code: 0, userInfo: nil))
                }
        })
    }
    
    // this function creates the required URLRequestConvertible and NSData we need to use Alamofire.upload
    class func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, imageData:NSData) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
         
        let boundaryConstant = "----------Boundary-\(NSUUID().UUIDString)"
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add parameters
        for (index, (key, value)) in enumerate(parameters) {
            if index == 0 {
                uploadData.appendData("--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            } else {
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            }
            
            uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        
        // add image
        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Disposition: form-data; name=\"image\"; filename=\"\(NSUUID().UUIDString).jpg\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Type: image/jpeg\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData(imageData)
        
        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }
    
}
