//
//  Plot.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/2/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit
import Alamofire

class PlotOnline {
    internal var plotId: String = ""
    internal var latitude: Double = 0
    internal var longitude: Double = 0
    
    internal var sectionName: String = ""
    internal var plotNumber: String = ""
    internal var remainingCapacity: Int = 0
    internal var totalCapacity: Int = 0
    
    internal var easting: String = ""
    internal var northing: String = ""
    
    // Additional Details
    internal var row: String = ""
    internal var lastUpdated: String = ""
    internal var lastUpdatedBy: String = ""
    internal var facilityName: String = ""
    
    internal var depthRemainingFeet: String = ""
    internal var depthRemainingInches: String = ""
    internal var depthRemainingString: String = ""
    
    // Deed owner details
    internal var deedOwner: DeedOwner = DeedOwner()
    internal var deedHistory: [DeedOwner] = []
    
    // memorial_mason_work
    internal var mmwPermitRegistrationNumber: String = ""
    internal var mmwDate: String = ""
    internal var mmwStatus: String = ""
    
    // Images
    internal var imageBaseUrl: String = ""
    internal var primaryImage: String = ""
    internal var images: [String] = [String]()
    internal var deceasedRecords: [DeceasedRecord] = []
    
    
    init() {
    }
    
    init(plotId: String, remainingCapacity: Int, totalCapacity: Int) {
        self.plotId = plotId
        self.remainingCapacity = remainingCapacity
        self.totalCapacity = totalCapacity
    }
    
    // GET
    class func search(session: LoginSession!, plotNumber: String, facilityId: String, sectionName: String, row: String, forename: String, surname: String, dateOfDeath: String, unlinked: Bool, offset: Int, limit: Int, completionHandler: (Int?, NSArray?, NSError?) -> Void) {
        
        var dateOfDeathApi = ""
        if !dateOfDeath.isEmpty {
            let df = NSDateFormatter()
            df.dateStyle = NSDateFormatterStyle.LongStyle
            df.locale = NSLocale.currentLocale()
            if let nsDateOfDeath = df.dateFromString(dateOfDeath) {
                let dfApi = NSDateFormatter()
                dfApi.dateFormat = "yyyy-MM-dd"
                dateOfDeathApi = dfApi.stringFromDate(nsDateOfDeath)
            }
        }
        
        var parameters = ["access_token": session.token, "company_identifier": session.companyId, "facility_id": facilityId, "section_name": sectionName, "row": row, "plot_number": plotNumber, "forename": forename, "surname": surname, "date_of_death": dateOfDeathApi, "offset": String(offset), "limit": String(limit)]
        
        if unlinked {
            parameters["unlinked"] = "y"
        }
        
        Alamofire.request(.GET, session.apiBaseUrl + searchPlotsUrl, parameters: parameters)
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, nil, e)
                    return
                }
                
                var plots:NSMutableArray = NSMutableArray()
                
                let totalRecords = json["total_plots"].intValue
                if let jsonPlots = json["plots"].array {
                    for jsonPlot in jsonPlots
                    {
                        
                        if let plotsData = jsonPlot.dictionary {
                            let plot = Plot.managedInstance(CoreData.getTemporaryContext())
                            
                            plot.plotId = jsonPlot["plot_id"].stringValue
                            plot.sectionName = jsonPlot["section_name"].stringValue
                            plot.row = jsonPlot["row"].stringValue
                            plot.plotNumber = jsonPlot["plot_number"].stringValue
                            plot.remainingCapacity = jsonPlot["remaining_capacity"].intValue
                            plot.totalCapacity = jsonPlot["total_capacity"].intValue
                            plot.latitude = jsonPlot["latitude"].doubleValue
                            plot.longitude = jsonPlot["longitude"].doubleValue
                            //plot.northing = jsonPlot["northing"].stringValue
                            //plot.easting = jsonPlot["easting"].stringValue
                            
                            // Deceased Records
                            var deceasedRecords: NSMutableSet = NSMutableSet()
                            for (index: String, subJson) in jsonPlot["deceased_records"] {
                                var deceasedRecord: DeceasedRecord = DeceasedRecord.managedInstance(CoreData.getTemporaryContext())
                                deceasedRecord.deceasedRecordId = subJson["deceased_id"].stringValue
                                deceasedRecord.deceasedForename = subJson["deceased_forename"].stringValue
                                deceasedRecord.deceasedSurname = subJson["deceased_surname"].stringValue
                                deceasedRecord.deceasedDate = subJson["deceased_date"].stringValue
                                deceasedRecords.addObject(deceasedRecord)
                                break
                            }
                            plot.deceasedRecords = deceasedRecords
                            
                            plots.addObject(plot)
                        }
                    }
                }
                
                completionHandler(totalRecords, plots, nil)
        })
    }
    
    
    class func getUnusedPlots(session: LoginSession! ,facilityId: String, facilitySectionId: String, completionHandler: (NSArray?, NSError?) -> Void) {
        
        Alamofire.request(.GET, session.apiBaseUrl + searchPlotsUrl, parameters: ["access_token": session.token,
            "company_identifier": session.companyId,
            "facility_id": facilityId,
            "facility_section_id": facilitySectionId,
            "unused":"y"])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                var plots: NSMutableArray = NSMutableArray()
                if let jsonPlots = json["plots"].array {
                    for jsonPlot in jsonPlots
                    {
                        
                        if let plotsData = jsonPlot.dictionary {
                            let plot = Plot()
                            
                            plot.plotId = jsonPlot["plot_id"].stringValue
                            plot.sectionName = jsonPlot["section_name"].stringValue
                            plot.plotNumber = jsonPlot["plot_number"].stringValue
                            plot.remainingCapacity = jsonPlot["remaining_capacity"].intValue
                            plot.totalCapacity = jsonPlot["total_capacity"].intValue
                            plot.latitude = jsonPlot["latitude"].doubleValue
                            plot.longitude = jsonPlot["longitude"].doubleValue
                            //plot.northing = jsonPlot["northing"].stringValue
                            //plot.easting = jsonPlot["easting"].stringValue
                            
                            plots.addObject(plot)
                        }
                    }
                }
                
                completionHandler(plots, nil)
        })
    }
    
    class func getById(session: LoginSession!, plotId: String, completionHandler: (Plot?, NSError?) -> Void) {
        
        Alamofire.request(.GET, session.apiBaseUrl + viewPlotUrl, parameters: ["access_token": session.token, "company_identifier": session.companyId, "plot_id": plotId])
            .responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(nil, e)
                    return
                }
                
                var plot: Plot = Plot.managedInstance(CoreData.getTemporaryContext())
                
                plot.plotId = plotId
                
                plot.sectionName = json["plot_data"]["section_name"].stringValue
                plot.plotNumber = json["plot_data"]["plot_number"].stringValue
                plot.remainingCapacity = json["plot_data"]["remaining_capacity"].intValue
                plot.totalCapacity = json["plot_data"]["total_capacity"].intValue
                
                plot.depthRemainingFeet = json["plot_data"]["depth_remaining_feet"].stringValue
                plot.depthRemainingInches = json["plot_data"]["depth_remaining_inches"].stringValue
                plot.depthRemainingString = json["plot_data"]["depth_remaining_string"].stringValue
                
                plot.row = json["plot_data"]["row"].stringValue
                //plot.lastUpdated = json["plot_data"]["last_updated_date_time"].stringValue
                //plot.lastUpdatedBy = json["plot_data"]["last_updated_by_user_name"].stringValue
                
                plot.deedOwnerForename = json["plot_data"]["deed_owner_forename"].stringValue
                plot.deedOwnerSurname = json["plot_data"]["deed_owner_surname"].stringValue
                plot.notes = json["plot_data"]["notes"].stringValue
                
                plot.imageBaseUrl = json["image_base_path"].stringValue
                plot.primaryImage = json["plot_data"]["primary_image_id"].stringValue
                
                // Images of the Plot
                var images: NSMutableSet = NSMutableSet()
                if let plotImages = json["plot_images"].array {
                    for plotImage in plotImages
                    {
                        if let plotImageData = plotImage.dictionary {
                            if let imageId = plotImageData["image_id"] {
                                let image = PlotImage.managedInstance(CoreData.getTemporaryContext())
                                image.filename = imageId.stringValue
                                image.plot = plot
                                images.addObject(image)
                            }
                        }
                    }
                }
                plot.images = images
                
                // Deceased Records
                var deceasedRecords: NSMutableSet = NSMutableSet()
                for (index: String, subJson) in json["deceased_records"] {
                    var deceasedRecord: DeceasedRecord = DeceasedRecord.managedInstance(CoreData.getTemporaryContext())
                    deceasedRecord.deceasedRecordId = subJson["deceased_id"].stringValue
                    deceasedRecord.deceasedForename = subJson["deceased_forename"].stringValue
                    deceasedRecord.deceasedSurname = subJson["deceased_surname"].stringValue
                    deceasedRecord.deceasedDate = subJson["deceased_date"].stringValue
                    deceasedRecords.addObject(deceasedRecord)
                }
                plot.deceasedRecords = deceasedRecords
                
                completionHandler(plot, nil)
        })
        
    }
    
    
    // POST
    class func save(session: LoginSession!, plot: Plot, completionHandler: (Bool!, NSError?) -> Void) {
        
        Alamofire.request(.POST, session.apiBaseUrl + editPlotUrl,
            parameters: ["access_token": session.token, "company_identifier": session.companyId,
                "plot_id": plot.plotId,
                "section_name": plot.sectionName,
                "row": plot.row,
                "plot_number": plot.plotNumber,
                "total_capacity": plot.totalCapacity,
                "depth_remaining_feet": plot.depthRemainingFeet,
                "depth_remaining_inches": plot.depthRemainingInches,
                "depth_remaining_string": plot.depthRemainingString,
                "latitude": plot.latitude,
                "longitude": plot.longitude,
                "notes": plot.notes
                
            ]).responseSwiftyJSONPlotBox({ (request, response, json, error) in
                
                if let e = error
                {
                    completionHandler(false, e)
                    return
                }
                
                if json["error"].stringValue != "" {
                    completionHandler(false, NSError(domain: json["error_description"].stringValue, code: 0, userInfo: nil))
                    return
                }
                
                completionHandler(json["status"].stringValue == "success", nil)
        })
        
    }
    
}
