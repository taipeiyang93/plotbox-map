//
//  PlotForm.swift
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/7/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

import UIKit

class PlotForm: NSObject, FXForm {
    var plot: Plot!
    var facility: Facility!
    
    var plotNumber: String = ""
    var sectionName: String  = ""
    var row: String = ""
    var totalCapacity: String  = ""
    var depthRemainingFeet: String  = ""
    var depthRemainingInches: String  = ""
    var depthRemainingString: String  = ""
    
    var plotNumberOptions: NSArray! = []
    
    //var description: String?
    
    func fields() -> [AnyObject]! {
        
        var facilitySections: NSMutableArray = []
        for item in facility.sortedSections {
            if let facilitySection = item as? FacilitySection {
                facilitySections.addObject(facilitySection.name)
            }
        }
        
        return [
            // Personal Details
            
            [FXFormFieldKey: "plotNumber", FXFormFieldHeader: "Plot Details"],
            //[FXFormFieldKey: "plotNumber", FXFormFieldHeader: "Plot Details", FXFormFieldOptions: plotNumberOptions, FXFormFieldType: FXFormFieldTypeOption,
             //   FXFormFieldCell: FXFormOptionPickerCell.self],
            [FXFormFieldKey: "sectionName", FXFormFieldOptions: facilitySections, FXFormFieldType: FXFormFieldTypeOption /*, FXFormFieldAction: "sectionNameChanged" */],
            
            [FXFormFieldKey: "row"],
            
            [FXFormFieldKey: "totalCapacity"],
            

            [FXFormFieldKey: "depthRemainingFeet", FXFormFieldTitle: "Feet", FXFormFieldHeader: "Depth Remaining"],
            [FXFormFieldKey: "depthRemainingInches", FXFormFieldTitle: "Inches"],
            [FXFormFieldKey: "depthRemainingString", FXFormFieldTitle: "String"]
        ]
    }
    
    
    init(plot: Plot, facility: Facility) {
        super.init()
        self.plot = plot
        self.facility = facility
        
        self.plotNumber = plot.plotNumber
        self.sectionName = plot.sectionName
        
        self.row = plot.row
        self.totalCapacity = String(Int(plot.totalCapacity))
        self.depthRemainingFeet = plot.depthRemainingFeet
        self.depthRemainingInches = plot.depthRemainingInches
        self.depthRemainingString = plot.depthRemainingString
    }
    
    /*class func sectionIdFromName(name: String, facility: Facility) -> String {
        for item in facility.sortedSections {
            if let facilitySection = item as? FacilitySection {
                if facilitySection.name == name {
                    return facilitySection.facilitySectionId
                }
            }
        }
        return ""
    }*/
    
    func toModel() -> Plot {
        var plot: Plot
        if self.plot != nil {
            plot = self.plot!
        } else {
            plot = Plot()
        }
        
        plot.plotNumber = self.plotNumber
        plot.sectionName =  self.sectionName
        //plot.section.facilitySectionId = PlotForm.sectionIdFromName(self.sectionName, facility: self.facility)
        
        plot.row = self.row
        if let totalCapacity = self.totalCapacity.toInt() {
            
            /*
            $old_total_capacity = $plot->TotalCapacity;
            if($new_total_capacity > $old_total_capacity){
            $capacity_increase = $new_total_capacity - $old_total_capacity;
            $data['RemainingCapacity'] = $plot->RemainingCapacity + $capacity_increase;
            }elseif($new_total_capacity < $old_total_capacity){
            $capacity_decrease = $old_total_capacity - $new_total_capacity;
            $remaining_capacity = $plot->RemainingCapacity - $capacity_decrease;
            $data['RemainingCapacity'] = ($remaining_capacity < 0) ? 0 : $remaining_capacity;
            }
            
            */
            
            plot.totalCapacity = totalCapacity
            //plot.
        }

        plot.depthRemainingFeet = self.depthRemainingFeet
        plot.depthRemainingInches = self.depthRemainingInches
        plot.depthRemainingString = self.depthRemainingString
        return plot
    }

}
