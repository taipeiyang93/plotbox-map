//
//  SHTableView.h
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/7/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHTableView : UITableView

@end
