//
//  MapCache.m
//  PlotBox
//
//  Created by Nishadh Shrestha on 4/22/15.
//  Copyright (c) 2015 PlotBox. All rights reserved.
//

#import "MapCache.h"


@implementation MapCache

@dynamic companyId;
@dynamic facilityId;
@dynamic mapFile;

@end
